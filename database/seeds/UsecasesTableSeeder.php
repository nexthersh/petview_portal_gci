<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Usecase;

class UsecasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usecase::create([
            'name' => 'ACL'
        ]);

        Usecase::create([
            'name' => 'DV'
        ]);
    }
}
