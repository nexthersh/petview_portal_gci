<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricingPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pricing_plans')->insert(
            [
                'name' => 'percase',
                'price' => 19,
                'number_of_cases' => 0,
                'price_percase' => 19,
                'length' => 1,
                'unit' => 'case',
                'tier' => 0,
            ]
        );
        DB::table('pricing_plans')->insert(
            [
                'name' => '150',
                'price' => 150,
                'number_of_cases' => 10,
                'price_percase' => 15,
                'length' => 1,
                'unit' => 'months',
                'tier' => 1,
            ]
        );
        DB::table('pricing_plans')->insert(
            [
                'name' => '250',
                'price' => 250,
                'number_of_cases' => 20,
                'price_percase' => 13,
                'length' => 1,
                'unit' => 'months',
                'tier' => 2,
            ]
        );
        DB::table('pricing_plans')->insert(
            [
                'name' => '350',
                'price' => 350,
                'number_of_cases' => 30,
                'price_percase' => 11,
                'length' => 1,
                'unit' => 'months',
                'tier' => 3,
            ]
        );
    }
}
