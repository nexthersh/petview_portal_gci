<?php

use Illuminate\Database\Seeder;
use App\BodyPart;

class BodyPartTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		BodyPart::create([
			'name' => 'Skull',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Abdomen',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Pelvis',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Thorax',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Forelimb Right',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Forelimb Left',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Hindlimb Right',
			'restricted' => 1
		]);
		BodyPart::create([
			'name' => 'Hindlimb Left',
			'restricted' => 1
		]);        
    }
}
