<?php

use Illuminate\Database\Seeder;
use App\BodyViewAlgorithm;

class BodyViewAlgorithmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BodyViewAlgorithm::create([
        	'body_view_id' => 2,
        	'algorithm_id' => 3,
        ]);

        BodyViewAlgorithm::create([
        	'body_view_id' => 4,
        	'algorithm_id' => 1,
        ]);
    }
}
