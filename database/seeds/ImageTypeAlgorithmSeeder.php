<?php

use Illuminate\Database\Seeder;
use App\ImagetypeAlgorithm;

class ImageTypeAlgorithmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImagetypeAlgorithm::insert([
        	'image_type_id' => 1,
        	'algorithm_id' => 1
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 1,
        	'algorithm_id' => 2
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 4,
        	'algorithm_id' => 1
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 2,
        	'algorithm_id' => 2
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 3,
        	'algorithm_id' => 2
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 1,
        	'algorithm_id' => 3
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 2,
        	'algorithm_id' => 3
        ]);

        ImagetypeAlgorithm::insert([
        	'image_type_id' => 3,
        	'algorithm_id' => 3
        ]);
    }
}
