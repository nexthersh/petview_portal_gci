<?php

use Illuminate\Database\Seeder;

class ImageTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('image_type')->insert(
        	[
        		'name' => 'Normal'
        	]);
        DB::table('image_type')->insert(
        	[
        		'name' => 'Benign'
        	]);
        DB::table('image_type')->insert(
        	[
        		'name' => 'Malignancy'
        	]);
        DB::table('image_type')->insert(
            [
                'name' => 'Abnormal'
            ]);
    }
}
