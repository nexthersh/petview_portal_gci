<?php

use Illuminate\Database\Seeder;

class AlteredStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('altered_status')->insert(
        	[
        		'name' => 'Spayed'
        	]);
        DB::table('altered_status')->insert(
        	[
        		'name' => 'Neutered'
        	]);
        DB::table('altered_status')->insert(
        	[
        		'name' => 'Unknown'
        	]);
        DB::table('altered_status')->insert(
            [
                'name' => 'Unaltered'
            ]);
    }
}
