<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Admin',
        	'email' => 'admin@petview.info',
        	'password' => Hash::make('petview'),
        	'role' => 'admin',
        ]);

        User::create([
        	'name' => 'User',
        	'email' => 'user@petview.info',
        	'password' => Hash::make('petview'),
        	'role' => 'user',
        ]);

        User::create([
        	'name' => 'Scott',
        	'email' => 'scott.hallihan@petviewdx.com',
        	'password' => Hash::make('petview'),
        	'role' => 'user',
        ]);
    }
}
