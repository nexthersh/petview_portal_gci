<?php

use Illuminate\Database\Seeder;
use App\BodyView;

class BodyViewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BodyView::create([
        	'name' => 'Lateral',
        ]);

        BodyView::create([
        	'name' => 'Ventrodorsal',
        ]);

        BodyView::create([
        	'name' => 'Dorsoventral',
        ]);

        BodyView::create([
            'name' => 'Orthogonal Lateral',
        ]);
    }
}
