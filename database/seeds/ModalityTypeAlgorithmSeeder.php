<?php

use Illuminate\Database\Seeder;
use App\ ModalityTypeAlgorithm;

class ModalityTypeAlgorithmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModalityTypeAlgorithm::create([
        	'modality_type_id' => 1,
        	'algorithm_id' => 1
        ]);

        ModalityTypeAlgorithm::create([
        	'modality_type_id' => 1,
        	'algorithm_id' => 2
        ]);
        ModalityTypeAlgorithm::create([
            'modality_type_id' => 2,
            'algorithm_id' => 3
        ]);
    }
}
