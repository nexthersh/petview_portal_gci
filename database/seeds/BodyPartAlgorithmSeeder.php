<?php

use Illuminate\Database\Seeder;
use App\BodyPartAlgorithm;

class BodyPartAlgorithmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BodyPartAlgorithm::create([
        	'body_part_id' => 2,
        	'algorithm_id' => 2,
            'for_no_processing_case' => 0,
        ]);

        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 1,
            'for_no_processing_case' => 0,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 1,
            'for_no_processing_case' => 0,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 3,
            'for_no_processing_case' => 0,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 4,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 2,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 1,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 5,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 6,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 5,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 6,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 6,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 7,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 8,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 9,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 9,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 10
        ]);BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 10,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 5,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 6,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 11,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 1,
            'algorithm_id' => 12,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 1,
            'algorithm_id' => 13,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 13,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 5,
            'algorithm_id' => 13,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 6,
            'algorithm_id' => 13,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 13,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 14,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 5,
            'algorithm_id' => 14,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 6,
            'algorithm_id' => 14,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 14,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 14,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 3,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 5,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 6,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 15,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 1,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 2,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 3,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 4,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 5,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 6,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 7,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);
        BodyPartAlgorithm::create([
            'body_part_id' => 8,
            'algorithm_id' => 18,
            'for_no_processing_case' => 1,
        ]);

    }
}
