<?php

use Illuminate\Database\Seeder;
use App\Algorithm;

class AlgorithmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Algorithm::create([
        	'name' => 'Cranial Cruciate Ligament Tear',
        	'suffix' => 'ACL',
        	'description' => 'The algorithms in this use case will analyze the tissue in the cropped area of the x-ray using machine learning to make a determination on whether surrounding tissue is consistent with a partial or complete Cranial Cruciate Ligament tear.',
        	'required' => 1,
        	'restricted' => 1 
        ]);
        Algorithm::create([
            'name' => 'Radiograph Optimization - Experimental',
            'suffix' => 'DV',
            'description' => 'Radiograph Optimization - Experimental',
            'required' => 0,
            'restricted' => 1 
        ]);
        Algorithm::create([
        	'name' => 'SPLENIC MASS',
        	'suffix' => 'SPLENIC_MASS_1',
        	'description' => null,
        	'required' => 0,
        	'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Abdomen Contrast',
            'suffix' => 'Abdomen Contrast',
            'description' => 'Abdomen Contrast',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Fibrosarcoma',
            'suffix' => 'Fibrosarcoma',
            'description' => 'Fibrosarcoma',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Intestinal Thickening',
            'suffix' => 'Intestinal Thickening',
            'description' => 'Intestinal Thickening',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Liver Mass',
            'suffix' => 'LIVER_MASS',
            'description' => 'Liver Mass',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Lung Mass',
            'suffix' => 'Lung Mass',
            'description' => 'Lung Mass',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Lymph Node',
            'suffix' => 'Lymph Node',
            'description' => 'Lymph Node',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Mammary Carcinoma',
            'suffix' => 'Mammary Carcinoma',
            'description' => 'Mammary Carcinoma',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Mast Cell Tumor',
            'suffix' => 'Mast Cell Tumor',
            'description' => 'Mast Cell Tumor',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Nasal Carcinomas',
            'suffix' => 'Nasal Carcinomas',
            'description' => 'Nasal Carcinomas',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Osteosarcoma',
            'suffix' => 'Osteosarcoma',
            'description' => 'Osteosarcoma',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Soft Tissue Sarcoma',
            'suffix' => 'Soft Tissue Sarcoma',
            'description' => 'Soft Tissue Sarcoma',
            'required' => 0,
            'restricted' => 0 
        ]);
        Algorithm::create([
            'name' => 'Subcutaneous Tumor',
            'suffix' => 'Subcutaneous Tumor',
            'description' => 'Subcutaneous Tumor',
            'required' => 0,
            'restricted' => 0 
        ]);

        Algorithm::create([
            'name' => 'Bone Visualization - Experimental',
            'suffix' => 'BONE',
            'description' => 'Bone Visualization - Experimental',
            'required' => 0,
            'restricted' => 0 
        ]);

    }
}
