<?php

use Illuminate\Database\Seeder;

class CaseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('case_type')->insert(
        	[
        		'name' => 'Normal'
        	]);
        DB::table('case_type')->insert(
        	[
        		'name' => 'Upload Data'
        	]);
        DB::table('case_type')->insert(
        	[
        		'name' => 'Quick View'
        	]);
    }
}
