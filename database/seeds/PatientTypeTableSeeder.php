<?php

use Illuminate\Database\Seeder;
use App\PatientType;

class PatientTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PatientType::create([
        	'name' => 'Dog'
        ]);

        PatientType::create([
        	'name' => 'Cat'
        ]);

        PatientType::create([
        	'name' => 'Etc.'
        ]);
    }
}
