<?php

use Illuminate\Database\Seeder;
use App\ImageModality;

class ModalityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImageModality::create([
        	'name' => 'X-Ray',
        ]);
        ImageModality::create([
        	'name' => 'Ultrasound',
        ]);
    }
}
