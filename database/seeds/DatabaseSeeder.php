<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(UsecasesTableSeeder::class);
         $this->call(AlgorithmTableSeeder::class);
         $this->call(BodyPartTableSeeder::class);
         $this->call(BodyViewTableSeeder::class);
         $this->call(ImageTypeTableSeeder::class);
         $this->call(ModalityTypeTableSeeder::class);
         $this->call(PatientTypeTableSeeder::class);
         $this->call(BodyViewAlgorithmSeeder::class);
         $this->call(BodyPartAlgorithmSeeder::class);
         $this->call(ModalityTypeAlgorithmSeeder::class);
         $this->call(CaseTypeTableSeeder::class);
         $this->call(AlteredStatusTableSeeder::class);
         $this->call(ImageTypeAlgorithmSeeder::class);
         $this->call(PricingPlanSeeder::class);
         
    }
}
