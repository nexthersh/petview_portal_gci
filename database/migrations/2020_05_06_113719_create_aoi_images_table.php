<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAoiImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aoi_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_name', 191);
            
            $table->unsignedInteger('image_id')->nullable();
            $table->foreign('image_id')
                    ->references('id')->on('images')
                    ->onDelete('cascade');

            $table->string('aoi_type', 191)->nullable();
            $table->text('aoi_coords')->nullable();
            $table->text('aoi_type_object_json')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('aoi_images', function (Blueprint $table) {
            $table->dropForeign('aoi_images_image_id_foreign');
            $table->dropColumn('image_id');
        });
        Schema::dropIfExists('aoi_images');
    }
}
