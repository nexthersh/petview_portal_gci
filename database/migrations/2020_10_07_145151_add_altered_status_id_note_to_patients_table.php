<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlteredStatusIdNoteToPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->Integer('altered_status_id')->unsigned()->index()->nullable();
            $table->foreign('altered_status_id')
                    ->references('id')->on('altered_status')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->text('note')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('patients', function (Blueprint $table) {
            $table->dropForeign('patients_altered_status_id_foreign');
            $table->dropColumn('altered_status_id');
            $table->dropColumn('note');
        });
    }
}
