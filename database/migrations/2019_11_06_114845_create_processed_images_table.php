<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessedImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processed_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 191)->nullable();
            $table->string('etag', 191)->nullable();
            $table->string('sequencer', 191)->nullable();
            $table->string('size', 191)->nullable();

            $table->unsignedInteger('case_id')->nullable();
            $table->foreign('case_id')
                    ->references('id')->on('cases')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processed_images');
        Schema::table('processed_images', function (Blueprint $table) {
            $table->dropForeign('processed_images_case_id_foreign');
            $table->dropColumn('case_id');
        });
    }
}
