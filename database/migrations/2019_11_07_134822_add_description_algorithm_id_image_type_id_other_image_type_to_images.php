<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionAlgorithmIdImageTypeIdOtherImageTypeToImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function(Blueprint $table) {
            $table->text('description')->nullable();
            $table->string('other_image_type')->nullable();

            $table->unsignedInteger('image_type_id')->nullable();
            $table->foreign('image_type_id')
                    ->references('id')
                    ->on('image_type')
                    ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::table('images', function(Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('other_image_type');

            $table->dropForeign('images_image_type_id_foreign');
            $table->dropColumn('image_type_id');

        });
    }
}
