<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClinicIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->Integer('clinic_id')->unsigned()->index()->nullable();
            $table->foreign('clinic_id')
                    ->references('id')->on('clinics')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_clinic_id_foreign');
            $table->dropColumn('clinic_id');
        });
    }
}
