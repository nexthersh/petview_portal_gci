<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);

            $table->unsignedInteger('patient_type_id');
            $table->foreign('patient_type_id')
                    ->references('id')->on('patient_type')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::disableForeignKeyConstraints();
       Schema::table('breeds', function (Blueprint $table) {
        $table->dropForeign('breeds_patient_type_id_foreign');
        $table->dropColumn('patient_type_id');
    });
    }
}
