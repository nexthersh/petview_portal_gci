<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBodyPartIdBodyViewIdAlteredStatusIdToImagesTable extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function(Blueprint $table) {
            
            $table->unsignedInteger('algorithm_id')->nullable();
            $table->foreign('algorithm_id')
                    ->references('id')
                    ->on('algorithm')
                    ->onDelete('set null');

            $table->unsignedInteger('body_part_id')->nullable();
            $table->foreign('body_part_id')
                    ->references('id')
                    ->on('body_part')
                    ->onDelete('set null');

            $table->unsignedInteger('image_modality_id')->nullable();
            $table->foreign('image_modality_id')
                    ->references('id')
                    ->on('modality_type')
                    ->onDelete('set null');

            $table->unsignedInteger('patient_type_id')->nullable();
            $table->foreign('patient_type_id')
                    ->references('id')
                    ->on('patient_type')
                    ->onDelete('set null');

            $table->unsignedInteger('breed_id')->nullable();
            $table->foreign('breed_id')
                    ->references('id')
                    ->on('breeds')
                    ->onDelete('set null');

            $table->unsignedInteger('altered_status_id')->nullable();
            $table->foreign('altered_status_id')
                    ->references('id')
                    ->on('altered_status')
                    ->onDelete('set null');

            $table->unsignedInteger('body_view_id')->nullable();
            $table->foreign('body_view_id')
                    ->references('id')
                    ->on('body_view')
                    ->onDelete('set null');

            $table->string('gender', 191)->nullable();
            $table->string('age', 191)->nullable();
            $table->text('diagnosis')->nullable();
            $table->text('differential_diagnosis')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::table('images', function(Blueprint $table) {

            $table->dropForeign('cases_algorithm_id_foreign');
            $table->dropColumn('algorithm_id');
            
        });
    }
}
