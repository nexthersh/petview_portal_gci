<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlteredStatusIdBodyPartIdToCases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table) {
            
            $table->unsignedInteger('altered_status_id')->nullable();
            $table->foreign('altered_status_id')
                    ->references('id')
                    ->on('altered_status')
                    ->onDelete('set null');

            $table->unsignedInteger('body_part_id')->nullable();
            $table->foreign('body_part_id')
                    ->references('id')
                    ->on('body_part')
                    ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::table('cases', function(Blueprint $table) {

            $table->dropForeign('cases_altered_status_id_foreign');
            $table->dropColumn('altered_status_id');
            
            $table->dropForeign('cases_body_part_id_foreign');
            $table->dropColumn('body_part_id');
        });
    }
}
