<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);

            $table->unsignedInteger('client_id');
            $table->foreign('client_id')
                    ->references('id')->on('clients')
                    ->onDelete('cascade');

            $table->unsignedInteger('patient_type_id');
            $table->foreign('patient_type_id')
                    ->references('id')->on('patient_type')
                    ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('patients', function (Blueprint $table) {
            $table->dropForeign('patients_client_id_foreign');
            $table->dropForeign('patients_patient_type_id_foreign');
            $table->dropColumn('client_id');
            $table->dropColumn('patient_type_id');
        });
    }
}
