<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlgorithmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('algorithm', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->string('suffix', 191);
            $table->text('description')->nullable();
            $table->tinyInteger('required')->default(0);
            $table->tinyInteger('restricted')->default(0);

            $table->unsignedInteger('usecase_id')->nullable();
            $table->foreign('usecase_id')
                    ->references('id')
                    ->on('usecases')
                    ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('algorithm');
    }
}
