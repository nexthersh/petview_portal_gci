<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyviewAlgorithmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('bodyview_algorithm', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('body_view_id')->nullable();
            $table->foreign('body_view_id')
                    ->references('id')->on('body_view')
                    ->onDelete('restrict');

            $table->unsignedInteger('algorithm_id')->nullable();
            $table->foreign('algorithm_id')
                    ->references('id')->on('algorithm')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bodyview_algorithm');
    }
}
