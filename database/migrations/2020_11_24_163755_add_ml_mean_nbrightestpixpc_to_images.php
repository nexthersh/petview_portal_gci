<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMlMeanNbrightestpixpcToImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function(Blueprint $table) {
            $table->text('ml')->nullable();
            $table->string('mean')->nullable();
            $table->string('nbrightestpixpc')->nullable();
            $table->text('algversion')->nullable();
            $table->text('mdlversion')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function(Blueprint $table) {
            $table->dropColumn('ml');
            $table->dropColumn('mean');
            $table->dropColumn('nbrightestpixpc');
            $table->dropColumn('algversion');
            $table->dropColumn('mdlversion');
        });
    }
}
