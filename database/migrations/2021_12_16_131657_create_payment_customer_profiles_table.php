<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentCustomerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_customer_profiles', function (Blueprint $table) {
            $table->id();
            $table->Integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->Integer('clinic_id')->unsigned()->index()->nullable();
            $table->foreign('clinic_id')
                  ->references('id')->on('clinics')->onDelete('set nill');

            $table->string('customer_profile_id', 191);
            $table->string('customer_payment_profile_id', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropForeign('payment_customer_profiles_user_id_foreign');
        Schema::dropForeign('payment_customer_profiles_clinic_id_foreign');
        Schema::dropIfExists('payment_customer_profiles');
    }
}
