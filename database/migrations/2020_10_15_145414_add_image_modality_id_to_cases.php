<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageModalityIdToCases extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table) {
            
            $table->unsignedInteger('image_modality_id')->nullable();
            $table->foreign('image_modality_id')
                    ->references('id')
                    ->on('modality_type')
                    ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::table('cases', function(Blueprint $table) {

            $table->dropForeign('cases_image_modality_id_foreign');
            $table->dropColumn('image_modality_id');
            
        });
    }
}
