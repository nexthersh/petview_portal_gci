<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagetypeAlgorithmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagetype_algorithm', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('image_type_id')->nullable();
            $table->foreign('image_type_id')
                    ->references('id')->on('image_type')
                    ->onDelete('restrict');

            $table->unsignedInteger('algorithm_id')->nullable();
            $table->foreign('algorithm_id')
                    ->references('id')->on('algorithm')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('imagetype_algorithm', function (Blueprint $table) {
            $table->dropForeign('imagetype_algorithm_image_type_id_foreign');
            $table->dropColumn('image_type_id');

            $table->dropForeign('imagetype_algorithm_algorithm_id_foreign');
            $table->dropColumn('algorithm_id');
        });
        Schema::dropIfExists('imagetype_algorithm');
    }
}
