<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlgorithmIdToCases extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table) {
            
            $table->unsignedInteger('algorithm_id')->nullable();
            $table->foreign('algorithm_id')
                    ->references('id')
                    ->on('algorithm')
                    ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::table('cases', function(Blueprint $table) {

            $table->dropForeign('cases_algorithm_id_foreign');
            $table->dropColumn('algorithm_id');
            
        });
    }
}
