<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBillingInfoToPaymentCustomerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_customer_profiles', function (Blueprint $table) {
            $table->string('billing_first_name', 191)->nullable();
            $table->string('billing_last_name', 191)->nullable();
            $table->string('billing_address', 191)->nullable();
            $table->string('billing_credit_card', 191)->nullable();
            $table->string('billing_expire_date', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_customer_profiles', function (Blueprint $table) {
            $table->dropColumn('billing_first_name');
            $table->dropColumn('billing_last_name');
            $table->dropColumn('billing_address');
            $table->dropColumn('billing_credit_card');
            $table->dropColumn('billing_expire_date');
        });
    }
}
