<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalitytypeAlgorithmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('modalitytype_algorithm', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('modality_type_id')->nullable();
            $table->foreign('modality_type_id')
                    ->references('id')->on('modality_type')
                    ->onDelete('restrict');

            $table->unsignedInteger('algorithm_id')->nullable();
            $table->foreign('algorithm_id')
                    ->references('id')->on('algorithm')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modalitytype_algorithm');
    }
}
