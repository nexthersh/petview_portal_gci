<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBreedIdToPatients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->Integer('breed_id')->unsigned()->index();
            $table->foreign('breed_id')
                    ->references('id')->on('breeds')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('patients', function (Blueprint $table) {
            $table->dropForeign('patients_breed_id_foreign');
            $table->dropColumn('breed_id');
        });
    }
}
