<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaseTypeIdToCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table) {
            $table->unsignedInteger('case_type_id');
            
            $table->foreign('case_type_id')
                  ->references('id')
                  ->on('case_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::table('cases', function(Blueprint $table) {
            $table->dropForeign('cases_case_type_id_foreign');
            $table->dropColumn('case_type_id');
        });
    }
}
