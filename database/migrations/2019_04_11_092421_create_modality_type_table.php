<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalityTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modality_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);

          /*  $table->unsignedInteger('algorithm_id')->nullable();
            $table->foreign('algorithm_id')
                    ->references('id')->on('algorithm')
                    ->onDelete('restrict');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modality_type');
    }
}
