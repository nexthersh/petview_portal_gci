<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForNoProcessingCaseToBodypartAlgorithmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bodypart_algorithm', function(Blueprint $table){
            $table->tinyInteger('for_no_processing_case')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('bodypart_algorithm', function(Blueprint $table){
            $table->dropColumn('for_no_processing_case');
        });
    }
}
