<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyPartTable extends Migration
{
    /**
     * Run the migrations...
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_part', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->tinyInteger('restricted')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_part');
    }
}
