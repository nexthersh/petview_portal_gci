<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('subscription_id', 191);
            $table->string('subscription_name', 191);
            $table->string('customer_profile_id', 191);
            $table->string('customer_payment_profile_id', 191);
            $table->double('amount');
            $table->string('status', 191);
            $table->timestamp('start_date');

            $table->Integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropForeign('payment_subscriptions_user_id_foreign');
        Schema::dropIfExists('payment_subscriptions');
    }
}
