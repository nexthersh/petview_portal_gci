<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentUserPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_user_plan', function (Blueprint $table) {
            $table->id();

            $table->Integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->Integer('clinic_id')->unsigned()->index()->nullable();
            $table->foreign('clinic_id')
                ->references('id')->on('clinics');


            $table->bigInteger('plan_id')->unsigned()->index()->nullable();
            $table->foreign('plan_id')
                ->references('id')->on('pricing_plans')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropForeign('payment_user_plan_user_id_foreign');
        Schema::dropForeign('payment_user_plan_clinic_id_foreign');
        Schema::dropForeign('payment_user_plan_plan_id_foreign');
        Schema::dropIfExists('payment_user_plan');
    }
}
