<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferentImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referent_images', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->string('aoi_type', 191)->nullable();
            $table->string('image_input_type', 191)->nullable();
            
            $table->unsignedInteger('algorithm_id');
            $table->foreign('algorithm_id')
                  ->references('id')
                  ->on('algorithm')
                  ->onDelete('cascade');

            $table->unsignedInteger('image_type_id');
            $table->foreign('image_type_id')
                  ->references('id')
                  ->on('image_type')
                  ->onDelete('cascade');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();       

        Schema::table('referent_images', function (Blueprint $table) {
            $table->dropForeign('referent_images_algorithm_id_foreign');
            $table->dropColumn('algorithm_id');

            $table->dropForeign('referent_images_image_type_id_foreign');
            $table->dropColumn('image_type_id');
        });
        Schema::dropIfExists('referent_images');    }
}
