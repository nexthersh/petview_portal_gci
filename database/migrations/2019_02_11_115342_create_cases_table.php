<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            $table->text('notes')->nullable();

            $table->unsignedInteger('patient_id')->nullable();
            $table->foreign('patient_id')
                    ->references('id')->on('patients')
                    ->onDelete('cascade');

            $table->unsignedInteger('client_id')->nullable();
            $table->foreign('client_id')
                    ->references('id')->on('clients')
                    ->onDelete('cascade');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function(Blueprint $table) {
            $table->dropForeign('cases_patient_id_foreign');
            $table->dropColumn('patient_id');

            $table->dropForeign('cases_client_id_foreign');
            $table->dropColumn('client_id');

            $table->dropForeign('cases_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('cases');
    }
}
