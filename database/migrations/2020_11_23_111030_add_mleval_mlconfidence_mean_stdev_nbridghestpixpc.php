<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMlevalMlconfidenceMeanStdevNbridghestpixpc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aoi_images', function(Blueprint $table) {
            $table->text('ml', 191)->nullable();
            $table->string('stdev', 191)->nullable();
            $table->string('mean', 191)->nullable();
            $table->string('nbrightestpixpc', 191)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aoi_images', function(Blueprint $table) {
            $table->dropColumn('ml');
            $table->dropColumn('stdev');
            $table->dropColumn('mean');
            $table->dropColumn('nbrightestpixpc');
        });
    }
}
