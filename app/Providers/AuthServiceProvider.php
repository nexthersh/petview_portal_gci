<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Client;
use App\Patient;
use App\User;
use App\Clinic;
use App\CaseModel;
use App\Policies\ClientPolicy;
use App\Policies\PatientPolicy;
use App\Policies\UserPolicy;
use App\Policies\ClinicPolicy;
use App\Policies\CasePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Client::class => ClientPolicy::class,
        Patient::class => PatientPolicy::class,
        User::class => UserPolicy::class,
        Clinic::class => ClinicPolicy::class,
        CaseModel::class => CasePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
