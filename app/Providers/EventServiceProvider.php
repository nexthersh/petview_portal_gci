<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Registered::class => [
        //     SendEmailVerificationNotification::class,
        // ],
        'App\Events\NewRegistrationRequest' => [
            'App\Listeners\NotificationForNewRegistrationRequest',
        ],
        'App\Events\RejectedApprovedRegistrationRequest' => [
            'App\Listeners\NotificationForRejectedApprovedRegistrationRequest',
        ],
        'App\Events\SendEmailVerification' => [
            'App\Listeners\NotificationSendEmailVerification',
        ],
        'App\Events\SetPasswordProcessed' => [
            'App\Listeners\SendSetPasswordEmail',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
