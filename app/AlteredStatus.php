<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlteredStatus extends Model
{
    public $timestamps = false;
    protected $table = 'altered_status';
}
