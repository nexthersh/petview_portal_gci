<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Images;
use App\AoiImage;
use App\ImageAlgorithm;

class CaseModel extends Model
{
    
    protected $table = 'cases';
    protected $appends = ["ml","stdev"];
    public static $preventAttrSet = true;
    protected $guarded = [];

    public function images(){

    	return $this->hasMany('App\Images', 'case_id', 'id');
    }

    public function client() {
    	return $this->belongsTo('App\Client');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function algorithm() {
    
         return $this->belongsTo('App\Algorithm');
    }

    public function bodyPart() {
    
         return $this->belongsTo('App\BodyPart');
    }

    public function imageModality() {
    
         return $this->belongsTo('App\ImageModality');
    }

    public function alteredStatus() {
    
         return $this->belongsTo('App\AlteredStatus');
    }

    public function caseType() {
    
         return $this->belongsTo('App\CaseType');
    }

    public function audits()
    {
        return $this->morphMany('App\Audit', 'auditable');
    }

    public function getMlAttribute()
    {
        if(self::$preventAttrSet){

            $result = [];
            $ml = Images::leftjoin('aoi_images', 'aoi_images.image_id', 'images.id')
                         ->select('images.ml as imagesMl', 'aoi_images.ml as aoiimagesMl')->where('case_id', $this->id)->get()->toArray();
             // dd($ml);
            foreach ($ml as $key => $value) {
                // dd($value);
                $ml = $value['imagesMl'] ? json_decode($value['imagesMl']) : json_decode($value['aoiimagesMl']);

                /*if (@$ml->eval2->eval ) {
                    $result[$ml->eval2->eval][]['confidence'] = $ml->eval2->confidence;
                }elseif(@$ml->eval->eval){
                    $result[$ml->eval->eval][]['confidence'] = $ml->eval->confidence;
                }*/
                if (@$ml->eval->eval ) {
                    $result[$ml->eval->eval][]['confidence'] = $ml->eval->confidence;
                }
            }
            
            if (isset($result['BENIGN'])) {
                $mleval['eval'] = 'BENIGN';
                $mleval['confidence'] = collect($result['BENIGN'])->sortByDesc('confidence')->pluck('confidence')->first(); 

            }elseif (isset($result['MALIGNANT'])) {
                $mleval['eval'] = 'MALIGNANT';
                $mleval['confidence'] = collect($result['MALIGNANT'])->sortByDesc('confidence')->pluck('confidence')->first(); 
            }elseif (isset($result['NORMAL'])) {
                $mleval['eval'] = 'NORMAL';
                $mleval['confidence'] = collect($result['NORMAL'])->sortByDesc('confidence')->pluck('confidence')->first();
            }else{
                $mleval['eval'] = null;
                $mleval['confidence'] = null;
            }
            
            if (isset($result['ABNORMAL'])) {
                $mleval['eval'] = 'ABNORMAL';
                $mleval['confidence'] = collect($result['ABNORMAL'])->sortByDesc('confidence')->pluck('confidence')->first(); 

            }elseif (isset($result['NORMAL'])) {
                $mleval['eval'] = 'NORMAL';
                $mleval['confidence'] = collect($result['NORMAL'])->sortByDesc('confidence')->pluck('confidence')->first();
            }else{
                $mleval['eval'] = null;
                $mleval['confidence'] = null;
            }
            
            // dd($mleval);
            return $mleval;

            /*return Images::select('ml_confidence', 'ml_eval')
                    ->where('case_id', $this->id)
                    ->where('ml_confidence', (Images::select(DB::raw('max(ml_confidence) as confidence'))->where('case_id', $this->id)->first()->toArray()['confidence']))
                    ->first();*/
        }
    }

    public function getStdevAttribute()
    {
        if(self::$preventAttrSet){

            $result = [];
            $stdev = Images::leftjoin('aoi_images', 'aoi_images.image_id', 'images.id')
                         ->select('images.stdev as imagesStdev', 'aoi_images.stdev as aoiimagesStdev')->where('case_id', $this->id)->get()->toArray();
            foreach ($stdev as $key => $value) {
                $result[]['stdev'] = $value['imagesStdev'] ? $value['imagesStdev'] : $value['aoiimagesStdev'];
            }
            
            $result = collect($result)->sortByDesc('stdev')->pluck('stdev')->first();
            
            return $result;


            // return Images::select('stdev')->where('case_id', $this->id)->whereNotNull('stdev')->get();
            /*return Images::select('ml_confidence', 'ml_eval')
                    ->where('case_id', $this->id)
                    ->where('ml_confidence', (Images::select(DB::raw('max(ml_confidence) as confidence'))->where('case_id', $this->id)->first()->toArray()['confidence']))
                    ->first();*/
        }
    }
}
