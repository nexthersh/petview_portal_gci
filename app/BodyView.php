<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyView extends Model
{
    public $timestamps = false;
    protected $table = 'body_view';
}
