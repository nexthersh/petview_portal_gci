<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCustomerProfile extends Model
{
    public $guarded = [];
    public $appends = ['profileId', 'paymentProfileId'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function clinic()
    {
        return $this->belongsTo('App\Clinic', 'clinic_id');
    }

    public function getProfileIdAttribute()
    {
        return $this->customer_profile_id;
    }

    public function getPaymentProfileIdAttribute()
    {
        return $this->customer_payment_profile_id;
    }
}
