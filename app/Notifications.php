<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    public $timestamps = true;

    protected $guarded=[];

}
