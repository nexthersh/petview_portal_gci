<?php

namespace App\Policies;

use App\User;
use App\Clinic;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClinicPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function any(User $user)
    {
        return $user->role === 'admin';
    }
}
