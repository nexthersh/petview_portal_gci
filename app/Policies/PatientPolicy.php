<?php

namespace App\Policies;

use App\User;
use App\Patient;
use Illuminate\Auth\Access\HandlesAuthorization;

class PatientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Patient $patient)
    {
        if ($user->id == $patient->client->user_id OR $user->role == 'admin' OR ($user->role == 'clinic_admin' AND $user->clinic_id == $patient->client->user->clinic_id)) {
            return true;
        }
    }
}
