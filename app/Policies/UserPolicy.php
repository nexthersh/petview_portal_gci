<?php

namespace App\Policies;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function create(User $authUser)
    {
        if ($authUser->role == 'admin' OR ($authUser->role == 'clinic_admin' AND $authUser->clinic_id == request('clinic_id'))) {
            return true;
        }
    }

    public function show(User $authUser, User $user)
    {
        if ($authUser->id == $user->id OR $authUser->role == 'admin' OR ($authUser->role == 'clinic_admin' AND $authUser->clinic_id == $user->clinic_id)) {
            return true;
        }
    }

    public function delete(User $authUser, User $user)
    {
        if ($authUser->role == 'admin' OR ($authUser->role == 'clinic_admin' AND $authUser->clinic_id == $user->clinic_id)) {
            return true;
        }
    }

    public function update(User $authUser, User $user)
    {
        if ($authUser->id == $user->id OR $authUser->role == 'admin' OR ($authUser->role == 'clinic_admin' AND $authUser->clinic_id == $user->clinic_id)) {
            return true;
        }
    }

    public function get(User $user)
    {
        if($user->role == 'admin' OR $user->role == 'clinic_admin'){
            return true;
        }
    }
}
