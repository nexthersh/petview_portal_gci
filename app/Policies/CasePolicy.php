<?php

namespace App\Policies;

use App\User;
use App\CaseModel;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, CaseModel $case)
    {
        if ($user->id == $case->user_id OR $user->role == 'admin' OR ($user->role == 'clinic_admin' AND $user->clinic_id == $case->user->clinic_id)) {
            return true;
        }
    }

    public function update(User $user, CaseModel $case)
    {
        if ($user->id == $case->user_id OR $user->role == 'admin' OR ($user->role == 'clinic_admin' AND $user->clinic_id == $case->user->clinic_id)) {
            return true;
        }
    }

}
