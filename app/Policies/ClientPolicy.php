<?php

namespace App\Policies;

use App\User;
use App\Client;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Client $client)
    {
        if ($user->id == $client->user_id OR $user->role == 'admin' OR ($user->role == 'clinic_admin' AND $user->clinic_id == $client->user->clinic_id)) {
            return true;
        }
    }
}
