<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModalityTypeAlgorithm extends Model
{
    public $timestamps = false;
    protected $table = 'modalitytype_algorithm';
}
