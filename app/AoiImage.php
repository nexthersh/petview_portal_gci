<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AoiImage extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $appends = ["ml"];

    public function getMlAttribute($value)
    {
    	return json_decode($value);
    }
}
