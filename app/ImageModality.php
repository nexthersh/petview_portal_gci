<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageModality extends Model
{
    public $timestamps = false;
    protected $table = 'modality_type';
}
