<?php

namespace App;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Client;
use App\Patient;
use App\PaymentUserPlan;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'email', 'password','role', 'title', 'phone', 'address', 'active', 'clinic_id', 'trial_start_date', 'email_verified_at', 'verify_token', 'verify_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verify_code', 'email_verified_at'
    ];

    protected $appends = ['name', 'clients', 'patients', 'isTrial', 'primaryOwner', 'availableCases', 'plan', 'billingInfo'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function cases()
    {
        return $this->hasMany('App\CaseModel');
    }

    public function clinic()
    {
        return $this->belongsTo('App\Clinic');
    }

    public function getNameAttribute()
    {
        return $this->title.$this->first_name.($this->middle_name ? ' '.$this->middle_name : ''). ' '.$this->last_name;
    }

    public function getClientsAttribute()
    {
        return $this->getClients()->count();
    }

    public function getPatientsAttribute()
    {
        return $this->getPatients()->count();
    }

    private function getClients()
    {
        return Client::where('user_id', $this->id)->get();
    }

    private function getPatients()
    {
        return Patient::whereIn('client_id', $this->getClients()->pluck('id'))->get();
    }

    public function audits()
    {
        return $this->morphMany('App\Audit', 'auditable');
    }

    public function getIsTrialAttribute()
    {
        return $this->role === 'trial_user';
    }

    public function getPrimaryOwnerAttribute()
    {
        $owner = PaymentCustomerProfile::where('user_id', $this->id)->first();
        if ($owner || in_array($this->role, ['trial_user', 'admin'])) {
            return true;
        }
        return false;
    }

    public function getAvailableCasesAttribute()
    {
        if (in_array($this->role, ['trial_user', 'admin'])) {
            return 1;
        }
        $clinic = Clinic::where('id', $this->clinic_id)->first();
        return $clinic ? $clinic->available_cases : 0;
    }

    public function getPlanAttribute()
    {
        $paymentPlanId = PaymentUserPlan::where('clinic_id', $this->clinic_id)->first();
        if (!$paymentPlanId) {
            return null;
        }
        return PricingPlan::where('id', @$paymentPlanId->plan_id)->first();
    }

    public function getBillingInfoAttribute()
    {
        return PaymentCustomerProfile::where('user_id', $this->id)->first();
    }

}
