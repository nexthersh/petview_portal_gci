<?php

namespace App\Listeners;

use App\Events\NewRegistrationRequest;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\User;
use Mail;
use App\Mail\RegistrationRequestNotifications;

class NotificationForNewRegistrationRequest implements ShouldQueue
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRegistrationRequest  $event
     * @return void
     */
    public function handle(NewRegistrationRequest $event)
    {
        $emails = [];
        
        $emails = User::where('role', 'admin')
                        ->orWhere(function($query) use ($event){
                        $query->where('role', 'clinic_admin');
                        $query->where('clinic_id', $event->user->clinic_id);
                        })->get()->pluck('email')->toArray();
        
        Mail::to($emails)->send(new RegistrationRequestNotifications());
    }
}
