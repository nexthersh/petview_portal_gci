<?php

namespace App\Listeners;

use App\Events\SetPasswordProcessed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\SetPasswordNotifications;
use Mail;
use Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SendSetPasswordEmail implements ShouldQueue
{

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SetPasswordProcessed  $event
     * @return void
     */
    public function handle(SetPasswordProcessed $event)
    {
        $email = $event->user->email;
        $token = Str::random(40);
        $resetPassword = DB::table('password_resets')->updateOrInsert(['email' => $email], ['email' => $email, 'token' => $token, 'created_at' => Carbon::now()]);

        Mail::to($email)->send(new SetPasswordNotifications($token, $email));
        
    }
}
