<?php

namespace App\Listeners;

use App\Events\RejectedApprovedRegistrationRequest;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\RejectedApprovedRegistrationRequestNotifications;
use App\User;
use Mail;

class NotificationForRejectedApprovedRegistrationRequest implements ShouldQueue
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RejectedApprovedRegistrationRequest  $event
     * @return void
     */
    public function handle(RejectedApprovedRegistrationRequest $event)
    {
        $status = $event->user->active ? 'approved' : 'denied';
        Mail::to($event->user->email)->send(new RejectedApprovedRegistrationRequestNotifications($status));
    }
}
