<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\SendEmailVerification;
use App\Mail\SendEmailVerificationNotifications;
use Mail;


class NotificationSendEmailVerification implements ShouldQueue
{

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmailVerification  $event
     * @return void
     */
    public function handle(SendEmailVerification $event)
    {
        $code = $event->user->verify_code;
        $link = url('/#/verify/'.$event->user->verify_token.'?code='.$code);
        Mail::to($event->user->email)->send(new SendEmailVerificationNotifications($link, $code));
    }
}
