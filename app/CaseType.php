<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseType extends Model
{
    protected $table = 'case_type';
}
