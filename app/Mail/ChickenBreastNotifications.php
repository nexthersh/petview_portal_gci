<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChickenBreastNotifications extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;
    public $subject;

    public function __construct($data, $subject = null)
    {
        $this->data = $data;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject($this->subject)
                    ->view('email.chicken-breast-notifications')
                    ->with('data', $this->data)
                    ->attach(storage_path('app/closed-case/'.$this->subject.'.json'));
    }
}
