<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailVerificationNotifications extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;
    public $link;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($link, $code)
    {
        $this->link = $link;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PetView - Verify Email')
                    ->with('link', $this->link)
                    ->with('code', $this->code)
                    ->view('email.verify-email');
    }
}
