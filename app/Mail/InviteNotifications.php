<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteNotifications extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;
    public $inviteUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inviteUser)
    {
        $this->inviteUser = $inviteUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = url('/#/invitation/'.$this->inviteUser->verify_token);
        return $this->subject("You've been invited to join the PetView")->view('email.invite')->with('link', $link);
    }
}
