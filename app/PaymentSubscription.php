<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentSubscription extends Model
{
    public $guarded = [];
}
