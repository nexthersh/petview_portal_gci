<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentUserPlan extends Model
{
    public $table = "payment_user_plan";
    public $guarded = [];
    public $timestamps = false;


    public function plan()
    {
        return $this->belongsTo('App\PricingPlan', 'plan_id');
    }
}
