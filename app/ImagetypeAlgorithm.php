<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagetypeAlgorithm extends Model
{
    public $table = 'imagetype_algorithm';

    public function imagetype()
    {
    	return $this->belongsTo('App\ImageType', 'image_type_id');
    }
}
