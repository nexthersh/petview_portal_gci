<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Algorithm extends Model
{
    public $timestamps = false;
    protected $table = 'algorithm';

    public function referentImage()
    {
    	return $this->hasMany('App\ReferentImage');
    }

    public function imageModality()
    {
    	return $this->belongsToMany('App\ImageModality', 'modalitytype_algorithm', 'algorithm_id', 'modality_type_id');
    }

    public function getSuffixAttribute($value)
    {
        return strtoupper($value);
    }
}
