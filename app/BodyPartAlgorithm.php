<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyPartAlgorithm extends Model
{
    public $timestamps = false;
    protected $table = 'bodypart_algorithm';

    public function bodypart()
    {
    	return $this->belongsTo('App\BodyPart', 'body_part_id');
    }
}
