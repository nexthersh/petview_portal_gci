<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessedImage extends Model
{
	protected $table = 'processed_images';
    protected $fillable = ['key', 'etag', 'sequencer', 'size', 'case_id', 'created_at', 'updated_at'];
}
