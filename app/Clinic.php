<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Client;
use App\Patient;

class Clinic extends Model
{

    protected $guarded = [];
    protected $appends = ['last_active', 'clinic_users', 'clinic_clients', 'clinic_patients'];

    public function user()
    {
    	return $this->hasMany('App\User');
    }

    public function getLastActiveAttribute()
    {
    	$lastActive = User::where('clinic_id', $this->id)->orderBy('updated_at', 'desc')->first();
    	if ($lastActive) {
    		$lastActive = $lastActive->updated_at->toDateTimeString() ? $lastActive->updated_at->toDateTimeString() : '';
    		return $lastActive;
    	}

    	return $this->updated_at->toDateTimeString();
    }

    public function getClinicUsersAttribute()
    {
        return $this->getUsers()->count();
    }

    public function getClinicClientsAttribute()
    {
        return $this->getClients()->count();
    }

    public function getClinicPatientsAttribute()
    {
        return $this->getPatients()->count();
    }

    private function getUsers()
    {
        return User::where('clinic_id', $this->id)->get();
    }

    private function getClients()
    {
        return Client::whereIn('user_id', $this->getUsers()->pluck('id'))->get();
    }

    private function getPatients()
    {
        return Patient::whereIn('client_id', $this->getClients()->pluck('id'))->get();
    }

    public function audits()
    {
        return $this->morphMany('App\Audit', 'auditable');
    }
}
