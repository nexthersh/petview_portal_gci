<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        if (in_array(Auth::user()->role, $role)) {
            return $next($request);
        }
         return response()->json(['status' => 401, 'message' => 'This action is unauthorized', 'timestamp' => Carbon::now()->toDateTimeString()], 401);
    }
}
