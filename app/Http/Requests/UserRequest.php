<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    protected function failedValidation(Validator $validator) { 
        
        throw new HttpResponseException(response()->json($validator->errors(), 422)); 
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:191',
            'middle_name' => 'nullable|string|max:191',
            'last_name' => 'required|string|max:191',
            'title' => 'nullable|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.request()->route('id'),
            'password' => 'sometimes|string|min:6|confirmed',
            'clinic_id' => 'nullable|exists:clinics,id',
            'role' => 'sometimes|in:clinic_admin,user'
        ];
    }
}
