<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;

class CaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'client_name' => 'required|string',
                'patient_name' => 'required|string',
                'patient_type_id' => 'required|Integer',
                'notes' => 'string|nullable',
                'diagnosis' => 'string|nullable',
                'images' => 'required',
                'images.*' => 'image|mimes:jpeg,jpg,png,dcm,tiff'
        ];
    }
    
        /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            
            //
        ];
    }

   protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(response()->json($validator->errors(), 422)); 
    }
}
