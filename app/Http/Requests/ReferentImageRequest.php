<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReferentImageRequest extends FormRequest
{
    protected function failedValidation(Validator $validator) { 
        
        throw new HttpResponseException(response()->json($validator->errors(), 422)); 
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'algorithm_id' => 'sometimes|required|exists:algorithm,id',
            'image_type_id' => 'sometimes|required|exists:image_type,id',
            'aoi_type' => 'sometimes|required|string|max:191',
            'image_input_type' => 'sometimes|required|in:Good,Bad',
            'images' => 'required|array'
        ];
    }
}
