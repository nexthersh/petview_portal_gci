<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChoosePlanReguest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:clinic_admin,user',
            'clinic_id' => 'required_if:type,user',
            'billingFirstName' => 'required_if:type,clinic_admin|string|max:20',
            'billingLastName' => 'required_if:type,clinic_admin|string|max:20',
            'billingCardNumber' => 'required_if:type,clinic_admin',
            'billingAddress' => 'required_if:type,clinic_admin|string|max:50',
            'billingExpireDate' => 'required_if:type,clinic_admin|date_format:Y-m',
            'billingCVV' => 'required_if:type,clinic_admin',
            'clinic' => 'required_if:type,clinic_admin|max:191'
        ];
    }
}
