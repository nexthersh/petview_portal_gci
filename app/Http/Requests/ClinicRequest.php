<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use App\User;
use Route;

class ClinicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $clinicId = @$this->route('clinic')->id;

        return [
            'name' => 'required|string|max:191|unique:clinics,name,'.$clinicId,
            'email' => 'required|string|email|max:191|unique:clinics,email,'.$clinicId,
            'password' => 'sometimes|string|min:6|confirmed',
            'discount' => 'numeric|between:0,100',
            'discount_expiry_date' => 'date'
        ];
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(response()->json($validator->errors(), 422)); 
    }
}
