<?php

namespace App\Http\Traits;

use Auth;

trait Audit
{
    public function assignAuditValues($event, $oldValues, $newValues, $request)
    {
    	$insert = [
    			'user_id' => Auth::id(),
    			'event' => $event,
    			'old_values' => is_array($oldValues) ? json_encode($oldValues) : $oldValues,
    			'new_values' => is_array($newValues) ? json_encode($newValues) : $newValues,
    			'url' => $request->url(),
    			'ip_address' => isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $request->ip(),
    			'user_agent' => $request->userAgent()
    	];
        return $insert;
    }
}