<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InviteRequest;
use App\User;
use Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\InviteNotifications;
use Carbon\Carbon;

class InviteController extends Controller
{
    public function sendInvitationLink(InviteRequest $request)
    {
        if ($request->invites && !empty($request->invites)) {
            foreach ($request->invites as $invite) {
                if (filter_var($invite['email'], FILTER_VALIDATE_EMAIL)) {
                    $inviteUser = User::create([
                        'first_name' => $invite['email'],
                        'email' => $invite['email'],
                        'verify_token' => Str::random(50),
                        'role' => 'in_process',
                        'clinic_id' => Auth::user()->clinic_id,
                    ]);

                    Mail::to($invite['email'])->send(new InviteNotifications($inviteUser));
                }
            }
        }

        return response()->json(["message" => "You have successfully sent invitations.", "timestamp" => Carbon::now()->toDateTimeString()], 200);
    }
}
