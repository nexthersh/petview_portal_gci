<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audit;

class AuditController extends Controller
{
    public function index(Request $request)
    {
    	$audits = Audit::with('user')->orderBy('created_at', 'desc')->paginate(10);

    	return response()->json($audits, 200);
    }
}
