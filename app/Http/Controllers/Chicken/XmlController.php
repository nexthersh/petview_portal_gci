<?php

namespace App\Http\Controllers\Chicken;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Log;

class XmlController extends Controller
{
    public static function xml($data, $images, $transactionNumber)
    {
        try {
            //create and open xml
            $xml_header = '<?xml version="1.0" encoding="UTF-8"?><Case></Case>';
            $xml = new \SimpleXMLElement($xml_header);
            $xml->addChild('TransactionNumber', $transactionNumber);
            $xmlImages = $xml->addChild('Images');
            $xmlChickenBreast = $xmlImages->addChild('ChickenBreast');
            $xmlChickenBreast->addChild('Farm', @$data->Farm ? $data->Farm : " ");
            $xmlChickenBreast->addChild('Line', @$data->Line ? $data->Line : " ");
            $xmlChickenBreast->addChild('Email', $data->Email ? $data->Email : " ");
            $xmlChickenBreast->addChild('Operator', @$data->Operator ? $data->Operator : " ");
            $xmlChickenBreast->addChild('Equipment', @$data->Equipment ? $data->Equipment : " ");
            
            foreach ($images as $key => $image) { 
                $xmlImageFileName = $xmlImages->addChild('ImageFileName', $transactionNumber.'_'.$image->getClientOriginalName());
                $xmlImageFileName->addChild('Wingband', $data->Chickens[$key]->Wingband ? $data->Chickens[$key]->Wingband : " ");
                $xmlImageFileName->addChild('BodyWeight', $data->Chickens[$key]->BodyWeight ? $data->Chickens[$key]->BodyWeight : " ");
                // $xmlImageFileName->addChild('Breed', $data->Chickens[$key]->Breed ? $data->Chickens[$key]->Breed : " ");
                $xmlImageFileName->addChild('Gender', $data->Chickens[$key]->Gender ? $data->Chickens[$key]->Gender : " ");
                $xmlImageFileName->addChild('Source', $data->Chickens[$key]->Source ? $data->Chickens[$key]->Source : " ");
                $useCaseXml = $xmlImageFileName->addChild('UseCase', 'WOODY_BREAST');
                $useCaseXml->addAttribute('result', 'VISUAL'); 
            }

            $xml = $xml->saveXml();
            $filename = $transactionNumber;
            $xmlFile = Storage::put('xml/ChickenBreast/'.$filename.'.xml', $xml);
            Log::channel('woody-breast')->info('CONVERSATION STATUS: DONE - '.$transactionNumber);
            return $xmlFile;

        } catch (\Throwable $th) {
            //throw $th;
            Log::channel('woody-breast')->error('CONVERSATION STATUS: '.$transactionNumber.' - '.$th->getMessage());
            return false;
        }
    }
}
