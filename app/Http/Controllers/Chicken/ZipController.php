<?php

namespace App\Http\Controllers\Chicken;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ZipController extends Controller
{
    public static function zip($xmlFile, $images, $transactionNumber)
    {
    	$filename = $transactionNumber;
    	try {
            //create zip
            $zip = new \ZipArchive;
            $res = $zip->open(storage_path('app/public/zip/'.$filename.'.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
                if ($res === FALSE) {
                    echo 'can not create zip';
                }
            foreach ($images as $key => $image) {
            // add image to zip
            $zip->addFile($image->getRealPath(), $filename.'/'.$filename.'/'.$image->getClientOriginalName()); 
            }
            
            // add xml to zip and close zip file
            $zip->addFile(storage_path('app/xml/ChickenBreast/'.$xmlFile), $filename.'/'.$xmlFile);
            $zip->close();
            Log::channel('woody-breast')->info('ZIP ARCHIVE: DONE - '.$transactionNumber);
        } catch (\Throwable $th) {
            //throw $th;
            Log::channel('woody-breast')->error('ZIP ARCHIVE: '.$transactionNumber.' - '.$th->getMessage());
            
        }
        
      	return $filename = $zip->status == 0 ? $filename.'.zip' : false;
    }
}
