<?php

namespace App\Http\Controllers\Chicken;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Chicken\XmlController;
use App\Http\Controllers\Chicken\ZipController;
use App\Http\Controllers\ImagoRestApiController;
use App\Mail\ChickenBreastNotifications;
use App\Mail\Notifications;
use Mail;
use Carbon\Carbon;
use File;
use Storage;
use Log;

class ChickenBreastController extends Controller
{
    public $transactionNumber;
    public $caseFilePath;
    public $basePath;
    public $caseTypeId = 4;

    public function __construct()
    {
        $this->basePath = storage_path('app/case/');
    }
    public function setTransactionNumber()
    {
        return 'EVC_'.$this->generateRandomString(5).'_'.Carbon::now()->format('mdy').'_'.Carbon::now()->format('Hi').'_CobbVantress';
    }

    public function create(Request $request)
    {
    	if (!$request->has('json') || !$request->hasFile('images')) {
            Log::channel('woody-breast')->error('JSON and images is required.');
    		return response()->json(['error' => 'json and images is required.'], 400);
    	}

        Log::channel('woody-breast')->info('REQUEST: '.$request->json);

        $this->transactionNumber = $this->setTransactionNumber();
        $this->caseFilePath = $this->openFile();
    	
        $data = json_decode($request->json);
    	$images = $request->images;

        if(!isset($data->Email) OR !filter_var($data->Email, FILTER_VALIDATE_EMAIL)){
            Log::channel('woody-breast')->error('Operator e-mail is required.'); 
            return response()->json(['error' => 'Operator e-mail is required.'], 400); 
        }
        // write info data to case file
        $content['TransactionNumber'] = $this->transactionNumber;
        $content['Farm'] = @$data->Farm ? $data->Farm : "";
        $content['Line'] = @$data->Line ? $data->Line : "";
        $content['Email'] = @$data->Email ? $data->Email : "";
        $content['Operator'] = @$data->Operator ? $data->Operator : "";
        $content['Equipment'] = @$data->Equipment ? $data->Equipment : "";
        
        foreach ($images as $key => $image) {
            $content['Images'][$key]['ImageFileName'] = @$image->getClientOriginalName() ? @$image->getClientOriginalName() : "";
            $content['Images'][$key]['Wingband'] = @$data->Chickens[$key]->Wingband ? @$data->Chickens[$key]->Wingband : "";
            $content['Images'][$key]['BodyWeight'] = @$data->Chickens[$key]->BodyWeight ? @$data->Chickens[$key]->BodyWeight : "";
            $content['Images'][$key]['Gender'] = @$data->Chickens[$key]->Gender ? @$data->Chickens[$key]->Gender : "";
            $content['Images'][$key]['Source'] = @$data->Chickens[$key]->Source ? @$data->Chickens[$key]->Source : "";
            $content['Images'][$key]['WB_SCORE'] = '';
            $content['Images'][$key]['BREASTWEIGHT'] = '';
            $content['Images'][$key]['MDLVERSION'] = '';
            $content['Images'][$key]['ALGVERSION'] = '';
        }
        
        $this->writeToFile($this->caseFilePath, json_encode($content));
        // create xml file
    	if (!$xmlFile = XmlController::xml($data, $images, $this->transactionNumber)) {
    		return response()->json(['error' => 'xml file is not created.'], 400);
    	}
        $xmlFile = $this->transactionNumber.'.xml';
    	// create zip file
    	if(!$zipFile = ZipController::zip($xmlFile, $images, $this->transactionNumber)){
    		return response()->json(['error' => 'zip file is not created.'], 400);
    	}
    	//upload to aws s3
    	$sourceFile = storage_path('app/public/zip/'.$zipFile);
    	$destinationPath = $zipFile;
        try {
            $iceClient = new ImagoRestApiController;
            $handle = $iceClient->upload($sourceFile);
            // $case->update(['handle' => $handle]);
            $iceClient->process($handle);
            Log::channel('woody-breast')->info('SEND STATUS: DONE - '.$this->transactionNumber);
        } catch (\Throwable $th) {
            //throw $th;
            Log::channel('woody-breast')->error('SEND STATUS GCP: '.$this->transactionNumber." - ".$th->getMessage());
        }
        
        // delete local file
        // File::delete(storage_path('app/public/zip/'.$this->transactionNumber.'.zip'));
        rename($this->basePath.$this->transactionNumber, $this->basePath.$this->transactionNumber.'_'.$handle);
    	return response()->json(['message' => 'OK', 'handle' => $handle], 200);
    }

    protected function openFile()
    {
        if (!is_dir($this->basePath)) {
            mkdir($this->basePath);
        }
        return fopen($this->basePath.$this->transactionNumber, "w+");
    }

    protected function writeToFile($file, $content)
    {
        ftruncate($file, 0);
        fwrite($file, $content);
        fclose($file);
    }

    public function setResult($handle)
    {
        // get fileName
        $fileName = $this->getFileName($handle);
        $oldContent = (array)json_decode(file_get_contents($this->basePath.$fileName));
        $resultFolder = str_replace($handle, "", $fileName).'RESULT';
        $results = scandir(storage_path('app/public/images/out/'.$resultFolder));
        
        foreach ($results as $value) {
            if (is_file(storage_path('app/public/images/out/'.$resultFolder.'/'.$value))) {
                $oldContent['result'][] = url('storage/images/out/'.$resultFolder.'/'.$value);
            }
           
        }
        /*if (strpos($response->Records[0]->s3->object->key, '.xml')) {
            $oldContent = $this->parseFromXml($oldContent, $response);
        }*/
        $content = str_replace("\/", "/", json_encode($oldContent, JSON_PRETTY_PRINT));
        $this->writeToFile(fopen($this->basePath.$fileName, 'a+'), $content);
        rename($this->basePath.$fileName, substr($this->basePath.$fileName, 0, strrpos($this->basePath.$fileName, '_')));
    }

    protected function getFileName($handle)
    {
        $files = scandir(storage_path('app/case'));
        foreach ($files as $key => $file) {
            if(strpos($file, $handle)){
                return $file;
            }
        }
        
    }

    protected function parseFromXml($content, $xmlUri)
    {
        // dd($content);
        $xml = file_get_contents($xmlUri);
        $xml = preg_replace('/\s+/S', " ", $xml);
        $res = str_replace(['<ImageFileName>', '</ImageFileName>'], '$', $xml);
        $res = explode('$', $res);
        array_shift($res);
        array_pop($res);
        $res = array_values(array_filter($res, function($value){ if(!empty(str_replace(['"',' '], '', $value))) return $value;}));
        
        foreach ($res as $key => $value) {
            // $content->{'Images'}[$key]->ML_EVAL = preg_replace(['#(.*?)(</ML_CONFIDENCE>)#','#(</ML_EVAL>)(.*)#'], "", $value);
            // $content->{'Images'}[$key]->ML_CONFIDENCE = preg_replace(['#(.*?)(<ML_CONFIDENCE>)#','#(</ML_CONFIDENCE>)(.*)#'], "", $value);
            $content->{'Images'}[$key]->WB_SCORE = preg_replace(['#(.*?)(<WB_SCORE>)#','#(</WB_SCORE>)(.*)#'], "", $value);
            $content->{'Images'}[$key]->MDLVERSION = preg_replace(['#(.*?)(<MDLVERSION>)#','#(</MDLVERSION>)(.*)#'], "", $value);
            $content->{'Images'}[$key]->ALGVERSION = preg_replace(['#(.*?)(<ALGVERSION>)#','#(</ALGVERSION>)(.*)#'], "", $value);
            $content->{'Images'}[$key]->BREASTWEIGHT = preg_replace(['#(.*?)(<BREASTWEIGHT>)#','#(</BREASTWEIGHT>)(.*)#'], "", $value);
        }
        return $content;
    }

    public static function notificationForProcesedCases()
    {
        $chickenBreast = new self;
        $cases = $chickenBreast->checkProcessedCases($chickenBreast->basePath);
        foreach ($cases as $key => $case) {
            $result = $chickenBreast->getResult($case, $chickenBreast->basePath);

            if ($result) {
                if (!is_dir(storage_path('app/closed-case'))) {
                    mkdir(storage_path('app/closed-case'));
                }
                rename($chickenBreast->basePath.$case, storage_path('app/closed-case/'.$case.'.json'));
                $chickenBreast->sendCaseResultToMail($result->Email, json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES), $case);
            }
        }
    }

    public function checkProcessedCases($basePath)
    {
        $cases = [];
        $caseFiles = scandir($basePath);        
        
        foreach ($caseFiles as $file) {
            if (is_file($basePath.$file) && Carbon::now() > Carbon::createFromTimestamp(filemtime($basePath.$file))->addSeconds(30)) {
                $cases[] = $file;
            }
        }
        return $cases;
    }

    public function getResult($case, $basePath)
    {
        $result = json_decode(file_get_contents($basePath.$case));
        if ($result && @$result->Email && isset($result->result)) {
            foreach ($result->result as $value) {
                if (strpos($value, '.xml')) {
                    $value = storage_path('app/public/images'. substr($value, strpos($value, '/out/')));
                    $result = $this->parseFromXml($result, $value);
                    $content = str_replace("\/", "/", json_encode($result, JSON_PRETTY_PRINT));
                    $this->writeToFile(fopen($basePath.$case, 'a+'), $content);
                }
            }
            Log::channel('woody-breast')->info('ICE RESULT - PARSE XML TO JSON: DONE - '.$case);
            return $result;
        }else{
            return null;
        }
    }

    public function sendCaseResultToMail($email, $content, $case)
    {
        try {
            Mail::to([$email, "victor.krivorotov@imagosystems.com"])->send(new ChickenBreastNotifications($content, $case));
            Log::channel('woody-breast')->info('RESULT EMAIL: SENT - '.$case);
        } catch (\Throwable $th) {
            //throw $th;
            Log::channel('woody-breast')->error('RESULT EMAIL: '.$case.' - '.$th->getMessage());
        }
    }

    private function generateRandomString($length = 5) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
