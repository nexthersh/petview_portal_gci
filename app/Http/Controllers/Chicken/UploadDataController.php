<?php

namespace App\Http\Controllers\Chicken;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Chicken\UploadDataRequest;
use App\CaseModel;
use App\Images;
use Auth;
use Carbon\Carbon;

class UploadDataController extends Controller
{

	public $caseTypeId = 4;
	
    public function uploadData(UploadDataRequest $request)
    {
        $images = collect($request->images);
        $file = $request->file;
        $images[] = $file;
        $case = CaseModel::create([
                                    'case_type_id' => $this->caseTypeId,
                                    'user_id' => Auth::user()->id,
                                    'processing' => 0,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now()
                                    ]); 
        
        $transactionNumber = $this->transactionNumberUploadData($case->id);
        foreach ($images as $key => $image) {
            $image->caseId = $case->id;
            $image->transactionNumber = $transactionNumber;
            $image = Images::create($this->assignValue($image));
        }

    }

    private function assignValue($image)
    {
        $insert = [
                  'name' => $image->getClientOriginalName(),
                  'original_name' => $image->getClientOriginalName(),
                  'transaction_id' => $image->transactionNumber,
                  'case_id' => $image->caseId,
                  'created_at' => Carbon::now(),
                  'updated_at' => Carbon::now()
                ];
        return $insert;
    }

    public function transactionNumberUploadData($caseId)
    {
        $transactionNumber = 'EVC_'.$caseId.'_'.Carbon::now()->format('mdy').'_'.Carbon::now()->format('Hi');

        return $transactionNumber;
    }        
}
