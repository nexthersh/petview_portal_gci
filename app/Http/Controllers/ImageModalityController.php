<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ImageModalityController extends Controller
{
     public function getImageModality()
    {
    	$image_modality = DB::table('modality_type')->get()->toArray();;

    	return response()->json($image_modality, 200);
    }

    public function getByAlgorithmsId()
    {
    	$ids = explode(',', request()->ids);
        $modality_type = DB::table('modalitytype_algorithm')
                        ->join('modality_type', 'modality_type.id', 'modalitytype_algorithm.modality_type_id');

        foreach ($ids as $key => $id) {
            
            if ($key == 0) {
                $modality_type->where('algorithm_id', $id);
            }
            $modality_type->orWhere('algorithm_id', $id);
        }
        $modality_type->select('modality_type.id as id', 'modality_type.name as name'/*, 'modalitytype_algorithm.algorithm_id as algorithm_id'*/);
        $modality_type->distinct();
        $modality_type = $modality_type->get();

        return response()->json($modality_type, 200);
    }
}
