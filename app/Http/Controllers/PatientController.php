<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\Audit;
use App\Patient;
use App\CaseModel;
use App\Images;
use Carbon\Carbon;  

class PatientController extends Controller
{
    use Audit;
    
    private $user;
    private $role;

    public function __construct(Request $request, JWTAuth $jwt)
    {
        $token = $jwt->getToken();
        $this->user = $jwt->toUser($token);
        $this->role = $this->user->role; 
  
    }

    public function index(Request $request)
    {
      $query = $this->query();
      
        if ($this->role == 'user' || $this->role == 'trial_user') {
            $query = $this->getPatientsByUser($query);
        }else if ($this->role == 'clinic_admin') {
            $query = $this->getPatientsByClinicAdmin($query);
        }
        
      
      if ($request->filled('search')) {
          $query->where('patients.name', 'LIKE', '%'.$request->search.'%');
          $query->orWhere('patients.id', 'LIKE', '%'.$request->search.'%');
      }
      
      if ($request->has('name') && $request->name) {
              $query->where('patients.name',  'LIKE', '%'.$request->name.'%');
        }
      if ($request->has('patientId') && $request->patientId) {
              $query->where('patients.id', $request->patientId);
        }

      if ($request->has('clientId') && $request->clientId) {
          $query->where('patients.client_id', $request->clientId);
      }

      if ($request->has('client') && $request->client) {
          $query->where(function($q) use ($request){
            $q->where('clients.first_name', 'LIKE', '%'.$request->client.'%');  
            $q->orWhere('clients.last_name', 'LIKE', '%'.$request->client.'%');  
          });
      }

      if ($request->has('created') && $request->created) {
              $query->whereDate('patients.created_at', parseGMTDate($request->created));
        }  
        
      if ($request->has('updated') && $request->updated) {
            $query->whereDate('patients.updated_at', parseGMTDate($request->created));
      }

      if ($request->has('dateFrom') && $request->dateFrom) {
              $query->whereDate('patients.created_at','>=', parseGMTDate($request->dateFrom));
        }

      if ($request->has('dateTo') && $request->dateTo) {
            $query->whereDate('patients.created_at','<=', parseGMTDate($request->dateTo));
      }
      
      $patients = $query->paginate(10);
      return response()->json($patients, 200);
    }

    private function getPatientsByClinicAdmin($query)
    {
        return $query->where('users.clinic_id', $this->user->clinic_id);
    }

    private function getPatientsByUser($query)
    {
        return $query->where('users.id', $this->user->id);
    }
    
    public function show($id)
    {
      CaseModel::$preventAttrSet = false;
      Images::$preventAttrSet = false;
      $patient = Patient::with('patientType',
                               'breed',
                               'alteredStatus',
                               'client.user',
                               'caseHistory.images.aoiImage',
                               'caseHistory.caseType',
                               'caseHistory.algorithm',
                               'caseHistory.imageModality',
                               'ddCase')
                          ->findOrFail($id);

      $this->authorize('show', $patient);
      return response()->json($patient, 200);
    }

    public function update($request)
    {
      $response = [];
      foreach ($request->patients as $key => $patient) {
          if (empty($patient)) { continue; }

          $query = isset($patient['id']) ? Patient::find($patient['id']) : null;
          
          if (!$query) {
              $data = $this->assignValue($patient);
              $data['client_id'] = $request->clientId;
              $response[] = $patient =  Patient::create($data);
              $patient->audits()->create($this->assignAuditValues('create', null, $patient, $request));
          
          }else{
            $oldValues = $query->toJson();
            $data = $this->assignValue($patient);
            $data['client_id'] = $request->clientId;

            $query->update($data);
            $query->audits()->create($this->assignAuditValues('update', $oldValues, $query, $request));
          }  
      }

      return $response;
    }
    
    public function delete(Request $request, $id)
    {
        $patient = Patient::with('client.user')->find($id);
        if ($patient) {
            $this->authorize('show', $patient);
            $patient->delete();
            $patient->audits()->create($this->assignAuditValues('delete', $patient, null, $request));
            return response()->json(['message' => 'Successfully deleted patient.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }else{  
            return response()->json(['message' => 'Patient not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function deleteAll(Request $request)
    { 
          $ids = explode(',', $request->ids);
          $patients = $this->query();

          if ($this->role == 'user' || $this->role == 'trial_user') {
            $patients = $this->getPatientsByUser($patients);
          }else if ($this->role == 'clinic_admin') {
              $patients = $this->getPatientsByClinicAdmin($patients);
          }

          $patients = $patients->whereIn('patients.id', $ids)->get();
          foreach ($patients as $patient) {
            $patient->delete();
            $patient->audits()->create($this->assignAuditValues('delete', $patient, null, $request));
          }

          return response()->json(['message' => 'Successfully deleted patients.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function query()
    {
        $query = Patient::join('clients','patients.client_id', '=', 'clients.id')
                            ->leftJoin('users', 'clients.user_id', '=', 'users.id')
                            ->leftJoin('patient_type', 'patients.patient_type_id', '=', 'patient_type.id')
                            ->leftJoin('breeds', 'patients.breed_id', '=', 'breeds.id')
                            ->leftJoin('altered_status', 'patients.altered_status_id', '=', 'altered_status.id')
                            ->select('patients.*',  'patient_type.name as patient_type', 'breeds.name as breed', 'altered_status.name as reproductive_status')
                            ->selectRaw("CONCAT_WS(' ', clients.first_name, clients.middle_name, clients.last_name) as client_name");

        return $query;
    }

    public function assignValue($patient)
    {
        $data['name'] = $patient['name'];
        $data['age'] = $patient['age'];
        $data['gender'] = $patient['gender'];
        $data['note'] = @$patient['notes'];
        $data['patient_type_id'] = @$patient['species'];
        $data['breed_id'] = @$patient['breed'];
        $data['altered_status_id'] = @$patient['reproductive_status'];

        return $data;
    }

}
