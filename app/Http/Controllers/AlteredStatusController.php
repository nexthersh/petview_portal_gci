<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AlteredStatusController extends Controller
{
    public function getAlteredStatus()
    {
    	$alteredStatus = DB::table('altered_status')->get();
    	return response()->json($alteredStatus, 200);
    }
}
