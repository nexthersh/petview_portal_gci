<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\ImagesNoProcessingController;
use App\Http\Controllers\ImagoRestApiController;
use App\Http\Controllers\XmlController;
use App\Http\Controllers\ZipController;
use App\Http\Traits\Audit;
use App\ImagesNoProcessing;
use App\Images;
use App\CaseModel;
use Carbon\Carbon;
use File;
use Auth;

class CaseNoProcessingController extends Controller
{
    use Audit;
    
    private $case_type_id = 2;
    
    public function store(Request $request, CaseModel $caseModel)
    {
        // decode json
    	$json = json_decode($request->json);
          // dd($json);
    	$images = $request->images;
        $aoiImages = $request->aoi_images;
    	$imagesData = $json->images;
        
        
        //create case
        $case = $caseModel->create([
                                                'case_type_id' => $this->case_type_id,
                                                'user_id' => Auth::user()->id,
                                                /*'body_part_id' => $json->body_part[0]->id,
                                                'algorithm_id' => $json->algorithm[0]->id,
                                                'image_modality_id' => $json->image_modality[0]->id,*/
                                                'processing' => 0,
                                                'created_at' => Carbon::now()->toDateTimeString(),
                                                'updated_at' => Carbon::now()->toDateTimeString()
                                            ]);
        $caseId = $case->id;
        $case->audits()->create($this->assignAuditValues('create', null, $case, $request));
        $transactionNumber = $this->transactionNumber($caseId);
 
		foreach ($images as $key => $image) {

            // insert images data to database
            // $originalImage = ImagesNoProcessingController::store($imagesData[$key], $caseId, $transactionNumber);
            $originalImage = ImagesController::create($imagesData[$key], $caseId, $transactionNumber, @$aoiImages[$key], $this->case_type_id);
            // create xml
            // $filename = str_replace([" ","-"], "_", explode(".", $image->getClientOriginalName())[0]);
            // $filename = str_replace([" ","-"], "_", explode(".", $imagesData[$key]->image_name)[0]);
            $filename = prepareImageName($imagesData[$key]->image_name);
            XmlController::xmlNoProcessing($json, $imagesData[$key], $caseId, $transactionNumber, $key, $filename);
      
            // upload image to aws
            $sourceImage = $image->getRealPath();
            Storage::putFileAs('public/images/develop/'.$transactionNumber.'/'.$filename, $sourceImage, $image->getClientOriginalname());
           
            $imageDestionationZip = $imagesData[$key]->image_name;
            $xmlDestinationZip = $caseId.'_'.$filename;
            $sourceXml = storage_path('app/public/images/develop/'.$transactionNumber.'/'.$filename.'/'.$caseId.'_'.$filename.'.xml');
            //create zip
            $zip = new \ZipArchive;
            $res = $zip->open(storage_path('app/public/zip/'.$caseId.'_'.$filename.'.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
                if ($res === FALSE) {
                    echo 'can not create zip';
                }
            $zip->addFile($sourceImage, $imageDestionationZip);

            // add xml to zip and close zip file
            $zip->addFile($sourceXml, $xmlDestinationZip);
            $zip->close();
            //send zip to aws
            $sourceFile = storage_path('app/public/zip/'.$caseId.'_'.$filename.'.zip');
            $iceClient = new ImagoRestApiController;
            $handle = $iceClient->upload($sourceFile);
            $case->update(['handle' => $handle]);
            // delete local zip and xml file
            
            File::delete($sourceFile);  
		}

        return response()->json(['message' => 'Successfully created the case!', 'handle' => $handle, 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function transactionNumber($caseId)
    {
    	$transactionNumber = 'EVC_'.$caseId.'_'.Carbon::now()->format('mdy').'_'.Carbon::now()->format('Hi');

    	return $transactionNumber;
    }
}
