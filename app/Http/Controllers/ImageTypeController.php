<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImagetypeAlgorithm;
use DB;

class ImageTypeController extends Controller
{
    public function getImageType()
    {
    	$image_type = DB::table('image_type')->get()->toArray();;

    	return response()->json($image_type, 200);
    }

    public function getByAlgorithmId($id)
    {
    	$image_type = ImagetypeAlgorithm::with('imagetype')
    									->where('algorithm_id', $id)
    									->get();
    	return response()->json($image_type, 200);
    }
}
