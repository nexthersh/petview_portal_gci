<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientType;

class PatientTypeController extends Controller
{
    public function getType()
    {
    	$type = PatientType::get();

    	return response()->json($type, 200);
    }

    public static function store($patientType)
    {
    	$type = PatientType::insertGetId(['name' => $patientType]);

    	return $type;
    }
}
