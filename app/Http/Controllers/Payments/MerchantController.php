<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class MerchantController extends Controller
{
    protected $merchantLoginId;
    protected $merchantTransactionKey;
    protected $merchantAuthentication;
    protected $environment;
    
    public function __construct()
    {
        $this->merchantLoginId = config('payment.merchant.MERCHANT_LOGIN_ID');
        $this->merchantTransactionKey = config('payment.merchant.MERCHANT_TRANSACTION_KEY');
        $this->merchantAuthentication = $this->setMerchant();   
        $this->setEnvironment();
    }

    public function setMerchant()
    {
        /* Create a merchantAuthenticationType object with authentication details
        retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->merchantLoginId);
        $merchantAuthentication->setTransactionKey($this->merchantTransactionKey);

        return $merchantAuthentication;

    }

    public function setEnvironment()
    {
        $environment = config('payment.environment');

        if ($environment === 'PRODUCTION') {
            $this->environment = \net\authorize\api\constants\ANetEnvironment::PRODUCTION;
        }else{
            $this->environment = \net\authorize\api\constants\ANetEnvironment::SANDBOX;
        }
    }
    
    
}
