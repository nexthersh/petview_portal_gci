<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Payments\CustomerProfileController;
use App\PaymentCustomerProfile;
use Illuminate\Support\Facades\DB;
use App\Mail\PaymentNotifications;
use App\PricingPlan;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function primaryOwnerPayment($data)
    {
        $customer = new CustomerProfileController();
        $customer->createCustomerProfile($data);
        
        $plan = $data->plan;
        sleep(15);
        if ($plan->name != 'percase') {

            if (!PaymentTransactionController::subscribe($customer, $plan)) {
                return false;
            }
        }

        DB::table('payment_user_plan')->updateOrInsert(
            ['user_id' => $customer->userId],
            [
                'user_id' => $customer->userId,
                'clinic_id' => $customer->clinicId,
                'plan_id' => $plan->id,
            ]
        );

        return $plan;
    }
 
    public function paymentPercase()
    {
        $customer = PaymentCustomerProfile::where('clinic_id', $this->user->clinic_id)->first();
       
        if (!$customer) {
            return false;
        }

        if (!PaymentTransactionController::charge($customer, $this->user->plan)) {
            return false;
        }

        $clinicAdmin = $customer->user;
        
        NotificationsController::sendPaymentPercaseNotification($clinicAdmin, $this->user->plan);
        Mail::to($clinicAdmin->email)->send(new PaymentNotifications($this->user->plan));
        return true;
    }
}
