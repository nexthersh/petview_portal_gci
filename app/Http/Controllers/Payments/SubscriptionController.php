<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Payments\MerchantController;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use App\PaymentSubscription;
use App\User;
use App\Clinic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;

class SubscriptionController extends MerchantController
{
    public function createSubscriptionFromCustomerProfile($customer, $plan)
    {
        if (!$customer) {
            $errorLog = "ERROR : Create subscription - Invalid customer\n";
            $errorLog .= "Response : Customer not found";
            Log::channel('payment-transaction')->error($errorLog);
            return false;
        }
        // Set the transaction's refId
        $refId = 'ref' . time();
        
        $startDate = Carbon::now();
        $subscriptionName = 'Plan - ' . $plan->name . ' Subscription';
        
        $clinic = Clinic::find($customer->clinicId ?: $customer->clinic_id);
        $discount = $clinic->discount;

        if ($clinic->discount_expiry_date < date('Y-m-d')) {
            $discount = 0;
        }
        $amount = number_format(($plan->price / 100) * (100 - $discount), 2);
        
        // Subscription Type Info
        $subscription = new AnetAPI\ARBSubscriptionType();
        $subscription->setName($subscriptionName);

        $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
        $interval->setLength($plan->length);
        $interval->setUnit($plan->unit);

        $paymentSchedule = new AnetAPI\PaymentScheduleType();
        $paymentSchedule->setInterval($interval);
        $paymentSchedule->setStartDate($startDate);
        $paymentSchedule->setTotalOccurrences("9999");
        // $paymentSchedule->setTrialOccurrences("1");

        $subscription->setPaymentSchedule($paymentSchedule);
        $subscription->setAmount($amount);
        // $subscription->setTrialAmount("0.00");

        $profile = new AnetAPI\CustomerProfileIdType();
        $profile->setCustomerProfileId($customer->profileId);
        $profile->setCustomerPaymentProfileId($customer->paymentProfileId);
        // $profile->setCustomerAddressId($customerAddressId); 

        $subscription->setProfile($profile);

        $request = new AnetAPI\ARBCreateSubscriptionRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($refId);
        $request->setSubscription($subscription);
        $controller = new AnetController\ARBCreateSubscriptionController($request);

        $response = $controller->executeWithApiResponse($this->environment);

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {

            $subscriptionId = $response->getSubscriptionId();

            PaymentSubscription::updateOrCreate(
                ['subscription_id' => $subscriptionId, 'customer_profile_id' => $customer->profileId],
                [
                    'subscription_name' => $subscriptionName,
                    'customer_profile_id' => $customer->profileId,
                    'customer_payment_profile_id' => $customer->paymentProfileId,
                    'amount' => $amount,
                    'status' => 'active',
                    'start_date' => $startDate,
                    'user_id' => $customer->userId,
                ]
            );

            $infoLog = "USER#" . $customer->userId . "\n";
            $infoLog .= "SUCCESS: Create Subscription ID : " . $subscriptionId . "\n";
            Log::channel('payment-transaction')->info($infoLog);

            return true;
        } else {
            $errorLog = "USER#" . $customer->userId . "\n";
            $errorLog .= "ERROR : Create Sub - Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            $errorLog .= "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            Log::channel('payment-transaction')->error($errorLog);

            return false;
        }
    }

    public function cancelSubscription($subscriptionId)
    {
        $subscription = PaymentSubscription::where('subscription_id', $subscriptionId)->first();
        if (!$subscription) {
            return response()->json(["message" => "Subscription not found."], 404);
        }
        // Set the transaction's refId
        $refId = 'ref' . time();

        $request = new AnetAPI\ARBCancelSubscriptionRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($refId);
        $request->setSubscriptionId($subscriptionId);

        $controller = new AnetController\ARBCancelSubscriptionController($request);

        $response = $controller->executeWithApiResponse($this->environment);

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {

            $subscription->update(['status' => 'canceled']);
            $successMessages = $response->getMessages()->getMessage();
            $infoLog = "Cancel SubscriptionID#" . $subscriptionId . "\n";
            $infoLog .= "SUCCESS : " . $successMessages[0]->getCode() . "  " . $successMessages[0]->getText() . "\n";
            Log::channel('payment-transaction')->info($infoLog);
            return true;
        } else {
            $errorLog = "Cancel SubscriptionID#" . $subscriptionId . "\n";
            $errorLog .= "ERROR :  Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            $errorLog .= "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            Log::channel('payment-transaction')->info($errorLog);
            return false;
        }
    }

    public static function getSubscription($subscriptionId)
    {
        $self = new self;
        // Set the transaction's refId
        $refId = 'ref' . time();

        // Creating the API Request with required parameters
        $request = new AnetAPI\ARBGetSubscriptionRequest();
        $request->setMerchantAuthentication($self->merchantAuthentication);
        $request->setRefId($refId);
        $request->setSubscriptionId($subscriptionId);
        $request->setIncludeTransactions(true);

        // Controller
        $controller = new AnetController\ARBGetSubscriptionController($request);

        // Getting the response
        $response = $controller->executeWithApiResponse($self->environment);

        if ($response != null) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                // // Success
                // echo "SUCCESS: GetSubscription:" . "\n";
                // // Displaying the details
                // echo "Subscription Name: " . $response->getSubscription()->getName() . "\n";
                // echo "Subscription amount: " . $response->getSubscription()->getAmount() . "\n";
                // echo "Subscription status: " . $response->getSubscription()->getStatus() . "\n";
                // echo "Subscription Description: " . $response->getSubscription()->getProfile()->getDescription() . "\n";
                // echo "Customer Profile ID: " .  $response->getSubscription()->getProfile()->getCustomerProfileId() . "\n";
                // echo "Customer payment Profile ID: " . $response->getSubscription()->getProfile()->getPaymentProfile()->getCustomerPaymentProfileId() . "\n";
                // $transactions = $response->getSubscription()->getArbTransactions();
                // if ($transactions != null) {
                //     foreach ($transactions as $transaction) {
                //         echo "Transaction ID : " . $transaction->getTransId() . " -- " . $transaction->getResponse() . " -- Pay Number : " . $transaction->getPayNum() . "\n";
                //     }
                // }

                return $response;
            } else {
                
                return false;
                // Error
                // echo "ERROR :  Invalid response\n";
                // $errorMessages = $response->getMessages()->getMessage();
                // echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            }
        } else {
            // Failed to get response
            // echo "Null Response Error";
            return false;
        }
    }
}
