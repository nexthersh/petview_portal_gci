<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Payments\SubscriptionController;
use App\PaymentCustomerProfile;
use Illuminate\Http\Request;
use App\PaymentSubscription;
use Log;

class WebhookController extends Controller
{
    public function notifications(Request $request)
    {
        $data =  file_get_contents('php://input');
        Log::channel('payment-transaction')->info('Webhook: '.$data);
        $data = json_decode($data);
        $payload = $data->payload;

        $subscriptionId = $payload->id;
        $subscriptionName = $payload->name;
        $customerProfileId = $payload->profile->customerProfileId;
        $customerPaymentProfileId = $payload->profile->customerPaymentProfileId;
        
        $customer = PaymentCustomerProfile::where('customer_profile_id', $customerProfileId)
            ->where('customer_payment_profile_id', $customerPaymentProfileId)
            ->first();
        
        $subscription = SubscriptionController::getSubscription($subscriptionId);
        if (!$customer || !$subscription) {
            return false;
        }

        $startDate = $subscription->getSubscription()->getPaymentSchedule()->getStartDate();
        
        PaymentSubscription::updateOrCreate(
            ['subscription_id' => $subscriptionId, 'customer_profile_id' => $customerProfileId],
            [
                'subscription_name' => $subscriptionName,
                'customer_profile_id' => $customerProfileId,
                'customer_payment_profile_id' => $customerPaymentProfileId,
                'amount' => $payload->amount,
                'status' => $payload->status,
                'start_date' => $startDate,
                'user_id' => $customer->user_id,
            ]
        );
        $planCases = $customer->user->plan->number_of_cases;
        $availableCases = $customer->clinic->availableCases;
        $customer->clinic()->update(['availableCases' => $availableCases + $planCases]);
    }
}
