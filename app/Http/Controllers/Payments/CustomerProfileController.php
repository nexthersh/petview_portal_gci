<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use App\PaymentCustomerProfile;
use Illuminate\Support\Facades\Auth;
use Log;

class CustomerProfileController extends MerchantController
{

    public function getCustomer()
    {
        $user = Auth::user();
        $customerProfile = PaymentCustomerProfile::where('user_id', $user->id)->first();
        $customerProfile->userId = $user->id;
        $customerProfile->profileId = $customerProfile->customer_profile_id;
        $customerProfile->paymentProfileId = $customerProfile->customer_payment_profile_id;

        return $customerProfile;
    }

    public function createCustomerProfile($data)
    {

        $user = Auth::user();

        $customerProfile = PaymentCustomerProfile::where('user_id', $user->id)->first();
        if ($customerProfile) {
            return $this->updateCustomerProfile($data);
        }
        // Set the transaction's refId
        $refId = 'ref' . time();

        // Set credit card information for payment profile
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($data->billingCardNumber);
        $creditCard->setExpirationDate($data->billingExpireDate);
        $creditCard->setCardCode($data->billingCVV);

        $paymentCreditCard = new AnetAPI\PaymentType();
        $paymentCreditCard->setCreditCard($creditCard);

        // Create the Bill To info for new payment type
        $billTo = new AnetAPI\CustomerAddressType();
        $billTo->setFirstName($data->billingFirstName);
        $billTo->setLastName($data->billingLastName);
       
        // Create a new CustomerPaymentProfile object
        $paymentProfile = new AnetAPI\CustomerPaymentProfileType();
        $paymentProfile->setCustomerType('individual');
        $paymentProfile->setBillTo($billTo);
        $paymentProfile->setPayment($paymentCreditCard);
        $paymentProfiles[] = $paymentProfile;


        // Create a new CustomerProfileType and add the payment profile object
        $customerProfile = new AnetAPI\CustomerProfileType();
        $customerProfile->setDescription("Customer - " . $user->email);
        $customerProfile->setMerchantCustomerId("M_" . time());
        $customerProfile->setEmail($user->email);
        $customerProfile->setpaymentProfiles($paymentProfiles);
        // $customerProfile->setShipToList($shippingProfiles);


        // Assemble the complete transaction request
        $request = new AnetAPI\CreateCustomerProfileRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($refId);
        $request->setProfile($customerProfile);

        // Create the controller and get the response
        $controller = new AnetController\CreateCustomerProfileController($request);
        $response = $controller->executeWithApiResponse($this->environment);
        
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {

            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
            $this->profileId = $response->getCustomerProfileId();
            $this->paymentProfileId = $paymentProfiles[0];
            $this->userId = $user->id;
            $this->clinicId = $user->clinic_id;

            $customer = PaymentCustomerProfile::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'user_id' => $user->id,
                    'clinic_id' => $this->clinicId,
                    'customer_profile_id' => $this->profileId,
                    'customer_payment_profile_id' => $this->paymentProfileId,
                    'billing_first_name' => substr($data->billingFirstName, 0, 2) . "****",
                    'billing_last_name' => substr($data->billingLastName, 0, 2) . "****",
                    'billing_address' => substr($data->billingAddress, 0, 2) . "****",
                    'billing_credit_card' => "****" . substr($data->billingCardNumber, -4),
                    'billing_expire_date' => $data->billingExpireDate,
                ]
            );

            $infoLog = "USER#" . $user->id . "\n";
            $infoLog .= "Succesfully created customer profile : " . $this->profileId . "\n";
            $infoLog .= "SUCCESS: PAYMENT PROFILE ID : " . $this->paymentProfileId . "\n";
            Log::channel('payment-transaction')->info($infoLog);
            
            return true;
        
        } else if($response != null) {
            
            $errorLog = "USER#" . $user->id . "\n";
            $errorLog .= "ERROR : Create Customer - Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            $errorLog .= "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            Log::channel('payment-transaction')->error($errorLog);

            return false;
        }else{
            $errorLog = "No response returned \n";
            $errorLog .= "CustomerProfileId#" . $user->id . "\n";
            Log::channel('payment-transaction')->error($errorLog);
            return false;
        }
    }

    public function update(Request $req)
    {
        ;
        return $this->updateCustomerProfile($req) ? response()->json(["message" => "Profile update success."], 200) : response()->json(["message" => "Profile update success."], 422);
    }

    public function updateCustomerProfile($req)
    {
        $user = Auth::user();
        $customerProfile = PaymentCustomerProfile::where('user_id', $user->id)->first();
        if (!$customerProfile) {
            return response()->json(["message" => "Profile not found."], 404);
        }
        $customerProfileId = $customerProfile->customer_profile_id;
        $customerPaymentProfileId = $customerProfile->customer_payment_profile_id;
        // Set the transaction's refId
        $refId = 'ref' . time();

        $request = new AnetAPI\GetCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($refId);
        $request->setCustomerProfileId($customerProfileId);
        $request->setCustomerPaymentProfileId($customerPaymentProfileId);

        $controller = new AnetController\GetCustomerPaymentProfileController($request);
        $response = $controller->executeWithApiResponse($this->environment);
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $billto = new AnetAPI\CustomerAddressType();
            $billto = $response->getPaymentProfile()->getbillTo();
            
            $creditCard = new AnetAPI\CreditCardType();
            $creditCard->setCardNumber($req->billingCardNumber);
            $creditCard->setExpirationDate($req->billingExpireDate);
            $creditCard->setCardCode($req->billingCVV);

            $paymentCreditCard = new AnetAPI\PaymentType();
            $paymentCreditCard->setCreditCard($creditCard);
            $paymentprofile = new AnetAPI\CustomerPaymentProfileExType();
            $paymentprofile->setBillTo($billto);
            $paymentprofile->setCustomerPaymentProfileId($customerPaymentProfileId);
            $paymentprofile->setPayment($paymentCreditCard);

            // We're updating the billing address but everything has to be passed in an update
            // For card information you can pass exactly what comes back from an GetCustomerPaymentProfile
            // if you don't need to update that info

            // Update the Bill To info for new payment type
            $billto->setFirstName($req->billingFirstName);
            $billto->setLastName($req->billingLastName);

            // Update the Customer Payment Profile object
            $paymentprofile->setBillTo($billto);

            // Submit a UpdatePaymentProfileRequest
            $request = new AnetAPI\UpdateCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($this->merchantAuthentication);
            $request->setCustomerProfileId($customerProfileId);
            $request->setPaymentProfile($paymentprofile);

            $controller = new AnetController\UpdateCustomerPaymentProfileController($request);
            $response = $controller->executeWithApiResponse($this->environment);
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                
                $this->userId = $user->id;
                $this->clinicId = $user->clinic_id;
                $customerProfile->update(
                    [
                        'clinic_id' => $this->clinicId,
                        'billing_first_name' => substr($req->billingFirstName, 0, 2) . "****",
                        'billing_last_name' => substr($req->billingLastName, 0, 2) . "****",
                        'billing_address' => substr($req->billingAddress, 0, 2) . "****",
                        'billing_credit_card' => "****" . substr($req->billingCardNumber, -4),
                        'billing_expire_date' => $req->billingExpireDate,
                    ]
                );
                $Message = $response->getMessages()->getMessage();
                $infoLog = "CustomerProfile#". $customerProfileId . "\n";
                $infoLog .= "Update Customer Payment Profile SUCCESS: " . $Message[0]->getCode() . "  " . $Message[0]->getText() . "\n";
                Log::channel('payment-transaction')->info($infoLog);

                return true;
            
            } else if ($response != null) {
                
                $errorMessages = $response->getMessages()->getMessage();
                $errorLog = "CustomerProfile#". $customerProfileId . "\n";
                $errorLog .= "Failed to Update Customer Payment Profile :  " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
                Log::channel('payment-transaction')->error($errorLog);
                
                return false;
            }

        } else {
            
            $errorMessages = $response->getMessages()->getMessage();
            $errorLog = "Update CustomerProfile#". $customerProfileId . "\n";
            $errorLog .= "Failed to Get Customer Payment Profile :  " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            Log::channel('payment-transaction')->error($errorLog);

            return false;
        }
    }

    public function chargeCustomerProfile($customer, $plan)
    {
        if(!$plan) { return false; }

        // Set the transaction's refId
        $refId = 'ref' . time();
        $discount = $customer->clinic->discount;

        if ($customer->clinic->discount_expiry_date < date('Y-m-d')) {
            $discount = 0;
        }
        $amount = number_format(($plan->price_percase / 100) * (100 - $discount), 2);

        $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
        $profileToCharge->setCustomerProfileId($customer->profileId);
        $paymentProfile = new AnetAPI\PaymentProfileType();
        $paymentProfile->setPaymentProfileId($customer->paymentProfileId);
        $profileToCharge->setPaymentProfile($paymentProfile);

        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setProfile($profileToCharge);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse($this->environment);

        if ($response != null) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $infoLog = " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
                    $infoLog .= "Charge Customer Profile APPROVED  :" . "\n";
                    $infoLog .= " Charge Customer Profile AUTH CODE : " . $tresponse->getAuthCode() . "\n";
                    $infoLog .= " Charge Customer Profile TRANS ID  : " . $tresponse->getTransId() . "\n";
                    $infoLog .= " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
                    $infoLog .= " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
                    Log::channel('payment-transaction')->info($infoLog);
                    return true;
                } else {
                    $errorLog = "Transaction Failed \n";
                    if ($tresponse->getErrors() != null) {
                        $errorLog .= "Charge Customer Profile - Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                        $errorLog .= " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                        Log::channel('payment-transaction')->error($errorLog);
                    }
                    return false;
                }
                
            } else {
                $errorLog = "Transaction Failed \n";
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $errorLog .= "CustomerProfileId#" . $customer->profileId . "\n";
                    $errorLog .= " Charge Customer Profile - Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                    $errorLog .= " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                    Log::channel('payment-transaction')->error($errorLog);
                } else {
                    $errorLog .= "CustomerProfileId#" . $customer->profileId . "\n";
                    $errorLog .= "Charge Customer Profile - Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
                    $errorLog .= " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
                    Log::channel('payment-transaction')->error($errorLog);
                }

                return false;
            }
        } else {
            $errorLog = "No response returned \n";
            $errorLog .= "Charge CustomerProfileId#" . $customer->profileId . "\n";
            Log::channel('payment-transaction')->error($errorLog);
            return false;
        }

        // return $response;
    }
}
