<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Payments\SubscriptionController;
use App\Http\Controllers\Payments\CustomerProfileController;
use App\PaymentSubscription;
use App\PaymentUserPlan;
use App\PricingPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PricingPlanController extends Controller
{
    public function update(Request $request)
    {
        $user = Auth::user();
        $plan = PricingPlan::where('name', $request->plan)->first();
        $subscription = new SubscriptionController();
        $customer = new CustomerProfileController();
        
        $paymentPlan = PaymentUserPlan::with('plan')->where('user_id', $user->id)->first();
        $paymentSubscription = PaymentSubscription::where('user_id', $user->id)->where('status', 'active')->first();
        
        if ($paymentSubscription) {
            if (!$subscription->cancelSubscription($paymentSubscription->subscription_id)) {
                return response()->json(['message' => 'Updated plan failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422);
            }
            $paymentSubscription->update(['status' => 'canceled']);
        }

        if ($plan->name == 'percase') {
            
            $availableCases = 0;
        
        }elseif ($plan->name != 'percase' ) {
            sleep(10);
            if (!$subscription->createSubscriptionFromCustomerProfile($customer->getCustomer(), $plan)) {
                return response()->json(['message' => 'Updated plan failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422);
            }

            if ($plan->tier > $paymentPlan->tier) {
                $availableCases = $user->availableCases + $plan->number_of_cases;
            }else{
                $availableCases = $plan->number_of_cases;
            }
            
            
        }
        $user->clinic()->update(['available_cases' => $availableCases]);
        $paymentPlan->update(['plan_id' => $plan->id]);
        
        return response()->json(['message' => 'Successfully updated plan.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }
}
