<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Payments\MerchantController;
use App\Http\Controllers\Payments\SubscriptionController;
use App\Http\Controllers\Payments\CustomerProfileController;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\PaymentSubscription;
use Log;

class PaymentTransactionController extends MerchantController
{

    public static function charge($customer, $plan)
    {
        $paymentTransaction = new CustomerProfileController();
        return $paymentTransaction->chargeCustomerProfile($customer, $plan);
    }

    public static function subscribe($customer, $plan)
    {
        $paymentTransaction = new SubscriptionController();
        return $paymentTransaction->createSubscriptionFromCustomerProfile($customer, $plan);
    }
    
}
