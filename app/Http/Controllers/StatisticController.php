<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Clinic;
use App\CaseModel;
use App\Client;
use App\Patient;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
	private $user;
    private $role;

	public function __construct(Request $request, JWTAuth $jwt){

        $token = $jwt->getToken();
        $this->user = $jwt->toUser($token);
        $this->role = $this->user->role;
    }

    public function index()
    {
    	$stats = [];
    	$auth = Auth::user();
		
		$userRequestCount = User::where('active', 0)->where('role', '=', 'user');
		if ($auth->role === 'clinic_admin') {
			$userRequestCount->where('clinic_id', $auth->clinic_id);
		}
		
		$userRequestCount = $userRequestCount->get()->count();
		
		$stats['userRequestCount'] = $userRequestCount;
    	$stats['clinicRequestCount'] = Clinic::where('active', 0)->get()->count();

    	return response()->json($stats, 200);
    }

	public function getStatisticsByRole(Request $request)
	{
		switch ($this->role) {
			case 'admin':
				return $this->getStatisticsBySuperadmin($request);
				break;
			
			case 'clinic_admin' :
				return $this->getStatisticsByClinicAdmin($request);
				break;
			
			case 'user' :
			case 'trial_user' :
				return $this->getStatisticsByUser($request);
				break;
		}
	}

	private function getStatisticsBySuperadmin($request)
	{
		$clinicId = $request->clinic_id;
		$params = [
			'clinicId' => $clinicId,
			'userId' => $request->user_id,
		];
		
		$stats = [];
		$stats['clinics'] = $this->clinicCount($request);
		$stats['doctors'] = $this->userCount($params);
		$stats['abnormalCase'] = $this->abnormalCaseCount($params);
		$stats['normalCase'] = $this->normalCaseCount($params);
		$stats['usecaseData'] = $this->useCaseData($params);
		$stats['weeklyData'] = $this->weeklyData($request);
		$stats['monthlyData'] = $this->monthlyData($request);
		$stats['caseStatistics'] = $this->caseStatistics($request);
		$stats['clinicPerformanse'] = $this->clinicPerformanse($request);

		return $stats;
	}

	private function getStatisticsByClinicAdmin($request)
	{
		$clinicId = $this->user->clinic_id;
		$params = [
			'clinicId' => $clinicId,
			'userId' => $request->user_id,
		];

		$stats = [];
		$stats['doctors'] = $this->userCount($params);
		$stats['cases'] = $this->caseCount($params);
		$stats['clients'] = $this->clientsCount($params);
		$stats['patients'] = $this->patientsCount($params);
		$stats['usecaseData'] = $this->useCaseData($params);
		$stats['weeklyData'] = $this->weeklyData($request);
		$stats['monthlyData'] = $this->monthlyData($request);
		$stats['caseStatistics'] = $this->caseStatistics($request);
		$stats['doctorPerformanse'] = $this->doctorPerformanse($request);

		return $stats;
	}

	private function getStatisticsByUser($request)
	{
		$userId = $this->user->id;
		$clinicId = $this->user->clinic_id;
		$params = ['userId' => $userId];
		$request->clinic_id = $clinicId;

		$stats = [];
		$stats['cases'] = $this->caseCount($params);
		$stats['abnormalCase'] = $this->abnormalCaseCount($params);
		$stats['clients'] = $this->clientsCount($params);
		$stats['patients'] = $this->patientsCount($params);
		$stats['usecaseData'] = $this->useCaseData($params);
		$stats['weeklyData'] = $this->weeklyData($request);
		$stats['monthlyData'] = $this->monthlyData($request);	
		$stats['caseStatistics'] = $this->caseStatistics($request);

		return $stats;
	}

	private function clinicCount($request)
	{
		if ($request->filled('clinic_id')) {
			return 0;
		}
		return Clinic::get()->count();
	}

	private function userCount($params)
	{
		$users = User::where('role', 'user');
		
		if (isset($params['clinicId'])) {
			$users->where('clinic_id', $params['clinicId']);
		}

		if (isset($params['userId'])) {
			return 0;
		}
		
		$users->get();
		return $users->count();
	}

	private function patientsCount($params)
	{
		$patients = Patient::query();
		
		if (isset($params['from'])) {
			$patients->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$patients->whereDate('created_at', '<=', $params['to']);
		}

		if (isset($params['clinicId'])) {
			$patients->whereHas('client.user', function($q) use ($params){
				$q->where('users.clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$patients->whereHas('client', function($q) use ($params){
				$q->where('user_id', $params['userId']);
			});
		}

		$patients = $patients->get();
		return $patients->count();
	}

	private function clientsCount($params)
	{
		$clients = Client::query();
		
		if (isset($params['from'])) {
			$clients->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$clients->whereDate('created_at', '<=', $params['to']);
		}

		if (isset($params['clinicId'])) {
			$clients->whereHas('user', function($q) use ($params){
				$q->where('clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$clients->where('user_id', $params['userId']);
		}

		$clients = $clients->get();
		return $clients->count();
	}

	private function caseCount($params)
	{
		$cases = CaseModel::whereIn('case_type_id', [1,3]);
		
		if (isset($params['from'])) {
			$cases->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$cases->whereDate('created_at', '<=', $params['to']);
		}

		if (isset($params['clinicId'])) {
			$cases->whereHas('user', function($q) use ($params){
				$q->where('clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$cases->where('user_id', $params['userId']);
		}

		$cases = $cases->get();
		return $cases->count();

	}

	private function clinicCasesCount()
	{
		$cases = DB::table('cases')->join('users', 'cases.user_id', 'users.id')
						  ->join('clinics', 'users.clinic_id', 'clinics.id')
						  ->whereIn('case_type_id', [1,3])
						  ->groupBy('clinics.id')
						  ->select('clinics.id', 'clinics.name')
						  ->selectRaw('COUNT(*) as casesCount')
						  ->orderBy('casesCount', 'desc');
						  
						 
		$cases = $cases->get();
		return $cases;
	}

	private function doctorCasesCount($clinicId)
	{
		$cases = DB::table('cases')->join('users', 'cases.user_id', 'users.id')
						  ->join('clinics', 'users.clinic_id', 'clinics.id')
						  ->whereIn('case_type_id', [1,3])
						  ->where('users.clinic_id', $clinicId)
						  ->groupBy('cases.user_id')
						  ->select('users.id')
						  ->selectRaw("CONCAT_WS(' ', CONCAT_WS('', `title`,`first_name`), `middle_name`, `last_name`) as name, COUNT(*) as casesCount")
						  ->orderBy('casesCount', 'desc');
						  
						 
		$cases = $cases->get();
		return $cases;
	}

	private function abnormalCases($params)
	{
		
		$cases = CaseModel::whereIn('case_type_id', [1,3]);
		$cases->where(function($q){
			$q->whereHas('images', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval->eval', 'ABNORMAL');
			});
			$q->orWhereHas('images.aoiImage', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval->eval', 'ABNORMAL');
			});
		});

		if (isset($params['clinicId'])) {
			$cases->whereHas('user', function($q) use ($params){
				$q->where('clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$cases->where('user_id', $params['userId']);
		}
		
		if (isset($params['from'])) {
			$cases->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$cases->whereDate('created_at', '<=', $params['to']);
		}

		return $cases->get();
	}

	private function normalCases($params)
	{
		$cases = CaseModel::whereIn('case_type_id', [1,3]);
		$cases->where(function($q){
			$q->whereHas('images', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval->eval', 'NORMAL');
			});
			$q->orWhereHas('images.aoiImage', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval->eval', 'NORMAL');
			});
		});

		if (isset($params['clinicId'])) {
			$cases->whereHas('user', function($q) use ($params){
				$q->where('clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$cases->where('user_id', $params['userId']);
		}

		if (isset($params['from'])) {
			$cases->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$cases->whereDate('created_at', '<=', $params['to']);
		}
		return $cases->get();
	}

	private function abnormalCaseCount($params)
	{
		$cases = $this->abnormalCases($params);

		return $cases->count();
	}

	private function normalCaseCount($params)
	{
		$cases = $this->normalCases($params);
		return $cases->count();
	}

	private function benignCaseCount($params)
	{
		$cases = CaseModel::whereIn('case_type_id', [1,3]);
		$cases->where(function($q){
			$q->whereHas('images', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval2->eval', 'BENIGN');
			});
			$q->orWhereHas('images.aoiImage', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval2->eval', 'BENIGN');
			});
		});

		if (isset($params['clinicId'])) {
			$cases->whereHas('user', function($q) use ($params){
				$q->where('clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$cases->where('user_id', $params['userId']);
		}

		if (isset($params['from'])) {
			$cases->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$cases->whereDate('created_at', '<=', $params['to']);
		}
		return $cases->get()->count();
	}

	private function malignantCaseCount($params)
	{
		
		$cases = CaseModel::whereIn('case_type_id', [1,3]);
		$cases->where(function($q){
			$q->whereHas('images', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval2->eval', 'MALIGNANT');
			});
			$q->orWhereHas('images.aoiImage', function($query){
				$query->whereNotNull('ml');
				$query->whereJsonContains('ml->eval2->eval', 'MALIGNANT');
			});
		});

		if (isset($params['clinicId'])) {
			$cases->whereHas('user', function($q) use ($params){
				$q->where('clinic_id', $params['clinicId']);
			});
		}

		if (isset($params['userId'])) {
			$cases->where('user_id', $params['userId']);
		}

		if (isset($params['from'])) {
			$cases->whereDate('created_at', '>=', $params['from']);
		}
		if (isset($params['to'])) {
			$cases->whereDate('created_at', '<=', $params['to']);
		}
		return $cases->get()->count();
	}

	private function useCaseData($params)
	{
		$cases = DB::table('cases')->leftJoin('algorithm', 'cases.algorithm_id', 'algorithm.id')
								   ->leftJoin('users', 'cases.user_id', 'users.id')
								   ->whereNotNull('algorithm_id')
								   ->whereIn('case_type_id', [1,3]);
		if (isset($params['clinicId'])) {
			$cases->where('users.clinic_id', $params['clinicId']);
		}

		if (isset($params['userId'])) {
			$cases->where('users.id', $params['userId']);
		}
								   
		$cases = $cases->groupBy('algorithm_id')
						->selectRaw("COUNT(*) as numberOfCases, algorithm.name as usecaseName, algorithm.suffix as usecaseSuffix")
						->orderBy('numberOfCases', 'desc')
						->get();

		return $cases;
	}

	private function clinicPerformanse($request)
	{
		$clinicsPerformanse = $this->clinicCasesCount();
		$req = $request;
		foreach ($clinicsPerformanse as $key => $clinic) {
			$req->clinic_id = $clinic->id;
			$clinic->weeklyData = $this->weeklyData($req);
			$clinic->monthlyData = $this->monthlyData($req);
		}

		return $clinicsPerformanse;
	}

	private function doctorPerformanse($request)
	{
		$doctorPerformanse = $this->doctorCasesCount($request->clinic_id);
		$req = $request;
		foreach ($doctorPerformanse as $key => $doctor) {
			$req->user_id = $doctor->id;
			$doctor->weeklyData = $this->weeklyData($req);
		}
		return $doctorPerformanse;
	}

	private function caseStatistics($request)
	{
		
		$caseStatistics['weekly'] = $this->weeklyCaseStatistics($request);
		$caseStatistics['monthly'] = $this->monthlyCaseStatistics($request);

		return $caseStatistics;
	}

	private function weeklyData($request)
	{
		$lastWeekStartDay = Carbon::now()->subWeek()->startOfWeek();
		$lastWeekEndDay = Carbon::now()->subWeek()->endOfWeek();
		
		$thisWeekStartDay = Carbon::now()->startOfWeek();
		$thisWeekEndDay = Carbon::now()->endOfWeek();

		$params['lastWeek'] = [
			'from' => $lastWeekStartDay,
			'to' => $lastWeekEndDay,
			'clinicId' => in_array($this->role, ['clinic_admin', 'user']) ? $this->user->clinic_id : $request->clinic_id,
			'userId' => in_array($this->role, ['user', 'trial_user']) ? $this->user->id : $request->user_id,
		];

		$params['thisWeek'] = [
			'from' => $thisWeekStartDay,
			'to' => $thisWeekEndDay,
			'clinicId' => in_array($this->role, ['clinic_admin', 'user']) ? $this->user->clinic_id : $request->clinic_id,
			'userId' => in_array($this->role, ['user', 'trial_user']) ? $this->user->id : $request->user_id,
		];
		
		$weeklyData = [];
		$weeklyData['newCases'] = $this->caseCount(["from" => $thisWeekStartDay]);
		$weeklyData['numberOfCasesIncrease'] = $this->numberOfCasesWeeklyIncrease($params);
		$weeklyData['numberOfPatientsIncrease'] = $this->numberOfPatientsWeeklyIncrease($params);
		$weeklyData['numberOfClientsIncrease'] = $this->numberOfClientsWeeklyIncrease($params);
		$weeklyData['numberOfNormalCasesIncrease'] = $this->numberOfNormalCasesWeeklyIncrease($params);
		$weeklyData['numberOfBenignCasesIncrease'] = $this->numberOfBenignCasesWeeklyIncrease($params);
		$weeklyData['numberOfMalignantCasesIncrease'] = $this->numberOfMalignantCasesWeeklyIncrease($params);

		return $weeklyData;
	}

	private function monthlyData($request)
	{
		$lastMonthStartDay = Carbon::now()->subMonth()->startOfMonth();
		$lastMonthEndDay = Carbon::now()->subMonth()->endOfMonth();
		
		$thisMonthStartDay = Carbon::now()->startOfMonth();
		$thisMonthEndDay = Carbon::now()->endOfMonth();

		$params['lastMonth'] = [
			'from' => $lastMonthStartDay,
			'to' => $lastMonthEndDay,
			'clinicId' => in_array($this->role, ['clinic_admin', 'user']) ? $this->user->clinic_id : $request->clinic_id,
			'userId' => in_array($this->role, ['user', 'trial_user']) ? $this->user->id : $request->user_id,
		];
		$params['thisMonth'] = [
			'from' => $thisMonthStartDay,
			'to' => $thisMonthEndDay,
			'clinicId' => in_array($this->role, ['clinic_admin', 'user']) ? $this->user->clinic_id : $request->clinic_id,
			'userId' => in_array($this->role, ['user', 'trial_user']) ? $this->user->id : $request->user_id,
		];

		$monthlyData = [];
		$monthlyData['newCases'] = $this->caseCount(["from" => $thisMonthStartDay]);
		$monthlyData['numberOfCasesIncrease'] = $this->numberOfCasesMonthlyIncrease($params);
		$monthlyData['numberOfPatientsIncrease'] = $this->numberOfPatientsMonthlyIncrease($params);
		$monthlyData['numberOfClientsIncrease'] = $this->numberOfClientsMonthlyIncrease($params);
		$monthlyData['numberOfNormalCasesIncrease'] = $this->numberOfNormalCasesMonthlyIncrease($params);
		$monthlyData['numberOfBenignCasesIncrease'] = $this->numberOfBenignCasesMonthlyIncrease($params);
		$monthlyData['numberOfMalignantCasesIncrease'] = $this->numberOfMalignantCasesMonthlyIncrease($params);

		return $monthlyData;
	}

	private function weeklyCaseStatistics($request)
	{
		$startDay = Carbon::now()->subDays(7);
		
		for ($i=0; $i < 7; $i++) { 
			$day = $startDay->addDay();
			$params = [
				'from' => $day,
				'to' => $day,
				'clinicId' => in_array($this->role, ['clinic_admin', 'user']) ? $this->user->clinic_id : $request->clinic_id,
				'userId' => in_array($this->role, ['user', 'trial_user']) ? $this->user->id : $request->user_id,
			];
			$weeklyCaseStats[$i]['name'] = $startDay->format('D');
			$weeklyCaseStats[$i]['series'][0]['name'] = 'Abnormal';
			$weeklyCaseStats[$i]['series'][0]['value'] = $this->abnormalCases($params)->count();

			$weeklyCaseStats[$i]['series'][1]['name'] = 'Normal';
			$weeklyCaseStats[$i]['series'][1]['value'] = $this->normalCases($params)->count();
		}
		
		return (array)$weeklyCaseStats;
	}

	private function monthlyCaseStatistics($request)
	{
		$j = 0;
 		for ($i=5; $i >= 0; $i--) { 
			
			$startMonth = Carbon::now()->subMonth($i)->startOfMonth();
			$endMonth = Carbon::now()->subMonth($i)->endOfMonth();
			
			$start = $startMonth;
			$end = $endMonth;
			$params = [
				'from' => $start,
				'to' => $end,
				'clinicId' => in_array($this->role, ['clinic_admin', 'user']) ? $this->user->clinic_id : $request->clinic_id,
				'userId' => in_array($this->role, ['user', 'trial_user']) ? $this->user->id : $request->user_id,
			];
			
			$monthlyCaseStats[$j]['name'] = $startMonth->format('M');
			$monthlyCaseStats[$j]['series'][0]['name'] = 'Abnormal';
			$monthlyCaseStats[$j]['series'][0]['value'] = $this->abnormalCases($params)->count();

			$monthlyCaseStats[$j]['series'][1]['name'] = 'Normal';
			$monthlyCaseStats[$j]['series'][1]['value'] = $this->normalCases($params)->count();
			
			$j++;
		}
		
		return $monthlyCaseStats;
	}

	private function numberOfCasesWeeklyIncrease($params)
	{
		$numberOfCaseLastWeek = $this->caseCount($params['lastWeek']);
		$numberOfCaseThisWeek = $this->caseCount($params['thisWeek']);
		
		return $this->calculateIncrease($numberOfCaseThisWeek, $numberOfCaseLastWeek);

	}

	private function numberOfNormalCasesWeeklyIncrease($params)
	{
		$numberOfCaseLastWeek = $this->normalCaseCount($params['lastWeek']);
		$numberOfCaseThisWeek = $this->normalCaseCount($params['thisWeek']);
		
		return $this->calculateIncrease($numberOfCaseThisWeek, $numberOfCaseLastWeek);
	}

	private function numberOfBenignCasesWeeklyIncrease($params)
	{
		$numberOfCaseLastWeek = $this->benignCaseCount($params['lastWeek']);
		$numberOfCaseThisWeek = $this->benignCaseCount($params['thisWeek']);
		
		return $this->calculateIncrease($numberOfCaseThisWeek, $numberOfCaseLastWeek);

	}

	private function numberOfMalignantCasesWeeklyIncrease($params)
	{
		$numberOfCaseLastWeek = $this->malignantCaseCount($params['lastWeek']);
		$numberOfCaseThisWeek = $this->malignantCaseCount($params['thisWeek']);
		
		return $this->calculateIncrease($numberOfCaseThisWeek, $numberOfCaseLastWeek);

	}

	private function numberOfPatientsWeeklyIncrease($params)
	{
		$numberOfPatientsLastWeek = $this->patientsCount($params['lastWeek']);
		$numberOfPatientsThisWeek = $this->patientsCount($params['thisWeek']);
		
		return $this->calculateIncrease($numberOfPatientsThisWeek, $numberOfPatientsLastWeek);
	}

	private function numberOfClientsWeeklyIncrease($params)
	{
		$numberOfClientsLastWeek = $this->clientsCount($params['lastWeek']);
		$numberOfClientsThisWeek = $this->clientsCount($params['thisWeek']);
		
		return $this->calculateIncrease($numberOfClientsThisWeek, $numberOfClientsLastWeek);
	}
	
	private function numberOfNormalCasesMonthlyIncrease($params)
	{
		$numberOfCaseLastMonth = $this->normalCaseCount($params['lastMonth']);
		$numberOfCaseThisMonth = $this->normalCaseCount($params['thisMonth']);
		
		return $this->calculateIncrease($numberOfCaseThisMonth, $numberOfCaseLastMonth);
	}

	private function numberOfBenignCasesMonthlyIncrease($params)
	{
		$numberOfCaseLastMonth = $this->benignCaseCount($params['lastMonth']);
		$numberOfCaseThisMonth = $this->benignCaseCount($params['thisMonth']);
		
		return $this->calculateIncrease($numberOfCaseThisMonth, $numberOfCaseLastMonth);
	}

	private function numberOfMalignantCasesMonthlyIncrease($params)
	{
		$numberOfCaseLastMonth = $this->malignantCaseCount($params['lastMonth']);
		$numberOfCaseThisMonth = $this->malignantCaseCount($params['thisMonth']);
		
		return $this->calculateIncrease($numberOfCaseThisMonth, $numberOfCaseLastMonth);
	}

	private function numberOfCasesMonthlyIncrease($params)
	{
		$numberOfCaseLastMonth = $this->caseCount($params['lastMonth']);
		$numberOfCaseThisMonth = $this->caseCount($params['thisMonth']);
		
		return $this->calculateIncrease($numberOfCaseThisMonth, $numberOfCaseLastMonth);
	}
	
	private function numberOfPatientsMonthlyIncrease($params)
	{
		$numberOfPatientsLastMonth = $this->patientsCount($params['lastMonth']);
		$numberOfPatientsThisMonth = $this->patientsCount($params['thisMonth']);
		
		return $this->calculateIncrease($numberOfPatientsThisMonth, $numberOfPatientsLastMonth);
	}

	private function numberOfClientsMonthlyIncrease($params)
	{
		$numberOfClientsLastMonth = $this->clientsCount($params['lastMonth']);
		$numberOfClientsThisMonth = $this->clientsCount($params['thisMonth']);
		
		return $this->calculateIncrease($numberOfClientsThisMonth, $numberOfClientsLastMonth);
	}

	private function calculateIncrease($current, $previous)
	{
		if ($previous === 0 && $current === 0) {
			return 0;
		}elseif ($previous === 0 && $current !== 0) {
			return 100;
		}else{
			return ($current - $previous) / $previous * 100;
		}
	}
	
}
