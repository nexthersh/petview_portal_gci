<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use File;

class ZipController extends Controller
{
    public static function zip($json, $images, $aoiImages = null, $caseId, $transactionNumber)
    {
    	if (!is_dir(storage_path('app/public/zip/'))) {
          mkdir(storage_path('app/public/zip'));
      }
    	//create zip
        $zip = new \ZipArchive;
        $res = $zip->open(storage_path('app/public/zip/'.$transactionNumber.'.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            if ($res === FALSE) {
                echo 'can not create zip';
            }
        foreach ($images as $key => $image) {
        	
            // add image to zip
            $image_name = prepareImageName($image->getClientOriginalName());
            $image_name = $caseId.'_'.$image_name;
            
            // $imageNameWithoutExtension = substr($image_name, 0, strrpos($image_name, "."));
            $imageNameWithoutExtension = imageNameWithoutExtension($image_name);
            
            $zip->addFile($image->getRealPath(), $transactionNumber.'/'.$transactionNumber.'/'.str_replace(" ", "", $json->body_part[0]->name).'/'.$imageNameWithoutExtension.'/'.$image_name);
            Storage::putFileAs('public/images/in/'.$transactionNumber.'/'.$transactionNumber.'/'.str_replace(" ", "", $json->body_part[0]->name).'/'.$imageNameWithoutExtension, $image->getRealPath(), $image_name);
        }
        // add xml to zip and close zip file
      	$zip->addFile(storage_path('app/public/images/in/'.$transactionNumber.'/'.$transactionNumber.'.xml'), $transactionNumber.'/'.$transactionNumber.'.xml');
      	$zip->close();
    }

    public static function zipImages($filename, $transactionId)
    {
        $zip = new \ZipArchive;
        if (!is_dir(public_path('downloads/zip/'))) {
            mkdir(public_path('downloads/zip/'), 0777, true);
        }
        
        $res = $zip->open('downloads/zip/'.$filename.'.zip', \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        if ($res === FALSE) {
            echo 'can not create zip';
        }

        $rootPath = public_path('/downloads/'.$transactionId);
        // Create recursive directory iterator
        
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        $link = url('/downloads/zip/'.$filename.'.zip');

        return $link;
    }

}
