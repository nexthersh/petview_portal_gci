<?php

namespace App\Http\Controllers;

use App\ReferentImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReferentImageRequest;
use App\Http\Traits\Audit;
use Carbon\Carbon;

class ReferentImageController extends Controller
{
    use Audit;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAoiTypeReferentImages()
    {
        $images = ReferentImage::whereNotNull('aoi_type')->whereNotNull('image_input_type')->get()->groupBy('aoi_type');

        return response()->json($images, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReferentImageRequest $request)
    {
        $data = $request->only('algorithm_id', 'image_type_id', 'aoi_type', 'image_input_type');

        foreach ($request->images as $key => $image) {
            $data['name'] = str_random(20).'_'.$image->getClientOriginalName();
            ReferentImage::create($data);
        }

        return response()->json(['message' => 'Successfully upload images.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReferentImage  $referentImage
     * @return \Illuminate\Http\Response
     */
    public function show(ReferentImage $referentImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReferentImage  $referentImage
     * @return \Illuminate\Http\Response
     */
    public function edit(ReferentImage $referentImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReferentImage  $referentImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReferentImage $referentImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReferentImage  $referentImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReferentImage $referentImage)
    {
        //
    }

    public function deleteAll(Request $request)
    { 
          $ids = explode(',', $request->ids);
          
          ReferentImage::whereIn('id', $ids)->delete();

          return response()->json(['message' => 'Successfully deleted images.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }
}
