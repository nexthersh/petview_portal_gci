<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ZipController;
use App\Http\Controllers\AlgorithmControlller;
use App\ProcessedImage;
use App\Mail\Notifications;
use Mail;
use File;
use Carbon\Carbon;
use App\Images;
use Auth;


class ImagesController extends Controller
{
    private static $client;

    public static function create($imagesData, $caseId,  $transactionNumber, $aoiImages, $case_type = null)
    {
      
      switch ($case_type) {
        case '1':
          self::assignValueNormalCase($imagesData, $caseId, $transactionNumber, $aoiImages);
          break;
        case '2':
          self::assignValueNoProcessingCase($imagesData, $caseId, $transactionNumber, $aoiImages);
          break;
        case '3':
          self::assignValueQuickCase($imagesData, $caseId, $transactionNumber, $aoiImages);
          break;
        default:
          die();
          break;
      }
    }

    private static function assignValueNormalCase($imagesData, $caseId, $transactionNumber, $aoiImages)
    {

      $imageModel = new Images;

      foreach ($imagesData as $key => $data) {
      $imageName = prepareImageName($data->image_name);
      $insert = [
                'name' => $caseId.'_'.$imageName,
                'original_name' => $data->original_image_name,
                'transaction_id' => $transactionNumber,
                'image_type_id' => @$data->image_type,
                'other_image_type' => @$data->other_image_type,
                'case_id' => $caseId,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
              ];

        if (@$data->aoi_coords) {
                foreach($data->aoi_coords as $coords){
                  foreach ($coords->aoi as $aoi) {
                    foreach ($aoi->dimensions as  $aoi_dimensions) {
                      foreach ((array)$aoi_dimensions as $dimension_key => $dimensions) {

                        $insert['aoi_coords'][$coords->aoiTypeName][] = $dimension_key.':'. $dimensions; 
                      }
                    }
                    $insert['aoi_coords'][$coords->aoiTypeName][] = 'shape'.':'.$aoi->shape;
                  }
                }
                $insert['aoi_coords'] = serialize($insert['aoi_coords']);
        }

        if (isset($data->pathology)) {
          $insert['pathology'] = $data->pathology;
        }
        if (isset($data->outline)) {
          $insert['outline'] = $data->outline;
        }
        $originalImage = $imageModel->insertGetId($insert);
        // $filename = str_replace([" ","-"], "_", explode(".", $data->image_name)[0]);
        $filename = $imageName;

        //upload aoi images to aws
        // dd($data);
            if (isset($data->aoi_coords) AND !empty($data->aoi_coords)){
                foreach ($data->aoi_coords as $aoi_key => $aoiData) {
                    AoiImagesController::store($aoiData, $originalImage);

                    $sourceAoiImage = $aoiImages[$key][$aoi_key]->getRealPath();
                    $imageAoiDestionation = 'public/images/aoi/'.$transactionNumber.'/'.$caseId.'_'.$filename;
                    Storage::makeDirectory($imageAoiDestionation);
                    Storage::putFileAs($imageAoiDestionation, $sourceAoiImage, str_replace("ROI", "AOI", str_replace([' ', '-'], '_', $aoiImages[$key][$aoi_key]->getClientOriginalName())));
                  }
                
            }
      }

    }

    private static function assignValueNoProcessingCase($imageData, $caseId, $transactionNumber, $aoiImages)
    {
      $imageModel = new Images;
           // dd($aoiImages);
        $insert = [
                  'name' => $imageData->image_name,
                  'original_name' => $imageData->original_image_name,
                  'transaction_id' => $transactionNumber,
                  'image_type_id' => @$imageData->image_type->id,
                  'algorithm_id' => @$imageData->algorithm[0]->id,
                  'body_part_id' => @$imageData->body_part->id,
                  'body_view_id' => @$imageData->body_view->id,
                  'patient_type_id' => @$imageData->patient_type_id->id,
                  'breed_id' => @$imageData->breed_id->id,
                  'image_modality_id' => @$imageData->image_modality->id,
                  'gender' => @$imageData->gender->name,
                  'altered_status_id' => @$imageData->altered_status->id,
                  'other_image_type' => @$imageData->other_image_type,
                  'differential_diagnosis' => @$imageData->differential_diagnosis,
                  'diagnosis' => @$imageData->diagnosis,
                  'age' => @$imageData->age,
                  'case_id' => $caseId,
                  'pathology' => @$imageData->pathology,
                  'outline' => @$imageData->outline,
                  'created_at' => Carbon::now()->toDateTimeString(),
                  'updated_at' => Carbon::now()->toDateTimeString()
                ];

          if (@$imageData->aoi_coords) {
                foreach($imageData->aoi_coords as $coords){
                  foreach ($coords->aoi as $aoi) {
                    foreach ($aoi->dimensions as  $aoi_dimensions) {
                      foreach ((array)$aoi_dimensions as $dimension_key => $dimensions) {

                        $insert['aoi_coords'][$coords->aoiTypeName][] = $dimension_key.':'. $dimensions; 
                      }
                    }
                    $insert['aoi_coords'][$coords->aoiTypeName][] = 'shape'.':'.$aoi->shape;
                  }
                }
                $insert['aoi_coords'] = serialize($insert['aoi_coords']);
          }

          $originalImage = $imageModel->insertGetId($insert);
          $filename = imageNameWithoutExtension($imageData->image_name);

          //upload aoi images to aws
          // dd($data);
              if (isset($imageData->aoi_coords) AND !empty($imageData->aoi_coords)){
                  foreach ($imageData->aoi_coords as $aoi_key => $aoiData) {
                      AoiImagesController::store($aoiData, $originalImage, $caseTypeId = 2);

                      $sourceAoiImage = $aoiImages[$aoi_key]->getRealPath();
                      Storage::putFileAs('public/images/aoi/'.$transactionNumber.'/'.$filename, $sourceAoiImage, $aoiImages[$aoi_key]->getClientOriginalName());
                  }
                
            }

          return $originalImage;
    }

    private static function assignValueQuickCase($imagesData, $caseId, $transactionNumber, $aoiImages)
    {
      $imageModel = new Images;

      foreach ($imagesData as $key => $data) {
      $imageName = prepareImageName($data->image_name);
      $insert = [
                'name' => $caseId.'_'.$imageName,//
                'original_name' => $data->original_image_name,
                'transaction_id' => $transactionNumber,
                'case_id' => $caseId,//
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
              ];

        if (@$data->aoi_coords) {
                foreach($data->aoi_coords as $coords){
                  foreach ($coords->aoi as $aoi) {
                    foreach ($aoi->dimensions as  $aoi_dimensions) {
                      foreach ((array)$aoi_dimensions as $dimension_key => $dimensions) {

                        $insert['aoi_coords'][$coords->aoiTypeName][] = $dimension_key.':'. $dimensions; 
                      }
                    }
                    $insert['aoi_coords'][$coords->aoiTypeName][] = 'shape'.':'.$aoi->shape;
                  }
                }
                $insert['aoi_coords'] = serialize($insert['aoi_coords']);
        }
        
         if (isset($data->pathology)) {
          $insert['pathology'] = $data->pathology;
        }
        if (isset($data->outline)) {
          $insert['outline'] = $data->outline;
        }
       
        $originalImage = $imageModel->insertGetId($insert);
        $filename = $imageName;

        //upload aoi images to aws
        // dd($data);
            if (isset($data->aoi_coords) AND !empty($data->aoi_coords)){
                foreach ($data->aoi_coords as $aoi_key => $aoiData) {
                    AoiImagesController::store($aoiData, $originalImage);

                    $sourceAoiImage = $aoiImages[$key][$aoi_key]->getRealPath();
                    $imageAoiDestionation = 'public/images/aoi/'.$transactionNumber.'/'.$caseId.'_'.$filename;
                    Storage::makeDirectory($imageAoiDestionation);
                    Storage::putFileAs($imageAoiDestionation, $sourceAoiImage, str_replace("ROI", "AOI", str_replace([' ', '-'], '_', $aoiImages[$key][$aoi_key]->getClientOriginalName())));
                  }
            }
      }
    }

    public function downloadImage(Request $request)
    {
      $link = $this->generateDownloadLink($request->images);
      
      if ($request->has('email')) {
        
        $this->sendMail($request, $link);
        
        return response()->json(['message' => 'Email sent.'], 200);
      
      }else{
        return response()->json(['link' => $link], 200);
      }
    }

    public function generateDownloadLink($images)
    {
      if(!is_dir(storage_path('app/public/downloads'))){
          Storage::makeDirectory('public/downloads');
      }
      // dd($images);
      foreach ($images as $image) {
        // dd($image);
        $objects = [];
        $image = json_decode($image);
        $transactionId = $image->transaction_id;
        $imageNameWithoutExtension = imageNameWithoutExtension($image->image_name);
        
        if($image->case_type_id == 2) {
          
          if (isset($image->original_image) && $image->original_image) {
            $objects['img']= 'develop/'.$image->transaction_id.'/'.$imageNameWithoutExtension.'/'.$image->image_name;
          }
           $caseId = explode('_', $image->transaction_id)[1];
           $objects['xml'] = 'develop/'.$image->transaction_id.'/'.$imageNameWithoutExtension.'/'.$caseId.'_'.$imageNameWithoutExtension.'.xml';

            // if (isset($image->aoi)) {

            //   foreach ($image->aoi as $key => $aoi) {
                // $aoiImageName = str_replace(config('aws.AWS_URL').'/'.config('aws.AWS_AOI_FOLDER').$image->transaction_id.'/'.$imageNameWithoutExtension.'/' , '', $aoi);
                // $objects[$aoiImageName] = config('aws.AWS_AOI_FOLDER').$image->transaction_id.'/'.$imageNameWithoutExtension.'/'.$aoiImageName;
            //   }
            // }
        }elseif ($image->case_type_id == 4) {
          // if (isset($image->original_image) && $image->original_image) {
          //   $objects['img']= config('aws.AWS_CHICKEN_UPLOAD_DATA').$image->transaction_id.'/'.$image->image_name;
          // }
        
        }elseif($image->case_type_id == 1 || $image->case_type_id == 3) {

          if (isset($image->original_image) && $image->original_image) {
            $objects['img'] = 'in/'.$image->transaction_id.'/'.$image->transaction_id.'/'.str_replace(" ", "", $image->body_part).'/'.$imageNameWithoutExtension.'/'.$image->image_name;
          }
          $objects['xml'] = 'out/'.$image->transaction_id.'_RESULT/'.$image->transaction_id.'.xml';

          if (isset($image->key)) {
             foreach ($image->key as $index => $value) {
               $objects['img_processed_'.$index] = 'out/'.$value;
             }
          }
          // if (isset($image->aoi) AND is_array($image->aoi)) {
          //   foreach ($image->aoi as $key => $aoi) {

          //     $objects['AoiImage_'.$aoi->name] = ltrim(str_replace(config('aws.AWS_URL'), '', $aoi->image), '/');
          //   }
          // }
        }else {
          $objects = [];
        }
      // download xml if selected
      if(!isset($image->xml)){
          unset($objects['xml']);
      }
        if(!is_dir(public_path('downloads/'.$transactionId.'/'.$imageNameWithoutExtension))){
          mkdir(public_path('downloads/'.$transactionId));
          mkdir(public_path('downloads/'.$transactionId.'/'.$imageNameWithoutExtension));
        }
        foreach ($objects as $key => $object) {
          try {
              // Get the object.
              // $result = file_get_contents(url('/storage/images/'.$object));
              
              if ($key == 'xml') {
                copy(storage_path('app/public/images/'.$object), public_path('downloads/'. $transactionId.'/'.$imageNameWithoutExtension.'/'.$imageNameWithoutExtension.'.xml'));
              }elseif(strpos($key, 'AoiImage_')){
                copy(storage_path('app/public/images/'.$object), public_path('downloads/'. $transactionId.'/'.$imageNameWithoutExtension.'/'.$key));
              }else{
                copy(storage_path('app/public/images/'.$object), public_path('downloads/'. $transactionId.'/'.$imageNameWithoutExtension.'/'.substr($object, strrpos($object, "/") + 1)));
              }
          
          } catch (S3Exception $e) {
              echo $e->getMessage() . PHP_EOL;
          }
        }
    }
      $filename = $transactionId.'_'.str_random(4);
      $link = ZipController::zipImages($filename, $transactionId);
      File::deleteDirectory(public_path('/downloads/'.$transactionId));
      return $link;
    }

    public function sendMail($request, $link)
    {
      $data = [
          'text' => 'To download the images, open the link: ',
          'link' => $link
      ];
      $subject = 'Images Download - Petview Portal';

       Mail::to($request->email)->send(new Notifications($data, $subject));
    }
} 
