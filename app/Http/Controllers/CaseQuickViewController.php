<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\XmlController;
use App\Http\Controllers\ZipController;
use App\Http\Controllers\ImagoRestApiController;
use App\Http\Traits\Audit;
use App\CaseModel;
use App\Http\Controllers\Payments\PaymentController;
use Auth;
use Carbon\Carbon;
use File;

class CaseQuickViewController extends Controller
{
    use Audit;

    private $case_type_id = 3;

    public function create(Request $request, CaseModel $caseModel)
    {
        // decode json
        $user = Auth::user();
        
        $json = json_decode($request->json);
        $images = $request->images;
        $aoiImages = $request->aoi_images;
        $imagesData = $json->images;
        
        // insert case and return case ID
        $case = $caseModel->create([
            'case_type_id' => $this->case_type_id,
            'body_part_id' => $json->body_part[0]->id,
            'algorithm_id' => $json->algorithm[0]->id,
            'image_modality_id' => $json->image_modality[0]->id,
            'user_id' => $user->id,
            'patient_refid' => @$json->patient_refid,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        $caseId = $case->id;
        $case->audits()->create($this->assignAuditValues('create', null, $case, $request));
        // create transactio ID
        $transactionNumber = $this->transactionNumber($caseId);
        // insert images
        //$this->storeImages($images, $imagesData, $caseId, $transactionNumber);
        ImagesController::create($imagesData, $caseId, $transactionNumber, @$aoiImages, $this->case_type_id);
        // create xml
        XmlController::xml($json, $caseId, $transactionNumber);
        // create zip
        ZipController::zip($json, $images, $request->aoi_images, $caseId, $transactionNumber);
        // upload zip to AWS
        $destination = $transactionNumber . '.zip';
        $sourceFile = storage_path('app/public/zip/' . $transactionNumber . '.zip');

        try {
            $iceClient = new ImagoRestApiController;
            $handle = $iceClient->upload($sourceFile);
            $case->update(['handle' => $handle]);
            $iceClient->process($handle);
        } catch (\Throwable $th) {
           $case->delete();
           return response()->json(['message' => 'Transaction failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422) ;
        }

        if ($user->availableCases == 0) {
            $payment = new PaymentController();
            $paymentResponse = $payment->paymentPercase();
            if (!$paymentResponse) {
                $case->delete();
                return response()->json(['message' => 'Payment Transaction failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422);
            }
        }else{
            $user->clinic()->update(['available_cases' => $user->availableCases - 1]);
        }
        // delete local zip and xml file
        File::delete(storage_path('app/xml/' . $transactionNumber . '.xml'));
        File::delete(storage_path('app/public/zip/' . $transactionNumber . '.zip'));

        return response()->json(['message' => 'Successfully created the case!', 'handle' => $handle, 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function transactionNumber($caseId)
    {
        $transactionNumber = 'EVC_' . $caseId . '_' . Carbon::now()->format('mdy') . '_' . Carbon::now()->format('Hi');

        return $transactionNumber;
    }
}
