<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Notifications;
use App\Notifications as NotificationsModel;
use Mail;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;
use App\CaseModel;
use Auth;
use Carbon\Carbon;
use Log;
use DB;


class NotificationsController extends Controller
{
    public static function create($case)
    {
        $case_id = $case->id;
        $user_id = $case->user->id;

        //check if exist notifications for this case
        $notification = NotificationsModel::where('user_id', $user_id)->where('case_id', $case_id)->first();

        // send notification
        if (!$notification) {
            // get user firebase token 
            $token = (isset($case->user->token)) ? $case->user->token : "";
            $message = 'Your case is completed!';
            $title = 'Case_' . $case_id;

            CaseModel::where('id', $case_id)->update(['processing' => 0]);

            try {
                NotificationsModel::firstOrCreate(
                    ['user_id' => $user_id, 'case_id' => $case_id],
                    [
                        'user_id' => $user_id, 'case_id' => $case_id,
                        'title' => $title,
                        'message' => $message,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]
                );

                self::sendNotification($title, $message, ['case_id' => $case_id], $token);
            } catch (\Exception $e) {
                // dd($e);
            }
        }
    }

    public static function sendPaymentPercaseNotification($user, $plan)
    {
        // get user firebase token 
        $token = (isset($user->token)) ? $user->token : "";
        $title = 'Your case is created!';
        $message = 'A payment has been made to create a case in the amount of ' . $plan->price_percase . '$ from your payment account on the PetView portal.';

        try {
            NotificationsModel::create([
                'user_id' => $user->id,
                'title' => $title,
                'message' => $message,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            self::sendNotification($title, $message, ['plan_id' => $plan->id], $token);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public static function sendNotification($title, $body, $data, $token)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        // Log::info('Notifications', ['token' => $token, 'options' => $option, 'notification' => $notification, 'data' => $data]);

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }

    public function changeStatus()
    {
        $notification = NotificationsModel::where('status', 0)->update(['status' => 1]);
    }

    public function notificationStatus($id)
    {
        $notification = NotificationsModel::where('id', $id)->update(['status' => 1]);
    }

    public function getNotifications()
    {
        $notifications = DB::table('notifications')
            ->where('user_id', Auth::user()->id)->get();

        foreach ($notifications as $key => $value) {
            $value->zone = config('app.timezone');
        }

        return response()->json($notifications, 200);
    }

    public function delete(Request $request)
    {
        $ids = explode(',', $request->ids);
        NotificationsModel::whereIn('id', $ids)->delete();

        return response()->json(['message' => 'Successfully deleted notifications.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }
}
