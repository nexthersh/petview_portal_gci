<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Breed;

class BreedController extends Controller
{
   public function getBreed($id)
   {
   		$breeds = Breed::where('patient_type_id', $id)->get()->toArray();

   		return response()->json($breeds, 200);
   }
}
