<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AoiImage;

class AoiImagesController extends Controller
{
    public static function store($imageData, $originalImageId, $caseTypeId = null)
    {
        // dd($imageData);
    	$aoiImage = new AoiImage;

        if ($caseTypeId) {
            $imageName = isset($imageData->name) ? $imageData->name : '';
            $aoiTypeName = $imageData->aoiTypeName;
        }else{
            $imageName = isset($imageData->name) ? str_replace("ROI", "AOI", str_replace([" ","-"], "_", $imageData->name)) : '';
            $aoiTypeName = $imageData->aoiTypeName == "ROI" ? "AOI" : $imageData->aoiTypeName;
        }
    	
        $insert = [
    			'image_name' => $imageName,
    			'image_id' => $originalImageId,
    			'aoi_type' => $aoiTypeName,
                'aoi_coords' => json_encode($imageData->aoi),
                'aoi_type_object_json' => @$imageData->aoiTypeObjectJSON,
    			];

        $aoiImage->insert($insert);
    }

    public static function update($imageData, $originalImageId, $caseTypeId = null)
    {
        $aoiImage = AoiImage::where('image_name', $imageData->name)->first();
        
        if ($caseTypeId) {
            $imageName = isset($imageData->name) ? $imageData->name : '';
            $aoiTypeName = $imageData->aoiTypeName;
        }else{
            $imageName = isset($imageData->name) ? str_replace("ROI", "AOI", str_replace([" ","-"], "_", $imageData->name)) : '';
            $aoiTypeName = $imageData->aoiTypeName == "ROI" ? "AOI" : $imageData->aoiTypeName;
        }
       
        $insert = [
                'image_name' => $imageName,
                'image_id' => $originalImageId,
                'aoi_type' => $aoiTypeName,
                'aoi_coords' => json_encode($imageData->aoi),
                'aoi_type_object_json' => @$imageData->aoiTypeObjectJSON
                ];

        AoiImage::updateOrCreate(['image_name' => $imageData->name, 'image_id' => $originalImageId], $insert);
    }
}
