<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Events\SendEmailVerification;
use Carbon\Carbon;
use App\Http\Traits\Audit;
use App\User;
use App\Clinic;
use Str;

class AuthController extends Controller
{

    use Audit;
    
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'checkIfEmailExists']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
       
        if ($token = $this->guard()->attempt($credentials)) {
            
            $user = Auth::user();
            
            // Check that the account has been verified
            if (!$user->email_verified_at) {
                $user->update(['verify_token' => Str::random(50), 'verify_code' => rand(100000, 999999)]);
                event(new SendEmailVerification($user));
                Auth::logout();
                return response()->json(['message' => 'Your account has not been verified, we have sent a verification link to your email address.', 'timestamp' => Carbon::now()->toDateTimeString()], 406);
            }
            
            // add left days and expired data to trial user
            if($user->isTrial){
                $trialLeftDays =  120 - (Carbon::create($user->trial_start_date)->diffInDays(Carbon::now()));
                $trialExpiredDate = Carbon::create($user->trial_start_date)->addDays(120);
                
                // Check if the trial account has expired
                if (Carbon::now()->subDays(120) > Carbon::create($user->trial_start_date)) {
                    $user->update(["role" => "in_process", "active" => 0]);

                    $user->trialLeftDays = $trialLeftDays;
                    $user->trialExpiriedDate = $trialExpiredDate;

                    $redirectUrl = url("/account-type");
                    return response()->json(['message' => 'Your trial account has expired.', 'timestamp' => Carbon::now()->toDateTimeString(), 
                                             'user' => $user,
                                             'redirectUrl' => $redirectUrl
                                            ], 200);
                }

                $user->trialLeftDays = $trialLeftDays;
                $user->trialExpiriedDate = $trialExpiredDate;

            }elseif(!$user->active && $user->role != 'in_process'){
                Auth::logout();
                return response()->json(['message' => 'Your account has not been approved by admin.', 'timestamp' => Carbon::now()->toDateTimeString()], 400);
            }
            
            $user->audits()->create($this->assignAuditValues('login', $user, null, $request));
            
            return $this->respondWithToken($token);
        }

        return response()->json(['status' => 400, 'message' => 'Credentials do not match.', 'timestamp' => Carbon::now()->toDateTimeString()], 400);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() 
    {
        $user = $this->guard()->user();
        if ($user->isTrial) {
            $user->trialLeftDays =  120 - (Carbon::create($user->trial_start_date)->diffInDays(Carbon::now()));
            $user->trialExpiriedDate = Carbon::create($user->trial_start_date)->addDays(120);

        }
        $user->clinic = Clinic::find($user->clinic_id);
        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
            'user' => Auth::user()
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    public function checkIfEmailExists(Request $request)
    {
        $email = $request->email;
        
        return User::where('email', $email)->exists() ?  response()->json(['message' => 'Ok','timestamp' => Carbon::now()->toDateTimeString()], 200)
                                                      :  response()->json(['message' => "Email doesn't exists",'timestamp' => Carbon::now()->toDateTimeString()], 406);

    }
}