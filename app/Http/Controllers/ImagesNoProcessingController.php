<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AlgorithmController;
use App\Images;
use Carbon\Carbon;
use App\Algorithm;
use Storage;

class ImagesNoProcessingController extends Controller
{
    public static function store($imageData, $caseId, $transactionNumber)
    {
		  $imageModel = new Images;
	    	 // dd($imageData);
	      $insert = [
	                'name' => $imageData->image_name,
	                'original_name' => $imageData->original_image_name,
	                'transaction_id' => $transactionNumber,
	                
	                'image_type_id' => @$imageData->image_type->id,
	                'other_image_type' => @$imageData->other_image_type,
	                'case_id' => $caseId,
	                'pathology' => @$imageData->pathology,
	                'outline' => @$imageData->outline,
	                'created_at' => Carbon::now()->toDateTimeString(),
	                'updated_at' => Carbon::now()->toDateTimeString()
	              ];

	        if (isset($imageData->pathology)) {
	          $insert['pathology'] = $imageData->pathology;
	        }
	        if (isset($imageData->outline)) {
	          $insert['outline'] = $imageData->outline;
	        }
	        
	        $imageId = $imageModel->insertGetId($insert);

	        return $imageId;
    }

    public static function update($imageData, $caseId, $imageId)
    {
    	$imageModel = Images::findOrFail($imageId);
	    	  // dd($imageData);
	      $insert = [
	                
	                'image_type_id' => @$imageData->image_type,
	                'other_image_type' => @$imageData->other_image_type,
	                'case_id' => $caseId,
	                'pathology' => @$imageData->pathology,
	                'outline' => @$imageData->outline,
	                'created_at' => Carbon::now()->toDateTimeString(),
	                'updated_at' => Carbon::now()->toDateTimeString()
	              ];

	        if (isset($imageData->pathology)) {
	          $insert['pathology'] = $imageData->pathology;
	        }
	        if (isset($imageData->outline)) {
	          $insert['outline'] = $imageData->outline;
	        }
	        
	        $imageModel->update($insert);

	        return $imageModel->id;
    }
}
