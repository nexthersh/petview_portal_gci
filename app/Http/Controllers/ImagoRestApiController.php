<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ProcessedImagesController;
use App\CaseModel;

class ImagoRestApiController extends Controller
{
    public $uid;
    public $endpoint;

    public function __construct()
    {
        $this->setUid();
        $this->setEndpoint();
    }

    private function setUid()
    {
        $this->uid = "nX3FHB4soofAGwqBZwlisGaEqvA3";
    }

    private function setEndpoint()
    {
        // $this->endpoint = "https://imagoice.net/";
        $this->endpoint = "https://imagoassess.net/";
        // $this->endpoint = "http://35.236.216.130/";
    }

    public function ch($arg)
    {
        $certificate_location = base_path('cacert.pem');
        $route = $arg['route'];
        $postFields = $arg['postFields'];
        
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->endpoint.$route,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_SSL_VERIFYHOST => $certificate_location,
            CURLOPT_SSL_VERIFYPEER => $certificate_location
        ));

        $response = curl_exec($ch);
    
        return $response;
        
    }

    public function login()
    {
        $route = "api/login";
        $postFields = "uid=".$this->uid;
        
        return $this->ch(['route' => $route, 'postFields' => $postFields]);
    }

    public function listSessions()
    {
        $route = "api/list-sessions";
        $postFields = "uid=".$this->uid;
        
        return $this->ch(['route' => $route, 'postFields' => $postFields]);
    }

    
    public function startSession($uid)
    {
        $route = "api/mp/start-session";
        $postFields = "uid=".$uid."&type=xml-request";
        
        return $this->ch(['route' => $route, 'postFields' => $postFields]);
    }

    public function upload($filePath)
    {
        $uid = json_decode($this->login());
        $handle = json_decode($this->startSession($uid->uid));
        $route = "api/mp/upload-xml-requests";
        // $filePath = storage_path('app/EVC_1623_061820_1745.zip');
        $filename = basename($filePath);
        $curlFile = curl_file_create($filePath, 'application/zip', $filename);

        $postFields = array();
        $postFields['zips'] =  $curlFile;
        $postFields['uid'] =  $uid->uid;
        $postFields['handle'] =  $handle->handle;
        
        $this->ch(['route' => $route, 'postFields' => $postFields]);

        return $handle->handle;
    }

    public function process($handle)
    {
        $route = "api/mp/process";
        $postFields = "uid=".$this->uid."&handle=".$handle;

        return $this->ch(['route' => $route, 'postFields' => $postFields]);
    }

    public function status($handle)
    {
        $route = "api/mp/status";
        $postFields = "uid=".$this->uid."&handle=".$handle;

        $status = $this->ch(['route' => $route, 'postFields' => $postFields]);
        $status = @json_decode($status);

        return @$status->status == 'Done' ? true : false;
    }

    public function websocket()
    {
        $host = '34.117.188.19';  //where is the websocket server
        $port = 443;
        $local = "http://localhost";  //url where this script run
        $data = "xml-request-1625489403";  //data to be send

        $head = "GET / HTTP/1.1"."\r\n".
                "Upgrade: WebSocket"."\r\n".
                "Connection: Upgrade"."\r\n".
                "Origin: $local"."\r\n".
                "Host: $host"."\r\n".
                "Sec-WebSocket-Key: asdasdaas76da7sd6asd6as7d"."\r\n".
                "Content-Length: ".strlen($data)."\r\n"."\r\n";
        //WebSocket handshake
        $sock = fsockopen($host, $port, $errno, $errstr, 2);
        fwrite($sock, $head ) or die('error:'.$errno.':'.$errstr);
        $headers = fread($sock, 2000);
        echo $headers;
        fwrite($sock,$data) or die('error:'.$errno.':'.$errstr);
        $wsdata = fread($sock, 2000);
        var_dump($wsdata);
        fclose($sock);




    
    }

    public function listoutputs($handle)
    {
        $route = "api/mp/listoutputs";

        $postFields = "uid=".$this->uid."&handle=".$handle;

        $outputs = $this->ch(['route' => $route, 'postFields' => $postFields]);
        
        $outputs = (array) json_decode($outputs);
        
        foreach($outputs['outputs'] as $output){
            if(strpos($output, '.zip') > 0){
                return $output;
            }
        }

        return false;
    }

    public function download($output)
    {
        $postFields = "uid=".$this->uid;
        $certificate_location = base_path('cacert.pem');
        $url = $this->endpoint.$output;
        $zipFileName = substr($output, strrpos($output, '/') + 1);
        $zipFile = storage_path("app/public/images/out/".$zipFileName);
        
        $zip_resource = fopen($zipFile, "w");

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'uid=nX3FHB4soofAGwqBZwlisGaEqvA3&handle=xml-request-1625489403',
            CURLOPT_SSL_VERIFYHOST => $certificate_location,
            CURLOPT_SSL_VERIFYPEER => $certificate_location,
            CURLOPT_FILE => $zip_resource
        ));

        $page = curl_exec($ch);
        
        if(!$page) {
            return false;
        }
        curl_close($ch);

        $zip = new \ZipArchive;
        $extractPath = storage_path('app/public/images/out');
        if($zip->open($zipFile) != "true"){
            return false;
        } 

        $zip->extractTo($extractPath);
        $zip->close();

        return true;
    }

    public static function checkCaseStatus()
    {
        $self = new self;
        $processed = new ProcessedImagesController;
        $cases = CaseModel::where('processing', 1)
                          ->where('case_type_id', '!=', 2)
                          ->whereNotNull('handle')
                          ->get();
        foreach($cases as $case){
            $status = $self->status($case->handle);
            if ($status) {
                $processed->getResponse($case->handle);
            }
        }

        $woodyCases = scandir(storage_path('app/case'));
        
        foreach ($woodyCases as $case) {
            
            $handle = substr($case, strrpos($case, '_') + 1);
            $status = $self->status($handle);
            if ($status) {
                $processed->getResponse($handle);
            } 
        }
    }
}
