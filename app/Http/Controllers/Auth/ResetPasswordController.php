<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Mail\ForgotPasswordNotifications;
use Carbon\Carbon;
use Mail;
use DB;
use Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendsPasswordResetEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            $email = $user->email;
            $token = Str::random(40);
            $resetPassword = DB::table('password_resets')->updateOrInsert(['email' => $email], ['email' => $email, 'token' => $token, 'created_at' => Carbon::now()]);

            Mail::to($email)->send(new ForgotPasswordNotifications($token, $email));
            return response()->json(["message" => "Verification link sent to email.", "time" => Carbon::now()->toDateTimeString()], 200);
        }else{
            return response()->json(["message" => "User not found.", "time" => Carbon::now()->toDateTimeString()], 404);
        }
    }

    public function passwordReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $checkToken = DB::table('password_resets')->where('email', $request->email)->where('token', $request->token)->where('created_at', '>=', Carbon::now()->subMinutes(60))->first();
        
        if ($checkToken) {
            User::where('email', $request->email)->update(['password' => Hash::make($request->password)]);
            
            DB::table('password_resets')->where('email', $request->email)->where('token', $request->token)->where('created_at', '>=', Carbon::now()->subMinutes(60))->delete();
            
            return response()->json(["message" => "Password changed.", "time" => Carbon::now()->toDateTimeString()], 200);
        
        }else{
            return response()->json(["message" => "Error, token missmatch.", "time" => Carbon::now()->toDateTimeString()], 400);
        }
    }
} 
