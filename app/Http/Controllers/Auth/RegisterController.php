<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterReguest;
use App\Http\Requests\InviteUserRegisterRequest;
use Carbon\Carbon;
use App\Events\SendEmailVerification;
use App\Http\Traits\Audit;
use Str;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, Audit;

    protected $verify_token;
    protected $verify_code;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->verify_token = Str::random(50);
        $this->verify_code = rand(100000, 999999);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterReguest $request)
    {
        if ($request->filled('trial')) {

            $user = $this->createTrial($request->all());
            $user->audits()->create($this->assignAuditValues('register', $user, null, $request));
            event(new SendEmailVerification($user));
                
            return response()->json(['message' => 'Successfully create a trial account .','timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
        
        }else{

            $user = $this->create($request->all());
            $user->audits()->create($this->assignAuditValues('register', $user, null, $request));
            event(new SendEmailVerification($user));
            
            return response()->json(['message' => 'Successfully create a account.','timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
        }
    }

    public function registerInvitedUser(InviteUserRegisterRequest $request)
    {
        $user = User::where('verify_token', $request->token)->first();
        if (!$user) {
            return response()->json(['message' => 'Error.', 'timestamp' => Carbon::now()->toDateTimeString()], 406);
        }
        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'password' => bcrypt($request->password),
            'role' => 'user',
            'active' => 1,
            'verify_token' => $this->verify_token,
            'verify_code' => $this->verify_code
        ]);

        $user->audits()->create($this->assignAuditValues('register', $user, null, $request));
        event(new SendEmailVerification($user));
            
        return response()->json(['message' => 'Successfully create a account.','timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => 'in_process',
                'active' => 0,
                'verify_token' => $this->verify_token,
                'verify_code' => $this->verify_code
            ]);
    }
    
    /**
     * Create a new trial user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function createTrial(array $data)
    {
        return User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => 'trial_user',
                'active' => 1,
                'trial_start_date' => Carbon::now(),
                'verify_token' => $this->verify_token,
                'verify_code' => $this->verify_code
            ]);
    }

    /**
     * Check if a email exists in database.
     *
     * @param  string  \Illuminate\Http\Request  $request
     */
    public function checkIfEmailExists(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();

        return $user ? response()->json(['message' => 'Email already exists','timestamp' => Carbon::now()->toDateTimeString(), 'trial_user' => $user->isTrial], 406)
                     : response()->json(['message' => 'Ok','timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    /**
     * Check if a email exists and user role is not trial_user and in_process.
     *
     * @param  string  \Illuminate\Http\Request  $request
     */
    public function checkIfEmailExistsAndUserIsTrial(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();

        if ($user && in_array($user->role, ['trial_user', 'in_process'])) {
            $user = Auth::login($user);
            return response()->json(['message' => 'Ok',
                                     'user'  => $user,
                                     'redirectUrl' => url("/account-type"),
                                     'timestamp' => Carbon::now()->toDateTimeString(),
                                    ], 200);
        }elseif(!$user){
            return response()->json(['message' => 'Ok', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }else{
            return response()->json(['message' => 'Email already exists','timestamp' => Carbon::now()->toDateTimeString()], 406);
        }
    }
}
