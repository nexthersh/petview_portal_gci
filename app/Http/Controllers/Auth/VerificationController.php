<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class VerificationController extends Controller
{
    public function verifyEmail(Request $request)
    {
        $user = User::where('verify_token', $request->token)
            ->where('verify_code', $request->code)
            ->first();

        if ($user) {
            $user->update(['email_verified_at' => Carbon::now()]);

            return response()->json(['message' => 'Email is verified.','timestamp' => Carbon::now()->toDateTimeString()], 200);
        }

        return response()->json(['message' => 'Wrong code.','timestamp' => Carbon::now()->toDateTimeString()], 404);
        
    }
}
