<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use App\Http\Controllers\PatientController;
use App\Http\Traits\Audit;
use Tymon\JWTAuth\JWTAuth;
use App\Client;
use App\Patient;
use Carbon\Carbon;
use DB;

class ClientController extends Controller
{
    use Audit;

    private $user;
	private $role;
 
    public function __construct(Request $request, JWTAuth $jwt){

        $token = $jwt->getToken();
        $this->user = $jwt->toUser($token);
        $this->role = $this->user->role; 
    }
    
    public function getClients() //dropdown
    {
        $clients = Client::with('patients.patientType', 'patients.breed', 'patients.alteredStatus');
    	
        if ($this->role == 'user' || $this->role == 'trial_user') {
            $clients = $this->getClientsByUser($clients);
        }else if ($this->role == 'clinic_admin') {
            $clients = $this->getClientsByClinicAdmin($clients);
        }

        $clients = $clients->get()->toArray();
        return response()->json($clients, 200);
        
    }

    public function allClients(Request $request)
    {
        $clients = Client::with('patients.patientType');  
        
        if ($this->role == 'user' || $this->role == 'trial_user') {
            $clients = $this->getClientsByUser($clients);
        
        }else if ($this->role == 'clinic_admin') {
            $clients = $this->getClientsByClinicAdmin($clients);
        }

        if ($request->filled('search')) {
            $clients->where(function($q) use ($request) {
               $q->where('first_name', 'LIKE', '%'.$request->search.'%');
               $q->orWhere('last_name', 'LIKE', '%'.$request->search.'%');
               $q->orWhere('middle_name', 'LIKE', '%'.$request->search.'%');
               $q->orWhere('id', 'LIKE', '%'.$request->search.'%');
               $q->orWhereRaw("CONCAT_WS(' ', `first_name`, `middle_name`, `last_name`) LIKE '%{$request->search}%'");
               $q->orWhereRaw("CONCAT_WS(' ', `last_name`, `middle_name`, `first_name`) LIKE '%{$request->search}%'");
            });
            
        }

        if ($request->filled('clientId')) {
            $clients->where('id', $request->clientId);
        }
        
        if ($request->filled('name')) {
            $clients->where(function($q) use ($request){
                $q->where('first_name', 'LIKE', '%'.$request->name.'%');
                $q->orWhere('last_name', 'LIKE', '%'.$request->name.'%');
                $q->orWhere('middle_name', 'LIKE', '%'.$request->name.'%');
                $q->orWhereRaw("CONCAT_WS(' ', `first_name`, `middle_name`, `last_name`) LIKE '%{$request->name}%'");
                $q->orWhereRaw("CONCAT_WS(' ', `last_name`, `middle_name`, `first_name`) LIKE '%{$request->name}%'");
            });
            
        }

        if ($request->filled('patient')) {
              $clients->whereHas('patients', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->patient .'%');
                });
        }

        if ($request->filled('patientId')) {
              $clients->whereHas('patients', function($q) use ($request){
                    $q->where('id', $request->patientId);
                });
        }

        if ($request->filled('created')) {
              $clients->whereDate('created_at', parseGMTDate($request->created));
        }  
        
        if ($request->filled('updated')) {
              $clients->whereDate('updated_at', parseGMTDate($request->created));
        }

        if ($request->filled('dateFrom') && $request->dateFrom) {
              $clients->whereDate('created_at','>=', parseGMTDate($request->dateFrom));
        }

        if ($request->filled('dateTo') && $request->dateTo) {
              $clients->whereDate('created_at','<=', parseGMTDate($request->dateTo));
        }


        if ($request->has('numberPatient') && $request->numberPatient != null && $request->numberPatient > 0) {
            $clients->whereIn('id', Patient::select('client_id')->groupBy('client_id')->havingRaw('COUNT(id) = ?', [$request->numberPatient])->get()->pluck('client_id'));
        }elseif($request->has('numberPatient') && $request->numberPatient != null && $request->numberPatient == 0){
            $clients->doesntHave('patients');
        } 
                               
        $clients = $clients->paginate(10); 
        return response()->json($clients, 200);
    }

    public function store(ClientRequest $request, PatientController $patient)
    {
        if ($this->clientExists($request)) {
            return response()->json(['client_first_name' => ['Client first, middle and last name has already been taken.']], 422);
        }
        if ($request->has('client') && $request->client && ($request->has('patients') && !empty($request->patients))) {
           
            $request->clientId = $request->client['id'];
            $patients = $patient->update($request);

            $response['clientId'] = $request->client;
            $response['patients'] = $patients;
            return response()->json($response, 200);
        
        }elseif (!$request->client) {

            $data = $this->assignValue($request);
            $client = Client::create($data);
            $client->audits()->create($this->assignAuditValues('create', null, $client, $request));
            $request->clientId = $client->id;
            
            $patients = $patient->update($request);
            
            $response['clientId'] = $client;
            $response['patients'] = $patients;
            return response()->json($response, 200);
        }
    }

    public function show($id)
    {
        $client = Client::with('patients.patientType', 'patients.breed', 'patients.alteredStatus', 'user')->withCount('patients')->find($id);

        if ($client) {
            $this->authorize('show', $client);
            return response()->json($client, 200);
            
        }else{
            return response()->json(['message' => 'Client not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 404);
        }
    }

    public function update(ClientRequest $request, PatientController $patient, $id)
    {
        $client = Client::with('user')->find($id);
        if (!$client) {
            return response()->json(['message' => 'Client not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 404);
        }

        $this->authorize('show', $client);       
        
        if ($this->clientExists($request, $id)) {
            return response()->json(['client_first_name' => ['Client first, middle and last name has already been taken.']], 422);
        }
        
        try {
               $oldValues = $client->toJson();
               $data = $this->assignValue($request);      
               $client->update($data);
               $client->audits()->create($this->assignAuditValues('update', $oldValues, $client, $request));
               $request->clientId = $id;

               $patients = $patient->update($request);
               
               $response['client'] = $client;
               $response['patients'] = $patients;
               return response()->json($response, 200);
           
           } catch (\Exception $e) {
               return response()->json(['message' => 'Client not updated.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
           }   
    }

    public function delete(Request $request, $id)
    {
        $client = Client::find($id);
        if ($client) {
            $this->authorize('show', $client);
            $client->delete();
            $client->audits()->create($this->assignAuditValues('delete', $client, null, $request));
            return response()->json(['message' => 'Successfully deleted client.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }else{  
            return response()->json(['message' => 'Client not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 404);
        }
    }

    public function deleteAll(Request $request)
    { 
        $ids = explode(',', $request->ids);
        $clients = Client::with('user');

        if ($this->role == 'user' || $this->role == 'trial_user'
        ) {
            $clients = $this->getClientsByUser($clients);
        
        }else if ($this->role == 'clinic_admin') {
            $clients = $this->getClientsByClinicAdmin($clients);
        }
         
        $clients = $clients->whereIn('clients.id', $ids)->get();
        foreach ($clients as $client) {
            $client->delete();
            $client->audits()->create($this->assignAuditValues('delete', $client, null, $request));
        }  
        return response()->json(['message' => 'Successfully deleted clients.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function assignValue($request)
    {
        $data['first_name'] = $request->client_first_name;
        $data['middle_name'] = $request->client_middle_name;
        $data['last_name'] = $request->client_last_name;
        $data['address'] = $request->address;
        $data['phone_number'] = $request->phone_number;
        $data['email'] = $request->email;
        $data['number_pets'] = $request->number_pets;
        $data['user_id'] = $this->user->id;
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        return $data;
    }

    public function clientExists($request, $id = null)
    {
        $client = Client::where('first_name', $request->client_first_name)
                        ->where('middle_name', $request->client_middle_name)
                        ->where('last_name', $request->client_last_name);
        if ($id) {
            $client->where('id', '!=', $id);
        }
        $client = $client->first();

        return $client;
    }

    private function getClientsByClinicAdmin($clients)
    {
        return $clients->whereHas('user', function($query){
            $query->where('clinic_id', $this->user->clinic_id);
        });
    }

    private function getClientsByUser($clients)
    {
        return $clients->where('user_id', $this->user->id);
    }

}
