<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BodyPartAlgorithm;
use App\BodyPart;

class BodyPartController extends Controller
{
    public function getAllBodyPart()
    {
    	$body_part = BodyPart::withCount(['algorithm' => function($q){ return $q->where('for_no_processing_case',0); } ])
                             ->with([ 'algorithm' => function($q){ return $q->where('for_no_processing_case',0)->select('algorithm.id', 'algorithm.name', 'algorithm.suffix'); } ])
                             ->with('algorithm.imageModality')
                             ->with(['algorithm' => function($q) { return $q->where('for_no_processing_case',0)->withCount('imageModality'); }])
                             // ->whereHas('algorithm', function($q) { return $q->where('bodypart_algorithm.for_no_processing_case', 0); })
                             ->orderBy('name')
                             ->get()->toArray();

    	return response()->json($body_part, 200);
    }

    public function getByAlgorithmId($id)
    {
    	$body_part = BodyPartAlgorithm::with('bodypart')
    									->where('algorithm_id', $id)
    									->get();
    	return response()->json($body_part, 200);
    }

}
