<?php

namespace App\Http\Controllers;

use App\Http\Requests\CaseRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\XmlController;
use App\Http\Controllers\ZipController;
use App\Http\Controllers\ImagoRestApiController;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\Payments\PaymentController;
use App\Http\Traits\Audit;
use DB;
use Auth;
use Validator;
use App\CaseModel;
use Carbon\Carbon;
use App\Client;
use App\Patient;
use App\Images;
use App\ImagesNoProcessing;
use App\User;
use File;
use Log;

class CaseController extends Controller
{
    use Audit;

    private $user;
    private $role;
    private static $client;
    private $case_type_id = 1;
    

    public function __construct(JWTAuth $jwt){

        $token = $jwt->getToken();
        $this->user = $jwt->toUser($token);
        $this->role = $this->user->role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 

        Images::$preventAttrSet = false;
        
        $query = CaseModel::with('user', 'client', 'patient.patientType', 'bodyPart', 'alteredStatus', 'images.aoiImage', 'images.bodyPart', 'images.alteredStatus',  'images.imageModality', 'images.breed', 'images.patientType', 'images.bodyView','imageModality', 'images.algorithm', 'images.imageType','algorithm', 'caseType');
        
        if ($this->role == 'user' || $this->role == 'woodybreast_user' || $this->role == 'trial_user') {
            $query = $this->getCasesByUser($query);
        
        }elseif ($this->role == 'clinic_admin') {
            $query = $this->getCasesByClinicAdmin($query);
        }

        if($request->has('usecase') && $request->usecase){
          $query->where(function($q) use ($request){
            $q->whereHas('algorithm', function($query) use ($request){
                    $query->where('id', $request->usecase);
                });
            $q->orWhereHas('images', function($query) use ($request){
                      $query->where('algorithm_id', $request->usecase);
                  });
          });
        }
        if($request->has('species') && $request->species){
          $query->where(function($q) use ($request){
            $q->whereHas('patient.patientType', function($query) use ($request){
                    $query->where('id', $request->species);
                });
            $q->orWhereHas('images', function($query) use ($request){
                    $query->where('patient_type_id', $request->species);
                });
          });
        }
        if($request->has('bodyPart') && $request->bodyPart){
          $query->where(function($q) use ($request){
            $q->whereHas('bodyPart', function($query) use ($request){
                    $query->where('id', $request->bodyPart);
                });
            $q->orWhereHas('images', function($query) use ($request){
                    $query->where('body_part_id', $request->bodyPart);
                });
          });
        }
        if($request->has('sex') && $request->sex){
          $query->where(function($q) use ($request){
            $q->whereHas('patient', function($query) use ($request){
                    $query->where('gender', $request->sex);
                });
            $q->orWhereHas('images', function($query) use ($request){
                    $query->where('gender', $request->sex);
                });
          });
        }
        if($request->has('alteredStatus') && $request->alteredStatus){
          $query->where(function($q) use ($request){
            $q->whereHas('alteredStatus', function($query) use ($request){
                    $query->where('id', $request->alteredStatus);
                });
            $q->orWhereHas('images', function($query) use ($request){
                    $query->where('altered_status_id', $request->alteredStatus);
                });
          });
        }
        if($request->has('breed') && $request->breed){
          $query->where(function($q) use ($request){
              $q->whereHas('patient.breed', function($query) use ($request){
                    $query->where('id', $request->breed);
                });
              $q->orWhereHas('images', function($query) use ($request){
                    $query->where('breed_id', $request->breed);
                });
          });
        }
        if($request->has('age') && $request->age){
          $query->where(function($q) use ($request){
            $q->whereHas('patient', function($query) use ($request){
                    $query->where('age', $request->age);
                });
            $q->orWhereHas('images', function($query) use ($request){
                    $query->where('age', $request->age);
                });
          });
        }
        if($request->has('imageModality') && $request->imageModality){
          $query->where(function($q) use ($request){
            $q->whereHas('imageModality', function($query) use ($request){
                    $query->where('id', $request->imageModality);
                });
            $q->orWhereHas('images', function($query) use ($request){
                    $query->where('image_modality_id', $request->imageModality);
                });
          });
        }
        if($request->has('client') && $request->client){
          $query->whereHas('client', function($q) use ($request){
                    $q->where('id', $request->client);
                });
        }
        if($request->has('patient') && $request->patient){
          $query->whereHas('patient', function($q) use ($request){
                    $q->where('id', $request->patient);
                });
        }

        if($request->has('dateFrom') && $request->dateFrom){
          $query->whereDate('created_at', '>=', parseGMTDate($request->dateFrom));
        }

        if($request->has('dateTo') && $request->dateTo){
          $query->whereDate('created_at', '<=', parseGMTDate($request->dateTo));
        }

        if($request->has('caseType') && $request->caseType){
          $query->where('cases.case_type_id', '=', $request->caseType);
        }

        if($request->has('diagnosis') AND $request->diagnosis == 1 AND $request->diagnosis != null){
          $query->whereHas('images', function($q) use ($request){
                    $q->whereNotNull('diagnosis');
                    $q->where('diagnosis', '!=', '');
                });
        }

        if($request->has('diagnosis') AND $request->diagnosis == 0 AND $request->diagnosis != null){
          $query->whereHas('images', function($q) use ($request){
                    $q->whereNull('diagnosis');
                    $q->orWhere('diagnosis', '=', '');
                });
        }

        if($request->has('aoi_captured') AND $request->aoi_captured == 1 AND $request->aoi_captured != null){
          $query->whereHas('images', function($q) use ($request){
                    $q->whereNotNull('aoi_coords');
                });
        }

        if($request->has('imageType') AND $request->imageType){
          $query->whereHas('images', function($q) use ($request){
                    $q->where('image_type_id', $request->imageType);
                });
        }

        if($request->has('aoi_captured') AND $request->aoi_captured == 0 AND $request->aoi_captured != null){
          $query->whereHas('images', function($q) use ($request){
                    $q->whereNull('aoi_coords');
                });
        }

        if($request->has('other_image_type') && $request->other_image_type){
          $query->whereHas('images', function($q) use ($request){
                    $q->where('other_image_type', 'like', '%'. $request->other_image_type. '%');
                });
        }

        if($request->has('aoi_type') && $request->aoi_type){
          $query->whereHas('images.aoiImage', function($q) use ($request){
                    $q->where('aoi_type', 'like', '%'. $request->aoi_type. '%');
                });
        }

        if($request->has('caseStatus') && $request->caseStatus){
          $query->where(function($q) use ($request){
              $q->whereHas('images.aoiImage', function($query) use ($request){
                  $query->where('ml->eval->eval', 'like', '%'. $request->caseStatus. '%');
              });
              $q->orWhereHas('images', function($query) use ($request){
                  $query->where('ml->eval->eval', 'like', '%'. $request->caseStatus. '%');
              });
          });
        }

        if($request->has('search') && $request->search){
          $query->where(function($q) use ($request){
              $q->orWhere('cases.id', 'LIKE', '%'.$request->search.'%')
                ->orWhereHas('client', function($query) use ($request){
                    $query->where('first_name', 'like', '%'. $request->search .'%');
                    $query->orWhere('last_name', 'like', '%'. $request->search .'%');
                    $query->orWhereRaw("CONCAT_WS(' ', `first_name`, `middle_name`, `last_name`) LIKE '%{$request->search}%'");
                    $query->orWhereRaw("CONCAT_WS(' ', `last_name`, `middle_name`, `first_name`) LIKE '%{$request->search}%'");
                })
                ->orWhereHas('patient', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('algorithm', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images.algorithm', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images.breed', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('patient.breed', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('imageModality', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images.imageModality', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images.imageType', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('caseType', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images', function($q) use ($request){
                    $q->where('transaction_id', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('patient.patientType', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images.patientType', function($q) use ($request){
                    $q->where('name', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images.aoiImage', function($q) use ($request){
                    $q->where('aoi_type', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images', function($q) use ($request){
                    $q->where('ml_eval', 'like', '%'. $request->search .'%');
                })
                ->orWhereHas('images', function($q) use ($request){
                    $q->where('other_image_type', 'like', '%'. $request->search .'%');
                });      
          });
        }
        
        $query->orderBy('cases.created_at', 'desc');
        // dd($query->toSql());
        $cases = $query->distinct()->paginate(10);
      
        return response()->json($cases, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $json_data = json_decode($request->json);
        $imagesData = $json_data->images;  
        $data = [
                'case_type_id' => $this->case_type_id,
                'client_id' => $json_data->client->id,
                'patient_id' => $json_data->patient->id,
                'user_id' => $this->user->id,
                // 'altered_status_id' => $json_data->altered_status,
                'body_part_id' => $json_data->body_part[0]->id,
                'algorithm_id' => $json_data->algorithm[0]->id,
                'image_modality_id' => $json_data->image_modality[0]->id,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
        
        $validator_data = $this->rules($data);
        //$validator_images = $this->imageRules($json_data);
        if ($validator_data->fails()) {
             return response()->json($validator_data->errors()->first(), 200);
        }
        $case = CaseModel::create($data);
        $case->audits()->create($this->assignAuditValues('create', null, $case, $request));

        $transactionNumber = $this->transactionNumber($case->id);
        // $file = $this->uploadFile($request->images, $case, $json_data);   
        ImagesController::create($imagesData, $case->id, $transactionNumber, @$request->aoi_images, $this->case_type_id);
        // create xml
        XmlController::xml($json_data, $case->id, $transactionNumber);
        // create zip
        ZipController::zip($json_data, $request->images, $request->aoi_images, $case->id, $transactionNumber);
        // upload zip to AWS
        $destination = $transactionNumber.'.zip';
        $sourceFile = storage_path('app/public/zip/'.$transactionNumber.'.zip');

        try {
          $iceClient = new ImagoRestApiController;
          $handle = $iceClient->upload($sourceFile);
          $case->update(['handle' => $handle]);
          $iceClient->process($handle);
      } catch (\Throwable $th) {
         $case->delete();
         return response()->json(['message' => 'Transaction failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422) ;
      }

      if ($this->user->availableCases == 0) {
          $payment = new PaymentController();
          $paymentResponse = $payment->paymentPercase();
          if (!$paymentResponse) {
              $case->delete();
              return response()->json(['message' => 'Payment Transaction failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422);
          }
      }else{
          $this->user->clinic()->update(['available_cases' => $this->user->availableCases - 1]);
      }


 		// delete local zip and xml file
        File::delete(storage_path('app/xml/'.$transactionNumber.'.xml'));
        File::delete(storage_path('app/public/zip/'.$transactionNumber.'.zip'));

        return response()->json(['message' => 'Successfully created the case!', 'timestamp' => Carbon::now()->toDateTimeString()], 200);   
    }

    public function transactionNumber($caseId)
    {
      $transactionNumber = 'EVC_'.$caseId.'_'.Carbon::now()->format('mdy').'_'.Carbon::now()->format('Hi');

      return $transactionNumber;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // if (Cache::has('case_'.$id)) {
        //   $case = Cache::get('case_'.$id);
        //   $this->authorize('show', CaseModel::find($id));
        //   return response()->json($case, 200);
        // }
        CaseModel::$preventAttrSet = false;
        $case = CaseModel::with('user', 'client', 'patient.patientType', 'patient.breed', 'patient.alteredStatus', 'bodyPart', 'alteredStatus', 'images.aoiImage', 'images.bodyPart', 'images.alteredStatus',  'images.imageModality', 'images.breed', 'images.patientType', 'images.bodyView','imageModality', 'images.algorithm','images.imageType', 'algorithm', 'caseType')->findOrFail($id);

         $this->authorize('show', $case);
         Cache::forever('case_'.$id, $case->toArray());

         return response()->json($case, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $case = CaseModel::findOrFail($id);
        $this->authorize('update', $case);
        
        $json_data = json_decode($request->json);  
        $data = [
                'case_type_id' => $this->case_type_id,
                'client_id' => $json_data->client->id,
                'patient_id' => $json_data->patient->id,
                'user_id' => $this->user->id,
                // 'altered_status_id' => $json_data->altered_status,
                'body_part_id' => $json_data->body_part[0]->id,
                'algorithm_id' => $json_data->algorithm[0]->id,
                'image_modality_id' => $json_data->image_modality[0]->id,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];

        $validator_data = $this->rules($data);
        
        if ($validator_data->fails()) {
             return response()->json($validator_data->errors()->first().''.$validator_data->errors()->first(), 200);
        }
        $oldValues = $case->toJson();
        $case->update($data);
        $case->audits()->create($this->assignAuditValues('update', $oldValues, $case, $request));

        Cache::forget('case_'.$id);
        return response()->json(['message' => 'Successfully update case.', 'timestamp' => Carbon::now()->toDateTimeString(), 'case' => $case], 200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        CaseModel::$preventAttrSet = false;
        $case = CaseModel::find($id);

        if ($case) {
          $this->authorize('show', $case);
            $case->delete();
            $case->audits()->create($this->assignAuditValues('delete', $case, null, $request));
            Cache::forget('case_'.$id);
            return response()->json(['message' => 'Successfully deleted case.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }else{  
            return response()->json(['message' => 'Case not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function deleteAll(Request $request)
    { 
      CaseModel::$preventAttrSet = false;
      $ids = explode(',', $request->ids);
      $cases = CaseModel::whereIn('id', $ids);
      
      if ($this->role == 'user' || $this->role == 'woodybreast_user') {
          $cases = $this->getCasesByUser($cases);
        
      }elseif ($this->role == 'clinic_admin') {
          $cases = $this->getCasesByClinicAdmin($cases);
      }
      $cases = $cases->get();
      foreach ($cases as $case) {
        $case->delete();
        $case->audits()->create($this->assignAuditValues('delete', $case, null, $request));
      }
      return response()->json(['message' => 'Successfully deleted cases.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function rules($data){

            $validator = Validator::make($data, [
                'client_id' => 'required|Integer',
                'patient_id' => 'required|Integer',
                'user_id' => 'required|Integer',
                'notes' => 'string|nullable',
                'diagnosis' => 'string|nullable',
                'reffering_doctor' => 'string|nullable'
            ]);
          
          return $validator; 
    }

    public function activate($id)
    {
      $case = CaseModel::where('id', $id)->update(['status' => 0]);

      return response()->json(['message' => 'Successfully activated the case.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function deactivate($id)
    {
      $case = CaseModel::where('id', $id)->update(['status' => 1]);

      return response()->json(['message' => 'Successfully deactivated the case.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    private function getCasesByClinicAdmin($query)
    {
        return $query->whereHas('user', function($q){
            $q->where('clinic_id', $this->user->clinic_id);
        });
    }

    private function getCasesByUser($query)
    {
        return $query->where('user_id', $this->user->id);
    }
    
    public function getFilter()
    {
      $clients = Client::query();
      $patients = Patient::query();
      
      if ($this->role == 'user') {
          $clients->where('user_id', $this->user->id);
          $patients->whereHas('client.user', function($query){
              $query->where('id', $this->user->id);
            });
      
      }else if ($this->role == 'clinic_admin') {
          $clients->whereHas('user', function($query){
              $query->where('clinic_id', $this->user->clinic_id);
            });
          $patients->whereHas('client.user', function($query){
              $query->where('clinic_id', $this->user->clinic_id);
            });
      }

      $filters['usecase'] = DB::table('algorithm')->get()->toArray();
      $filters['species'] = DB::table('patient_type')->get()->toArray();
      $filters['body_part'] = DB::table('body_part')->orderBy('name')->get()->toArray();
      $filters['altered_status'] = DB::table('altered_status')->get()->toArray();
      $filters['breed'] = DB::table('breeds')->get()->toArray();
      $filters['image_modality'] = DB::table('modality_type')->get()->toArray();
      $filters['image_type'] = DB::table('image_type')->get()->toArray();
      $filters['case_type'] = DB::table('case_type')->get()->toArray();
      $filters['clients'] = $clients->get()->toArray();
      $filters['patients'] = $patients->get()->toArray();
      
      return response()->json($filters, 200);
    }
}
