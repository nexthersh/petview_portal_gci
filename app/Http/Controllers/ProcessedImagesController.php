<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessedImages;
use App\ProcessedImage;
use App\Images;
use App\AoiImage;
use App\CaseModel;
use App\Http\Controllers\XmlController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\ImagoRestApiController;
use Carbon\Carbon;

class ProcessedImagesController extends Controller
{

    public function getResponse($handle)
    {
        $case = CaseModel::with('images')->where('handle', $handle)->first();
        $iceClient = new ImagoRestApiController;
        $output = $iceClient->listoutputs($handle);
        $result = $iceClient->download($output);
        
        if ($case || $result) {
             ProcessedImages::dispatch($case, $handle);
        }
    }

    public static function create($key, $size, $caseId)
    {
    	ProcessedImage::updateOrCreate(
                                        ['key' => $key, 'case_id' => $caseId],
                                		['key' => $key,
                                		'size' => @$size,
                                		'case_id' => $caseId,
                                		'created_at' => Carbon::now()->toDateTimeString(),
                                        'updated_at' => Carbon::now()->toDateTimeString()
                                	   ]
                                    );

        // get value from xml and set to images
        if (strpos($key, '.xml')) {
            
            $data = file_get_contents(storage_path($key));
            
            $results = XmlController::getResult($data);
            $versions = XmlController::getAlgMdlVersion($data);

            self::setResult($results);
            self::setAlgMdlVersion($versions);
        }
    }

    private function getCaseIdFromResponse($response)
    {
        $object_key = $response->Records[0]->s3->object->key;
        if (strpos($object_key, '.zip')) {
            return false;
        }
        $transactionId =str_replace(['OUT//', 'DEVELOP/'], '', $object_key);
        $transactionId = substr($transactionId, 0, strpos($transactionId, '/'));
        
        if (Images::where('transaction_id', $transactionId)->get()->count() > 0 || file_exists(storage_path('app/case/'.$transactionId))){
            
            $transactionId = ltrim($transactionId, '$/EVC_');
            $case_id = substr($transactionId, 0, strpos($transactionId,'_' ));
            return $case_id;
        }
        
        return false;
    }

    private static function setResult($results)
    {
        foreach ($results as $key => $result) {
            
            if (is_array($result) && isset($result[0]['name'])) {
                foreach ($result as $tag => $value) {
                    $update = ['ml' => json_encode(@$value['ML']),
                       'stdev' => @$value['STDEV'],
                       'mean' => @$value['MEAN'],
                       'nbrightestpixpc' => @$value['NBrightestPixPC'],
                    ];
                    
                    if ($image = AoiImage::where('image_name', $value['name'])->first()) {
                        $image->update($update);
                    }elseif ($image = Images::where('name', $value['name'])->first()) {
                        $image->update($update);
                    }
                    
                }
            }else{
                $update = ['ml' => json_encode(@$result['ML']),
                       'stdev' => @$result['STDEV'],
                       'mean' => @$result['MEAN'],
                       'nbrightestpixpc' => @$result['NBrightestPixPC'],
                    ];
                    
                if ($image = AoiImage::where('image_name', $result['name'])->first()) {
                    $image->update($update);
                }elseif ($image = Images::where('name', $result['name'])->first()) {
                    $image->update($update);
                }
            } 
        }
    }

    private static function setAlgMdlVersion($versions)
    {
         // dd($versions);
        foreach ($versions as $key => $value) {
           Images::where('name', str_replace(" ", "", $value['name']))->update(['algversion' => $value['ALGVERSION'], 'mdlversion' => $value['MDLVERSION']]); 
        } 
    }
}
