<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClinicRequest;
use Carbon\Carbon;
use App\Events\NewRegistrationRequest;
use App\Events\RejectedApprovedRegistrationRequest;
use App\Events\SetPasswordProcessed;
use App\Http\Traits\Audit;
use Illuminate\Support\Facades\Auth;
use DB;
use Validator;

class ClinicController extends Controller
{
    use Audit;

    public function __construct()
    {
        $this->middleware('auth')->except(['newClinicRequest', 'getClinics']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('any', Clinic::class);
        $query = Clinic::where('active', 1);
        $query = $this->getByFilters($query, $request);
        $clinics = $query->paginate(10);

        return response()->json($clinics, 200);
    }

    public function getClinics()
    {
        $clinics = Clinic::where('active', 1)->get();
        $clinics = $clinics->map(function ($clinics) {
            return collect($clinics)->only(['id', 'name', 'email']);
        });
        return response()->json($clinics, 200);
    }

    public function clinicsRequestList(Request $request)
    {
        $this->authorize('any', Clinic::class);
        $query = Clinic::where('active', 0);
        $query = $this->getByFilters($query, $request);
        $clinics = $query->paginate(10);

        return response()->json($clinics, 200);
    }

    private function getByFilters($query, $request)
    {
        if ($request->has('search') && $request->search) {
            $query->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%');
                $q->orWhere('email', 'like', '%' . $request->search . '%');
                $q->orWhere('id', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('name') && $request->name) {
            $query->where('name', 'like', '%' . $request->name . '%');
        }

        if ($request->has('email') && $request->email) {
            $query->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->has('address') && $request->address) {
            $query->where('address', 'like', '%' . $request->address . '%');
        }

        if ($request->has('phone') && $request->phone) {
            $query->where('phone', 'like', '%' . $request->phone . '%');
        }

        if ($request->has('created_from') && $request->created_from) {
            $query->whereDate('created_at', '>=', parseGMTDate($request->created_from));
        }

        if ($request->has('created_to') && $request->created_to) {
            $query->whereDate('created_at', '<=', parseGMTDate($request->created_to));
        }

        return $query;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClinicRequest $request)
    {
        $this->authorize('any', Clinic::class);

        $insert = $request->except('_token');
        // dd($insert);
        $insert['active'] = 1;
        $validate = $this->userExistsValidation($insert);
        if ($validate->fails()) {

            return response()->json($validate->errors(), 422);
        }
        $clinic = Clinic::create($insert);
        $clinic->audits()->create($this->assignAuditValues('create', null, $clinic, $request));
        $user = $this->addClinicAdmin($request, $clinic->id, 1);
       
        event(new SetPasswordProcessed($user));
        return response()->json($clinic, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function show(Clinic $clinic)
    {
        $this->authorize('any', Clinic::class);
        return response()->json($clinic, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function update(ClinicRequest $request, Clinic $clinic)
    {
        $this->authorize('any', Clinic::class);
        $insert = $request->except('_token', '_method');
        $oldValues = $clinic->toJson();
        $clinic->update($insert);
        $clinic->audits()->create($this->assignAuditValues('update', $oldValues, $clinic, $request));

        return response()->json($clinic, 200);
    }

    public function activateClinic(Request $request, $id)
    {
        $clinic = Clinic::find($id);
        if ($clinic) {
            $this->authorize('any', $clinic);
            $oldValues = $clinic->toJson();
            $clinic->update(['active' => 1]);
            $clinic->audits()->create($this->assignAuditValues('update', $oldValues, $clinic, $request));
            // active clinic admin
            $clinicAdmin = User::where('email', $clinic->email)->first();
            if ($clinicAdmin) {
                $clinicAdmin->update(['active' => 1]);
            }

            return response()->json(['message' => 'Successfully activated clinic.', 'timestamp' => Carbon::now()->toDateTimeString(), 'clinic' => $clinic], 200);
        } else {
            return response()->json(['message' => 'Clinic not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Clinic $clinic)
    {
        $this->authorize('any', Clinic::class);
        $clinic->delete();
        $clinic->audits()->create($this->assignAuditValues('delete', $clinic, null, $request));

        return response()->json(['message' => 'Successfully deleted clinic.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
    }

    public function newClinicRequest(ClinicRequest $request)
    {
        $insert = $request->except('password', 'password_confirmation', 'termsOfService', 'privacyPolicy');

        $validate = $this->userExistsValidation($insert);
        if ($validate->fails()) {
            return response()->json($validate->errors(), 422);
        }

        $clinic = Clinic::create($insert);
        // add clinic admin
        $user = $this->addClinicAdmin($request, $clinic->id);
        //send mail to administrator
        event(new NewRegistrationRequest($user, $clinic));

        return response()->json(['message' => 'Successfully submit a clinic registration request.', 'timestamp' => Carbon::now()->toDateTimeString(), 'clinic' => $clinic], 200);
    }

    public function rejectedClinicRequest(Request $request, $id)
    {
        $clinic = Clinic::find($id);
        if ($clinic) {
            $this->authorize('any', $clinic);
            $clinicAdmin = User::where('email', $clinic->email)->first();
            $clinic->delete();
            $clinic->audits()->create($this->assignAuditValues('delete', $clinic, null, $request));
            if ($clinicAdmin) {
                event(new RejectedApprovedRegistrationRequest($clinicAdmin));
            }
            return response()->json(['message' => 'Successfully rejected clinic registration request.', 'timestamp' => Carbon::now()->toDateTimeString(), 'clinic' => $clinic], 200);
        } else {
            return response()->json(['message' => 'Clinic not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function userExistsValidation($data)
    {
        $v = Validator::make($data, [
            'email' => 'required|string|email|max:191|unique:users,email',
        ]);
        return $v;
    }

    protected function addClinicAdmin($request, $clinicId, $active = 0)
    {
        $insert['first_name'] = $request->name;
        $insert['email'] = $request->email;
        $insert['phone'] = $request->phone;
        $insert['address'] = $request->address;
        $insert['password'] = bcrypt($request->password);
        $insert['clinic_id'] = $clinicId;
        $insert['role'] = 'clinic_admin';
        $insert['active'] = $active;

        return User::create($insert);
    }

    public function getAvailableCases()
    {
        $user = Auth::user();
        $clinic = Clinic::where('id', $user->clinic_id)->first();
        
        return $clinic->available_cases > 0 ? true : false;
    }
}
