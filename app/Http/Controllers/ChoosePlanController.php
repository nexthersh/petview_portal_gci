<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChoosePlanReguest;
use App\Events\NewRegistrationRequest;
use App\Http\Controllers\Payments\PaymentController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Clinic;
use Carbon\Carbon;

class ChoosePlanController extends Controller
{
    public function choosePlan(ChoosePlanReguest $request)
    {
        if($request->type == 'user'){

            return $this->userAccountPlan($request);
            
        }elseif($request->type == 'clinic_admin'){

            return $this->primaryAccountOwnerPlan(new PaymentController, $request);
        }
    }

    private function userAccountPlan($data)
    {
        $user = Auth::user();
        
        $user->update(['role' => $data->type, 'clinic_id' => $data->clinic_id]);
        event(new NewRegistrationRequest($user));
        Auth::logout();
        return response()->json(['message' => 'Registration request sent to administrator.', 'timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
    }

    private function primaryAccountOwnerPlan($payment, $data)
    {
        $user = Auth::user();
        $plan = $this->getPlan($data->plan);
        $data->plan = $plan;

        // create clinic
        $clinic = Clinic::create([
            'name' => $data->clinic,
            'email' => $user->email,
            'active' => 1,
            'available_cases' => $plan->number_of_cases,
        ]);
        $user->update(['clinic_id' => $clinic->id]);
        // payment transaction
        if (!$payment->primaryOwnerPayment($data)) {
            $user->update(['clinic_id' => null]);
            $clinic->delete();
            return response()->json(['message' => 'Payment Transaction failed.', 'timestamp' => Carbon::now()->toDateTimeString()], 422);
        }
        
        // update user role
        $user->update(['role' => $data->type, 'active' => 1]);
        
        return response()->json(['message' => 'Registration success.', 'timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
    }

    private function getPlan($plan)
    {
        return DB::table('pricing_plans')->where('name', $plan)->first();
    }

}
