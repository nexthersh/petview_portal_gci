<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\BodyPart;
use App\BodyView;
use App\Breed;
use App\AlteredStatus;
use App\Algorithm;

class XmlController extends Controller
{
    public static function xml($json, $caseId, $transactionNumber, $index = null, $filename = null)
    {
        //create and open xml
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?><Case></Case>';
        $xml = new \SimpleXMLElement($xml_header);
        $xml->addChild('TransactionNumber', $transactionNumber);
        $imagesXml = $xml->addChild('Images');
        $self = new self;

        foreach ($json->images as $key => $image) {

            // skip if image is aoi
            if ($index != null and $index != $key) {
                continue;
            }
            $imageName = prepareImageName($image->image_name);
            $imageXml = $imagesXml->addChild('ImageFileName', $transactionNumber . '_' . $caseId . '_' .$imageName);

            if (isset($json->patient_type_id)) {
                $imageXml->addChild('AnimalType', @$json->patient_type_id);
            } elseif (isset($image->patient_type_id->id)) {
                $imageXml->addChild('AnimalType', @$image->patient_type_id->id);
            } elseif (isset($json->patient->patient_type_id)) {
                $imageXml->addChild('AnimalType', @$json->patient->patient_type_id);
            } else {
                $imageXml->addChild('AnimalType', "N/A");
            }

            $imageXml->addChild('ImagingModalityType', @$json->image_modality[0]->id);
            $imageXml->addChild('RegionOfAnatomy', $json->body_part[0]->name);
            
            if (in_array($json->algorithm[0]->suffix, ['SPLENIC_MASS_1', 'LIVER_MASS', 'LYMPH_NODE'])) {

                $visual = $imageXml->addChild('UseCase', $json->algorithm[0]->suffix);
                $visual->addAttribute('result', 'VISUAL');
                $visual->addAttribute('aoitype', 'RAW');
            } else {
                $imageXml->addChild('UseCase', $json->algorithm[0]->suffix);
            }

            if (@$image->aoi_coords) {
                foreach ($image->aoi_coords as $key => $coords) {
                    foreach ($coords->aoi as  $aoi) {
                        $imageCoords = $imageXml->addChild('Coordinates', str_replace("ROI", "AOI", str_replace([' ', '-'], "_", $coords->name)));
                        $imageCoords->addAttribute('aoitype', $self->setAoiType($coords->aoiTypeName));
                        foreach ($aoi->dimensions as  $aoi_dimensions) {
                            foreach ((array)$aoi_dimensions as $dimension_key => $dimensions) {

                                $imageCoords->addChild($dimension_key, $dimensions);
                            }
                        }
                    }
                }
            }

            if (isset($image->other_image_type) and $image->other_image_type) {
                // $imageXml->addChild('OtherImageType', $image->other_image_type);
                $imageXml->addChild('Truth', $image->other_image_type);
            }
            if (isset($image->pathology) and $image->pathology) {
                $imageXml->addChild('Pathology', $image->pathology);
            }
            if (isset($image->outline) and $image->outline) {
                $imageXml->addChild('Outline', $image->outline);
            }
        }
        //save xml
        $xml = $xml->saveXML();
        // storage xml to local disk
        if ($filename) {
            Storage::put('public/images/in/' . $transactionNumber . '/' . $transactionNumber . '_' . $caseId . '_' . $filename . '.xml', $xml);
        } else {
            Storage::put('public/images/in/' . $transactionNumber . '/' . $transactionNumber . '.xml', $xml);
        }
    }

    public static function xmlNoProcessing($json, $imagesData, $caseId, $transactionNumber, $index = null, $filename = null)
    {
        //create and open xml
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?><Case></Case>';
        $xml = new \SimpleXMLElement($xml_header);
        $xml->addChild('TransactionNumber', $transactionNumber);
        $imagesDatasXml = $xml->addChild('Images');

        $imageXml = $imagesDatasXml->addChild('ImageFileName', $transactionNumber . '_' . $imagesData->image_name);

        if (isset($imagesData->patient_type_id->id)) {
            $imageXml->addChild('AnimalType', @$imagesData->patient_type_id->id);
        } elseif (isset($imagesData->patient_type_id)) {
            $imageXml->addChild('AnimalType', @$imagesData->patient_type_id);
        } elseif (isset($imagesData->patient->patient_type_id)) {
            $imageXml->addChild('AnimalType', @$imagesData->patient->patient_type_id);
        } else {
            $imageXml->addChild('AnimalType', "N/A");
        }

        $imageXml->addChild('ImagingModalityType', isset($imagesData->image_modality->id) ? $imagesData->image_modality->id : 'N/A');
        $imageXml->addChild('RegionOfAnatomy', isset($imagesData->body_part->name) ? $imagesData->body_part->name : 'N/A');
        $imageXml->addChild('OtherAlgorithmType', @$imagesData->other_algorithm_type ? $imagesData->other_algorithm_type : 'N/A');

        $useCase = $imageXml->addChild('UseCase', $imagesData->algorithm[0]->suffix);

        $store = $useCase->addChild('Store', '');
        if (is_array($imagesData->aoi_coords)) {
            foreach ($imagesData->aoi_coords as $coords) {
                $aoi = $store->addChild('AOI', str_replace([' ', '-'], '_', $coords->name));
                $aoiType = in_array($coords->aoiTypeId, [1, 2, 3]) ? $aoi->addChild('AOIType', $coords->aoiTypeId) : '';
                $aoiType = $aoi->addChild('Truth', @$imagesData->image_type->name);

                foreach ($coords->aoi as $polygonKey => $polygon) {
                    $coordinates = $imageXml->addChild('Coordinates', str_replace([' ', '-'], '_', $coords->name));
                    foreach ((array)$polygon->dimensions as $dimension) {
                        $dimension = (array)$dimension;
                        $coordinates->addChild(key($dimension), $dimension[key($dimension)]);
                    }
                }
            }
        }
        //save xml
        $xml = $xml->saveXML();
        // storage xml to local disk
        if ($filename) {
            $xmlFile = Storage::put('public/images/develop/' . $transactionNumber . '/' . $filename . '/' . $caseId . '_' . $filename . '.xml', $xml);
        } else {
            $xmlFile = Storage::put('public/images/develop/' . $transactionNumber . '/' . $transactionNumber . '.xml', $xml);
        }
    }

    public static function xmlUpdateNoProcessing($json, $imagesData, $caseId, $transactionNumber, $index = null, $filename = null)
    {
        //create and open xml
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?><Case></Case>';
        $xml = new \SimpleXMLElement($xml_header);
        $xml->addChild('TransactionNumber', $transactionNumber);
        $jsonsXml = $xml->addChild('Images');

        /*foreach ($json->jsons as $key => $json) {

            // skip if json is aoi
            if ($index != null AND $index != $key) {
                continue;
            }*/

        $imageXml = $jsonsXml->addChild('ImageFileName', $transactionNumber . '_' . $json->image_name);

        if (isset($json->patient_type_id)) {
            $imageXml->addChild('AnimalType', @$json->patient_type_id);
        } else {
            $imageXml->addChild('AnimalType', "N/A");
        }

        $imageXml->addChild('ImagingModalityType', isset($json->image_modality) ? $json->image_modality : 'N/A');
        $imageXml->addChild('RegionOfAnatomy', isset($json->body_part) ? $json->body_part : 'N/A');
        $imageXml->addChild('OtherAlgorithmType', $json->other_algorithm_type ? $json->other_algorithm_type : 'N/A');
        $imageXml->addChild('BodyView', isset($json->body_view) ? $json->body_view : 'N/A');
        // $imageXml->addChild('jsonType', isset($json->image_type->name) ? $json->image_type->name : 'N/A');
        // $imageXml->addChild('OtherjsonType', $json->other_image_type ? $json->other_image_type : 'N/A');
        $imageXml->addChild('Breed', isset($json->breed_id) ? $json->breed_id : 'N/A');
        $imageXml->addChild('Age', $json->age ? $json->age : 'N/A');
        $imageXml->addChild('Birthdate', $json->birthdate ? $json->birthdate : 'N/A');
        $imageXml->addChild('Gender', $json->gender ? $json->gender : 'N/A');
        $imageXml->addChild('AlteredStatus', isset($json->altered_status) ? $json->altered_status : 'N/A');
        $imageXml->addChild('Diagnosis', $json->diagnosis ? $json->diagnosis : 'N/A');
        $imageXml->addChild('DifferentialDiagnosis', $json->differential_diagnosis ? $json->differential_diagnosis : 'N/A');
        $imageXml->addChild('Pathology', $json->pathology ? $json->pathology : 'N/A');
        // $imageXml->addChild('Outline', $json->outline ? $json->outline : 'N/A');

        foreach ($json->algorithm as $key => $algorithm) {
            $algorithmValue[] = Algorithm::where('id', $algorithm)->first()->suffix;
        }

        $useCase = $imageXml->addChild('UseCase', implode(',', $algorithmValue));
        unset($algorithmValue);
        $store = $useCase->addChild('Store', '');
        if (is_array($json->aoi_coords)) {
            foreach ($json->aoi_coords as $coords) {
                $aoi = $store->addChild('AOI', str_replace([' ', '-'], '_', $coords->name));
                $aoiType = in_array($coords->aoiTypeId, [1, 2, 3]) ? $aoi->addChild('AOIType', $coords->aoiTypeId) : '';
                $aoiType = $aoi->addChild('Truth', $json->image_type);

                foreach ($coords->aoi as $polygonKey => $polygon) {
                    $coordinates = $imageXml->addChild('Coordinates', str_replace([' ', '-'], '_', $coords->name));
                    foreach ((array)$polygon->dimensions as $dimension) {
                        $dimension = (array)$dimension;
                        $coordinates->addChild(key($dimension), $dimension[key($dimension)]);
                    }
                }
            }
        }

        /* if (isset($json->other_image_type) AND $json->other_image_type) {
                $imageXml->addChild('OtherjsonType', $json->other_image_type);
            }*/
        /*if (isset($json->pathology) AND $json->pathology) {
                $imageXml->addChild('Pathology', $json->pathology);
            }
            if (isset($json->outline) AND $json->outline) {
                $imageXml->addChild('Outline', $json->outline);
            }*/
        // }
        //save xml

        $xml = $xml->saveXML();
        // storage xml to local disk
        if ($filename) {
            $xmlFile = Storage::put('xml/' . $transactionNumber . '_' . $filename . '.xml', $xml);
        } else {
            $xmlFile = Storage::put('xml/' . $transactionNumber . '.xml', $xml);
        }
    }

    public static function getResult($data)
    {
        $tags = ['ML', 'STDEV', 'MEAN', 'NBrightestPixPC'];

        // $uri = $data['@metadata']['effectiveUri'];
        // $xml = file_get_contents($uri);
        // $xml = $data;
        $xml = preg_replace('/\s+/S', " ", $data);
        $transactionNumber = str_replace(['<TransactionNumber>', '</TransactionNumber>'], '$', $xml);
        $transactionNumber = explode('$', $transactionNumber)[1];
        $res = str_replace(['<ImageFileName>', '</ImageFileName>'], '$', $xml);
        $res = explode('$', $res);
        array_shift($res);
        array_pop($res);
        $res = array_filter($res, function ($value) {
            if (!empty(str_replace(['"', ' '], '', $value))) return $value;
        });

        foreach ($res as $key => $value) {

            if (preg_match('</Coordinates>', $value)) {
                $coords = preg_replace("#(<Coordinates)(.*?)(>)#", "<Coordinates>", $value);
                $coords = str_replace(['<Coordinates>', '</Coordinates>'], '$', $coords);
                $coords = explode('$', $coords);
                array_shift($coords);
                array_pop($coords);
                $coords = array_filter($coords, function ($v) {
                    if (!empty(str_replace(['"', ' '], '', $v))) return $v;
                });

                foreach ($coords as $keyCoord => $coord) {
                    // var_dump($coord);
                    $img[$key][$keyCoord]['name'] = preg_replace('/\s+/S', "", substr($coord, 0, strpos($coord, '<x0>')));

                    foreach ($tags as $tag) {
                        $start = strpos($coord, '<' . $tag . '>');
                        $end = strpos($coord, '</' . $tag . '>');
                        $length = strlen($coord) - $end;
                        $res = substr($coord, $start + strlen("<" . $tag . ">"), -$length);
                        $img[$key][$keyCoord][$tag] = $res ? $res : null;

                        if ($tag == 'ML') {
                            $str = preg_replace('/\s+/S', "", $img[$key][$keyCoord][$tag]);
                            // dd($str);
                            $ml['eval']['eval'] = preg_replace(["#(.*?)(<ML_EVAL>)(.*?)(</ML_RANK>)#", "#(</ML_EVAL>)(.*)#"], "", $str);
                            $ml['eval']['confidence'] = preg_replace(['#(.*?)(<ML_EVAL>)(.*?)(<ML_RANK>)#', '#(</ML_RANK>)(.*)#'], "", $str);
                            $ml['eval2']['eval'] = preg_match('<ML_EVAL2>', $str) ? preg_replace(['#(.*?)(<ML_EVAL2>)(.*?)(</ML_RANK>)#', '#(</ML_EVAL2>)(.*)#'], "", $str) : null;
                            $ml['eval2']['confidence'] = preg_match('<ML_EVAL2>', $str) ? preg_replace(['#(.*?)(<ML_EVAL2>)(.*?)(<ML_RANK>)#', '#(</ML_RANK>)(.*?)(</ML_EVAL2>)(.*)#'], "", $str) : null;

                            $img[$key][$keyCoord][$tag] = $ml;
                        }
                    }
                }
            } else {
                $imgName = preg_replace('/\s+/S', "", substr($value, 0, strpos($value, '<AnimalType>')));
                $img[$key]['name'] = str_replace($transactionNumber . "_", "", $imgName);
                foreach ($tags as $tag) {
                    $start = strpos($value, '<' . $tag . '>');
                    $end = strpos($value, '</' . $tag . '>');
                    $length = strlen($value) - $end;
                    $res = substr($value, $start + strlen("<" . $tag . ">"), -$length);
                    $img[$key][$tag] = $res ? $res : null;

                    if ($tag == 'ML') {
                        $str = preg_replace('/\s+/S', "", $img[$key][$tag]);
                        // dd($str);
                        $ml['eval']['eval'] = preg_replace(["#(.*?)(<ML_EVAL>)(.*?)(</ML_RANK>)#", "#(</ML_EVAL>)(.*)#"], "", $str);
                        $ml['eval']['confidence'] = preg_replace(['#(.*?)(<ML_EVAL>)(.*?)(<ML_RANK>)#', '#(</ML_RANK>)(.*)#'], "", $str);
                        $ml['eval2']['eval'] = preg_match('<ML_EVAL2>', $str) ? preg_replace(['#(.*?)(<ML_EVAL2>)(.*?)(</ML_RANK>)#', '#(</ML_EVAL2>)(.*)#'], "", $str) : null;
                        $ml['eval2']['confidence'] = preg_match('<ML_EVAL2>', $str) ? preg_replace(['#(.*?)(<ML_EVAL2>)(.*?)(<ML_RANK>)#', '#(</ML_RANK>)(.*?)(</ML_EVAL2>)(.*)#'], "", $str) : null;

                        $img[$key][$tag] = $ml;
                    }
                }
            }
        }
        // dd($img);
        return $img;
    }

    public static function getAlgMdlVersion($data)
    {
        $versionTags = ['ALGVERSION', 'MDLVERSION'];

        // $uri = $data['@metadata']['effectiveUri'];
        // $xml = file_get_contents($uri);
        $xml = preg_replace('/\s+/S', " ", $data);
        $transactionNumber = str_replace(['<TransactionNumber>', '</TransactionNumber>'], '$', $xml);
        $transactionNumber = explode('$', $transactionNumber)[1];
        $res = str_replace(['<ImageFileName>', '</ImageFileName>'], '$', $xml);
        $res = explode('$', $res);
        array_shift($res);
        array_pop($res);
        $res = array_filter($res, function ($value) {
            if (!empty(str_replace(['"', ' '], '', $value))) return $value;
        });

        foreach ($res as $key => $value) {
            $imgName = preg_replace('/\s+/S', "", substr($value, 0, strpos($value, '<AnimalType>'))) ? preg_replace('/\s+/S', "", substr($value, 0, strpos($value, '<AnimalType>'))) : preg_replace('/\s+/S', "", substr($value, 0, strpos($value, '<UseCase')));
            $img[$key]['name'] = str_replace($transactionNumber . "_", "", $imgName);

            // get ALGVERSION and MDLVERSION
            foreach ($versionTags as $tag) {
                $start = strpos($value, '<' . $tag . '>');
                $end = strpos($value, '</' . $tag . '>');
                $length = strlen($value) - $end;
                $res = substr($value, $start + strlen("<" . $tag . ">"), -$length);
                $img[$key][$tag] = $res ? $res : null;
            }
        }

        return $img;
    }

    public function setAoiType($name)
    {
        switch ($name) {
            case 'FOV':
                return 'FOV';
                break;
            case 'Lesion (up to 4)':
                return 'ROI';
                break;
            case 'Spleen':
                return 'AOI';
                break;
            case 'ROI':
                return 'AOI';
                break;
        }
    }
}
