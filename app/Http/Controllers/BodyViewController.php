<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BodyViewController extends Controller
{
     public function getBodyView()
    {
    	$body_view = DB::table('body_view')->get()->toArray();;

    	return response()->json($body_view, 200);
    }

    public function getByAlgorithmsId()
    {
    	$ids = explode(',', request()->ids);
    	$body_view = DB::table('bodyview_algorithm')
    					->join('body_view', 'body_view.id', 'bodyview_algorithm.body_view_id');

    	foreach ($ids as $key => $id) {
    		
    		if ($key == 0) {
    			$body_view->where('algorithm_id', $id);
    		}
    		$body_view->orWhere('algorithm_id', $id);
    	}
    	$body_view->select('body_view.id as id', 'body_view.name as name');
    	$body_view->distinct();
    	$body_view = $body_view->get();

    	return response()->json($body_view, 200);
    }
}
