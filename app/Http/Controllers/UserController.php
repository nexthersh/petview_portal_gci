<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Validator;
use App\Events\RejectedApprovedRegistrationRequest;
use App\Events\SetPasswordProcessed;
use App\Jobs\NotificationForRejectedApprovedRegistrationRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\Audit;
use App\User;
use Carbon\Carbon;
use App\Clinic;

class UserController extends Controller
{
    use Audit;

    private $user;

    public function __construct(Request $request, JWTAuth $jwt){

        $token = $jwt->getToken();
        $this->user = $jwt->toUser($token);
    }

    public function getAllUsers(Request $request)
    {
        $this->authorize('get', User::class);
        $query = User::with('clinic');
        $query->where('active', 1);
        $query->where('id', '!=', Auth::id());
        if ($this->user->role == 'clinic_admin') {
            $query = $this->getUsersByClinicAdmin($query);
        }
        $query = $this->getByFilters($query, $request);
        $users = $query->paginate(10);
    	  
        return response()->json($users, 200);
    }

    public function getUsers(Request $request)
    {
        $this->authorize('get', User::class);
        $query = User::query();
        $query->where('active', 1);
        $query->where('id', '!=', Auth::id());
        $query->where('role', '!=', 'woodybreast_user');
        if ($this->user->role == 'clinic_admin') {
            $query = $this->getUsersByClinicAdmin($query);
        }

        $query = $this->getByFilters($query, $request);
        $users = $query->paginate(10);
        
        $users = $query->get()->toArray();
        return response()->json($users, 200);
    }

    public function getUsersRequestList(Request $request)
    {
        $this->authorize('get', User::class);
        $query = User::with('clinic');
        $query->where('active', 0);
        $query->where('role', '=', 'user');
        if ($this->user->role == 'clinic_admin') {
            $query = $this->getUsersByClinicAdmin($query);
        }
        $query = $this->getByFilters($query, $request);
        // dd($query->toSql());
        $users = $query->paginate(10);
        
        return response()->json($users, 200);
    }

    private function getByFilters($query, $request)
    {
      if ($request->filled('search') && $request->search) {
            $query->where(function($q) use ($request){
              $q->where('id', 'LIKE', '%'.$request->search.'%');
              $q->orWhere('first_name', 'LIKE', '%'.$request->search.'%');
              $q->orWhere('last_name', 'LIKE', '%'.$request->search.'%');
              $q->orWhere('middle_name', 'LIKE', '%'.$request->search.'%');
              $q->orWhereRaw("CONCAT_WS(' ', CONCAT_WS('', `title`,`first_name`), `middle_name`, `last_name`) LIKE '%{$request->search}%'");
            });
            
        }

        if ($request->filled('name') && $request->name) {
            $query->where(function($q) use ($request){
              $q->where('first_name', 'LIKE', '%'.$request->name.'%');
              $q->orWhere('last_name', 'LIKE', '%'.$request->name.'%');
              $q->orWhere('middle_name', 'LIKE', '%'.$request->name.'%');
              $q->orWhereRaw("CONCAT_WS(' ', CONCAT_WS('', `title`,`first_name`), `middle_name`, `last_name`) LIKE '%{$request->name}%'");
            });
        }

        if ($request->filled('clinic_name') && $request->clinic_name) {
            $query->whereHas('clinic', function($q) use ($request){
              $q->where('name', 'like', '%'.$request->clinic_name.'%');
            });
        }

        if ($request->filled('clinic_id') && $request->clinic_id) {
            $query->where('clinic_id', $request->clinic_id);
        }

        if ($request->filled('email') && $request->email) {
            $query->where('email', 'like', '%'.$request->email.'%');
        }

        if ($request->filled('address') && $request->address) {
            $query->where('address', 'like', '%'.$request->address.'%');
        }

        if ($request->filled('phone') && $request->phone) {
            $query->where('phone', 'like', '%'.$request->phone.'%');
        }

        if ($request->has('created_from') && $request->created_from) {
            $query->whereDate('created_at', '>=', parseGMTDate($request->created_from));
        }

        if ($request->has('created_to') && $request->created_to) {
            $query->whereDate('created_at', '<=', parseGMTDate($request->created_to));
        }

        return $query;
    }

    public function show($id)
    {
        $user = User::with('clinic')->where('id', $id)->first();

        if ($user) {
            $this->authorize('show', $user);
            return response()->json($user, 200);
        }else{  
            return response()->json(['message' => 'User not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function store(UserRequest $request)
    {
        $this->authorize('create', User::class);

        try {
               $user = new User;
               $user->first_name = $request->first_name;
               $user->middle_name = $request->middle_name;
               $user->last_name = $request->last_name;
               $user->email = $request->email;
               $user->password = @$request->password ? bcrypt($request->password) : null;
               $user->title = $request->title;
               $user->phone = $request->phone;
               $user->address = $request->address;
               $user->clinic_id = $request->clinic_id;
               $user->role = $request->role;
               $user->active = 1;
               
               $user->save();
               $user->audits()->create($this->assignAuditValues('create', null, $user, $request));
               
               event(new SetPasswordProcessed($user));
               
               return response()->json($user, 200);
           
           } catch (\Exception $e) {
              // dd($e);
               return response()->json(['message' => 'User not created.', 'timestamp' => Carbon::now()->toDateTimeString()], 409);
           }         
    }

    public function update(UserRequest $request, $id)
    { 
       // dd($id);
        $user = User::with('clinic')->find($id);
        if (!$user) {
            return response()->json(['message' => 'User not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
        $this->authorize('update', $user);      
        
        try {
               $oldValue = $user->toJson();
               $user->first_name = $request->first_name;
               $user->middle_name = $request->middle_name;
               $user->last_name = $request->last_name;
               $user->email = $request->email;
               $user->password = @$request->password ? bcrypt($request->password) : $user->password;
               $user->title = $request->title;
               $user->phone = $request->phone;
               $user->address = $request->address;
            //    $user->clinic_id = $request->clinic_id;
               if ($request->role) {
                   $user->role = $request->role;
               }
                
               
               $user->save();
               $user->audits()->create($this->assignAuditValues('update', $oldValue, $user, $request));

               return response()->json($user, 200);
           
           } catch (\Exception $e) {
            // dd($e);
               return response()->json(['message' => 'User not updated.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
           }   
    }

    public function activateUser(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $this->authorize('update', $user);
            
            $oldValue = $user->toJson();
            $user->update(['active' => 1]);
            $user->audits()->create($this->assignAuditValues('update', $oldValue, $user, $request));

            event(new RejectedApprovedRegistrationRequest($user));
            return response()->json(['message' => 'Successfully activated account.', 'timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
        
        }else{  
            return response()->json(['message' => 'Account not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function rejectedUserRequest(Request $request, $id)
    {
      $user = User::find($id);
        if ($user && !$user->active) {
            $this->authorize('update', $user);
            
            $user->delete();
            $user->audits()->create($this->assignAuditValues('delete', $user, null, $request));

            NotificationForRejectedApprovedRegistrationRequest::dispatch($user->email);
            return response()->json(['message' => 'Successfully rejected user registration request.', 'timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
        
        }else{  
            return response()->json(['message' => 'User not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function updatetoken(Request $request)
    {
      $user = User::where('id', $this->user->id)->update(['token'=> $request->token]);
    }

    public function delete(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $this->authorize('delete', $user);
            
            $user->delete();
            $user->audits()->create($this->assignAuditValues('delete', $user, null, $request));
            
            return response()->json(['message' => 'Successfully deleted user.', 'timestamp' => Carbon::now()->toDateTimeString(), 'user' => $user], 200);
        }else{  
            return response()->json(['message' => 'User not found.', 'timestamp' => Carbon::now()->toDateTimeString()], 200);
        }
    }

    public function deleteAll(Request $request)
    {
      // dd($request->all());
        $this->authorize('get', User::class); 
        $ids = explode(',', $request->ids);
        $users = User::whereIn('id', $ids);

        if ($this->user->role == 'clinic_admin') {
            $users = $this->getUsersByClinicAdmin($users);
        }
        foreach ($users->get() as $user) {
            
            $user->delete();
            $user->audits()->create($this->assignAuditValues('delete', $user, null, $request));
        }

        return response()->json(['message' => 'Successfully deleted users.', 'timestamp' => Carbon::now()->toDateTimeString(), 'users' => $users], 200);
    }

    private function getUsersByClinicAdmin($query)
    {
        return $query->where('clinic_id', $this->user->clinic_id);
    }

    public function userPerformance()
    {
        $performance = User::withCount('cases')->get();

        return response()->json($performance, 200);
    }
}
