<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Algorithm;
use DB;

class AlgorithmController extends Controller
{
    public function getAlgorithm()
   {
   		$algorithms = Algorithm::with('referentImage.imageType')->get()->toArray();

   		return response()->json($algorithms, 200);
   }

   public function getByBodyPartId($id)
   {
   		$algorithms = DB::table('bodypart_algorithm')
   						->join('algorithm', 'algorithm.id', '=', 'bodypart_algorithm.algorithm_id')
   						->join('body_part', 'body_part.id', '=', 'bodypart_algorithm.body_part_id')
   						->where('body_part.id', $id)
                     ->where('for_no_processing_case', 0)
   						->select('algorithm.*')
   						->get();
   		return response()->json($algorithms, 200);
   }

   public function getByBodyPartIdForNoProcessingCase($id)
   {
      $algorithms = DB::table('bodypart_algorithm')
                     ->join('algorithm', 'algorithm.id', '=', 'bodypart_algorithm.algorithm_id')
                     ->join('body_part', 'body_part.id', '=', 'bodypart_algorithm.body_part_id')
                     ->where('body_part.id', $id)
                     ->where('for_no_processing_case', 1)
                     ->select('algorithm.*')
                     ->get();
         return response()->json($algorithms, 200);
   }

   public static function store($algorithm, $bodyPartId, $imageModalityId, $bodyViewId = null)
   {
      // add new algorithm
      $algorithmId = Algorithm::insertGetId([
                                      'name' => $algorithm,
                                      'suffix' => $algorithm 
                                    ]);
      // connect algorithm and body part
      $algorithmBodyPart = DB::table('bodypart_algorithm')->insert([
                                                                     'body_part_id' => $bodyPartId,
                                                                     'algorithm_id' => $algorithmId,
                                                                     'for_no_processing_case' => 1
                                                                  ]);
      // connect algorithm and body view
      $algorithmBodyView = DB::table('bodyview_algorithm')->insert([
                                                                     'body_view_id' => $bodyViewId,
                                                                     'algorithm_id' => $algorithmId
                                                                  ]);
      // connect algorithm and image modality
      $algorithmImageModality = DB::table('modalitytype_algorithm')->insert([
                                                                     'modality_type_id' => $imageModalityId,
                                                                     'algorithm_id' => $algorithmId
                                                                  ]);

      return $algorithmId;
   }
}
