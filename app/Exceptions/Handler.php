<?php

namespace App\Exceptions;

use Carbon\Carbon;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException as UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        // dd($exception);
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        // detect previous instance
            if ($exception instanceof TokenExpiredException) {
                return response()->json(['messages' => $exception->getMessage(), 'timestamp' => Carbon::now()->toDateTimeString()], 401);
            }
            else if ($exception instanceof TokenInvalidException) {
                return response()->json(['messages' => $exception->getMessage(), 'timestamp' => Carbon::now()->toDateTimeString()], 400);
            }
            else if ($exception instanceof TokenBlacklistedException) {
                return response()->json(['messages' => $exception->getMessage(), 'timestamp' => Carbon::now()->toDateTimeString()], 400);
            }else if ($exception instanceof JWTException) {

                return response()->json(['messages' => $exception->getMessage(), 'timestamp' => Carbon::now()->toDateTimeString()], 400);
            
            }else if ($exception instanceof ModelNotFoundException) {
               // return response()->json(['messages' => 'Not Found.', 'timestamp' => Carbon::now()->toDateTimeString()], 404);
               return response()->json(['messages' => $exception->getMessage(), 'timestamp' => Carbon::now()->toDateTimeString()], 404);
                
            }elseif ($exception instanceof AuthorizationException) {
                return response()->json(['messages' => $exception->getMessage(), 'timestamp' => Carbon::now()->toDateTimeString()], 403);
            
            }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['status' => 401, 'message' => 'Unauthenticated.', 'timestamp' => Carbon::now()->toDateTimeString()], 401);
        }

        return redirect()->guest(route('login'));
    }
}
