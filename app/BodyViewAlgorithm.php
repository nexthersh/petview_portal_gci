<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyViewAlgorithm extends Model
{
    public $timestamps = false;
    protected $table = 'bodyview_algorithm';
}
