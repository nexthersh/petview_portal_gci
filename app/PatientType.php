<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientType extends Model
{
    public $timestamps = false;
    protected $table = 'patient_type';
}
