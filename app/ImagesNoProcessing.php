<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use App\AoiImage;
use App\ImageAlgorithm;

class ImagesNoProcessing extends Model
{
    protected $table = 'images_no_processing';

    protected $appends = ["processed_images", "original_images", "original_images_thumbnail", "original_aoi_images", "algorithm_id", 'algorithm'];
    public static $preventAttrSet = true; 
    
    public function cases()
    {
    	return $this->belongsTo('App\CaseModel', 'case_id', 'id');
    }

    public function getProcessedImagesAttribute($value)
    {
        if(self::$preventAttrSet){
            $images = [];
            $algorithms = explode(',', $this->algorithm);
            //dd($algorithms);
            foreach ($algorithms as $index => $algorithm) {
               $res = ProcessedImage::where('case_id', $this->case_id)
                                ->where('key', 'not like', '%'.$this->transaction_id.'.xml'.'%')
                                ->where('key', 'not like', '%'.$this->name)
                                ->where('key', 'like', '%'.imageNameWithoutExtension($this->name).'%')
                                ->where('key', 'like', '%'.$algorithm.'%')
                                ->select('key', 'etag')
                                ->get()->toArray();
                if (count($res)>0) {
                    foreach ($res as $key => $value) {
                        $images[$index]['url'] = config('aws.AWS_URL').'/'.$value['key'];
                        $images[$index]['algorithm'] = $algorithm;
                        $images[$index]['key'] = $value['key'];
                        // add thumbmail
                        $path = 'thumbnail/'.$value['etag'].'.jpg';
                        if (file_exists(public_path( $path))) {
                            $images[$index]['thumbnail'] = url($path);
                        }else{
                            $img = $this->resize($images[$index]['url']);
                            if (!is_dir(public_path('thumbnail/'))) {
                                mkdir(public_path('thumbnail/'), 0777, true);
                            }
                            $img->save(public_path($path), 100);
                            $images[$index]['thumbnail'] = url($path);
                        }
                    }
                }                    
            }
        return $images; 
        }
    }

    public function getOriginalImagesAttribute($value)
    { 
        if(self::$preventAttrSet){
            if ($this->cases->case_type_id == 2) {
                $folder = config('aws.AWS_DEVELOP_FOLDER');

                $key = $folder.$this->transaction_id.'/'.$this->image_name;
                $url = $this->getImage($key);
            }else{
               $folder = config('aws.AWS_OUT_FOLDER').$this->transaction_id.'/';
               $imageNameWithoutExtension = substr($this->name, 0, strrpos($this->name, "."));
               $key = $folder.$this->transaction_id.'/'.str_replace(" ", "", $this->bodyPart->name).'/'.$imageNameWithoutExtension.'/'.$this->name;
               $url = $this->getImage($key);
            }
            

            return $url;
        }
    }

    public function getOriginalImagesThumbnailAttribute($value)
    {
        if (self::$preventAttrSet) {

            if ($this->cases->case_type_id == 2) {
                $folder = config('aws.AWS_DEVELOP_FOLDER');

                $key = $folder.$this->transaction_id.'/'.$this->image_name;
                $path = 'thumbnail/'.$this->image_name;
                $url = url($path);
            }else{
               $folder = config('aws.AWS_OUT_FOLDER').$this->transaction_id.'/';
               $imageNameWithoutExtension = substr($this->name, 0, strrpos($this->name, "."));
                $key = $folder.$this->transaction_id.'/'.str_replace(" ", "", $this->bodyPart->name).'/'.$imageNameWithoutExtension.'/'.$this->name;
                $path = 'thumbnail/'.$this->name;
                $url = url($path);
            }
            
            
            if (file_exists(public_path($path))) {
                return $url;
            }

            $img = $this->resize($this->getOriginalImagesAttribute($value));
            
            if (!is_dir(public_path('thumbnail/'))) {
                mkdir(public_path('thumbnail/'), 0777, true);
            }
            $img->save(public_path($path), 100);
            
            return $url;
        }
    }

    public function getImage($key)
    {
        $url = config('aws.AWS_URL').'/'.$key;
        /*if ($key) {
            $res = ProcessedImage::where('case_id', $this->case_id)
                                ->where('key', $key)
                                ->first();
            return $res;                    
        }
        $res = ProcessedImage::where('case_id', $this->case_id)
                                ->where('key','like', '%'.$this->name.'%')
                                ->where('key', 'not like', '%'.$this->transaction_id.'.xml'.'%')
                                ->first();*/
        return $url;
    }

    public function resize($path)
    {
        // dd($path);
        // create instance
        $img = Image::make($path);
        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img->resize(550, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $img;
    }

    public function getOriginalAoiImagesAttribute($value)
    {
        $url = [];
        if(self::$preventAttrSet){
            $folder = config('aws.AWS_AOI_FOLDER');
            $path = $this->transaction_id.'/'.imageNameWithoutExtension($this->image_name);

            $aoiImages = AoiImage::where('image_no_processing_id', $this->id)
                                 ->select('id', 'image_name', 'aoi_type', 'image_no_processing_id')
                                 ->get();
           
           foreach ($aoiImages as $key => $aoiImage) {
               $keyAws = $folder . $path .'/'. $aoiImage->image_name;
               $url[$key]['aoiType'] = $aoiImage->aoi_type;
               $url[$key]['image'] =  $this->getImage($keyAws);

               $img = $this->resize($url[$key]['image']);
               $pathThumbnail = 'thumbnail/'.imageNameWithoutExtension($this->image_name).'/'.$aoiImage->image_name;
               
               if (!is_dir(public_path('thumbnail/'.imageNameWithoutExtension($this->image_name)))) {
                    mkdir(public_path('thumbnail/'.imageNameWithoutExtension($this->image_name)), 0777, true);
               }
               $img->save(public_path($pathThumbnail), 100);

               $url[$key]['thumbnail'] = url($pathThumbnail);
           }
           return $url;
        }
    }

    public function imageModality()
    {
        return $this->belongsTo('App\ImageModality', 'image_modality_id');
    }

    public function bodyView()
    {
        return $this->belongsTo('App\BodyView', 'body_view_id');
    }

    public function bodyPart()
    {
        return $this->belongsTo('App\BodyPart', 'body_part_id');
    }

    public function imageAlgorithm()
    {
        return $this->hasMany('App\ImageAlgorithm','image_no_processing_id');
    }

    public function imageType()
    {
        return $this->belongsTo('App\ImageType', 'image_type_id');
    }

    public function patientType()
    {
        return $this->belongsTo('App\PatientType', 'patient_type_id');
    }

    public function getAlgorithmIdAttribute()
    {
        return ImageAlgorithm::where('image_no_processing_id', $this->id)->first()->algorithm_id;
    }
    public function getAlgorithmAttribute()
    {
        return ImageAlgorithm::join('algorithm', 'image_algorithm.algorithm_id', 'algorithm.id')
                            ->where('image_no_processing_id', $this->id)->first()->suffix;
    }
}
