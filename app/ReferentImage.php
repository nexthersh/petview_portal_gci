<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferentImage extends Model
{

    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ["url"];
    
    public function algorithm()
    {
    	return $this->belongsTo('App\Algorithm');
    }

    public function imageType()
    {
    	return $this->belongsTo('App\imageType');
    }

    public function getUrlAttribute($value)
    {
    	return config('aws.AWS_URL').'/'.config('aws.AWS_REFERENT_IMAGES').''.$this->name;
    }

    public function audits()
    {
        return $this->morphMany('App\Audit', 'auditable');
    }
}
