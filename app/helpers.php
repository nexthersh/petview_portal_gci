<?php

use Carbon\Carbon;

function imageNameWithoutExtension($name)
{    
    return substr($name, 0, strrpos($name, "."));
}

function prepareImageName($name)
{
    if (!$name) { return ""; }

    $array = explode(".", $name);
    $imageName = $array[0];
    $extension = $array[1];
    $imageName = rtrim($imageName, " ");
    $imageName = str_replace([" ", "-"], "_", $imageName);
    $imageName .= "__.".$extension;
    
    return $imageName;
}

function imageNameWithoutAlgorithm($name)
{
    return substr($name, 0, strpos($name, "__") + 2);
}

function parseGMTDate($gmtDate)
{
    $array = explode(" ", $gmtDate);
    $date = array_splice($array, 0, 4);

    return Carbon::createFromFormat("D M d Y", implode(" ", $date))->toDateString(); 
}

 ?>