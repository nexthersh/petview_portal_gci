<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyPart extends Model
{
    public $timestamps = false;
    protected $table = 'body_part';

    public function algorithm()
    {
    	return $this->belongsToMany('App\Algorithm', 'bodypart_algorithm');
    }
}
