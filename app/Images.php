<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\CaseController;
use App\ProcessedImage;
use Image;

class Images extends Model
{

	protected $appends = ["processed_images", "processed_original_images", "original_images", "original_aoi_images", "ml_eval"/*,"original_images_thumbnail", "stdev"*/];
    public static $preventAttrSet = true; 
    protected $guarded = [];

    public function aoiImage()
    {
        return $this->hasMany('App\AoiImage', 'image_id');
    }

    public function getCase($id)
    {
        return CaseModel::with('user', 'client', 'patient.patientType', 'bodyPart', 'AlteredStatus', 'images.aoiImage', 'imageModality', 'algorithm', 'caseType')->findOrFail($id);
    }

    public function algorithm() {
    
         return $this->belongsTo('App\Algorithm');
    }

    public function bodyPart() {
    
         return $this->belongsTo('App\BodyPart');
    }

    public function imageModality() {
    
         return $this->belongsTo('App\ImageModality');
    }

    public function alteredStatus() {
    
         return $this->belongsTo('App\AlteredStatus');
    }

    public function patientType(){

        return $this->belongsTo('App\PatientType');
    }

    public function bodyView(){

        return $this->belongsTo('App\BodyView');
    }

    public function breed(){

        return $this->belongsTo('App\Breed');
    }

    public function imageType()
    {
        return $this->belongsTo('App\ImageType', 'image_type_id');
    }

    public function getProcessedImagesAttribute($value)
    {   
        $images = array();
        if(self::$preventAttrSet && $this->getCase($this->getAttributeValue('case_id'))->case_type_id != 2){
            
            $algorithms = explode(',', $this->algorithm);
            $algorithms[] = 'SYMMII';
            $outFolderName = $this->transaction_id.'_RESULT';
            $res = scandir(storage_path('app/public/images/out/'.$outFolderName));
            $key = 0;
            
            foreach ($res as $value) {

                $fileName = imageNameWithoutExtension($this->name);
                
                if (!strpos($value, '.jpg') || !preg_match("#{$fileName}#", $value) || $fileName == imageNameWithoutExtension($value)) {
                    continue;
                }
                
                $images[$key]['url'] = url('/storage/images/out/'.$outFolderName.'/'.$value);
                $images[$key]['algorithm'] = $this->algorithm;
                $images[$key]['key'] = $outFolderName.'/'.$value;
                $images[$key]['result'] = $this->getResult($value);
                $images[$key]['aoi_type'] = @$images[$key]['result']['aoi_type'];
                
                //add thumbmail
                $path = 'thumbnail/'.$value;
                if (file_exists(public_path( $path))) {
                    $images[$key]['thumbnail'] = url($path);
                
                }else{
                    $img = $this->resize(storage_path('app/public/images/out/'.$images[$key]['key']));
                    if (!is_dir(public_path('thumbnail/'))) {
                        mkdir(public_path('thumbnail/'), 0777, true);
                    }
                    $img->save(public_path($path), 100);
                    $images[$key]['thumbnail'] = url($path);
                }

                $key++;
            }
        }
        
        return $images; 
    }

    public function getProcessedOriginalImagesAttribute($value)
    {   
        $image = array();
        if(self::$preventAttrSet && $this->getCase($this->getAttributeValue('case_id'))->case_type_id != 2){
            
            $outFolderName = $this->transaction_id.'_RESULT';
            $res = scandir(storage_path('app/public/images/out/'.$outFolderName));
            
            foreach ($res as $value) {
                // image name - replace .dcm extension with .jpg
                $name = imageNameWithoutExtension($this->name).'.jpg';
                $value = imageNameWithoutExtension($value).'.jpg';
                
                if ($name != $value) {
                    continue;
                }
                
                $image['url'] = url('/storage/images/out/'.$outFolderName.'/'.$value);
                
                //add thumbmail
                $path = 'thumbnail/original-processed/'.$value;
                if (file_exists(public_path( $path))) {
                    $image['thumbnail'] = url($path);
                
                }else{
                    $img = $this->resize(storage_path('app/public/images/out/'.$outFolderName.'/'.$value));
                    if (!is_dir(public_path('thumbnail/original-processed/'))) {
                        mkdir(public_path('thumbnail/original-processed/'), 0777, true);
                    }
                    $img->save(public_path($path), 100);
                    $image['thumbnail'] = url($path);
                }
            }
        }
        
        return $image; 
    }

    public function getResult($value)
    {
        $str = $value;
        // $imageName = substr($str, strpos($str, '_') + 1, strrpos($str, "_")  - strlen($str));
        // $imageName = str_replace(["_DV", "_ACL", "RAW_SYMMIIIX"], "", $str);
        
        $imageName = strpos($str, "__") ? imageNameWithoutAlgorithm($str) : str_replace(["_DV", "_ACL", "RAW_SYMMIIIX"], "", $str);

        if($result = AoiImage::where('image_name', 'like', '%'. $imageName .'%')->select('ml', 'stdev', 'mean', 'nbrightestpixpc', 'aoi_type')->first()){
           
            return $result = ['ml' => json_decode($result->ml) ?: json_decode('{"eval":{"eval":null,"confidence":null},"eval2":{"eval":null,"confidence":null}}'),
                               'stdev' => $result->stdev,
                               'mean' => $result->mean,
                               'nbrightestpixpc' => $result->nbrightestpixpc,
                               'aoi_type' => $result->aoi_type,
                           ];

        }elseif ($result = Images::where('name', 'like', '%'. $imageName .'%')->select('ml', 'stdev', 'mean', 'nbrightestpixpc')->first()) {
            
            return $result = ['ml' => json_decode($result->ml) ?: json_decode('{"eval":{"eval":null,"confidence":null},"eval2":{"eval":null,"confidence":null}}'),
                               'stdev' => $result->stdev,
                               'mean' => $result->mean,
                               'nbrightestpixpc' => $result->nbrightestpixpc
                           ];
        }else{
            return $result = ['ml' => json_decode('{"eval":{"eval":null,"confidence":null},"eval2":{"eval":null,"confidence":null}}'),
                               'stdev' => null,
                               'mean' => null,
                               'nbrightestpixpc' => null
                           ];
        }
    }

    public function getOriginalImagesAttribute($value)
    {
        if(self::$preventAttrSet){
            $case = $this->getCase($this->getAttributeValue('case_id'));
            if ($case->case_type_id == 2) {
                $folder = 'storage/images/develop/';
                $key = $folder.$this->transaction_id.'/'.explode('.', $this->name)[0].'/'.$this->name;
                $url = $this->getImage($key);
            
            }elseif ($case->case_type_id == 4) {
                $folder = config('aws.AWS_CHICKEN_UPLOAD_DATA');
                $key = $folder.$this->transaction_id.'/'.$this->name;
                $url = $this->getImage($key);
            
            }else{
               
                $folder = 'storage/images/in/';
                $imageNameWithoutExtension = imageNameWithoutExtension($this->name);
                $key = $folder.$this->transaction_id.'/'.$this->transaction_id.'/'.str_replace(" ", "", $case->bodyPart->name).'/'.$imageNameWithoutExtension.'/'.$this->name;
                $url = $this->getImage($key);
            }

            return $url;
        }
    }

    public function getOriginalImagesThumbnailAttribute($value)
    {
        $storagePath = storage_path('app/public/images/');
        $publicPath = public_path('thumbnail/'.$this->name);
        $url = url('thumbnail/'.$this->name);
        if (file_exists($publicPath)) {
            return $url;
        }
        
        if (self::$preventAttrSet) {
            $case = $this->getCase($this->getAttributeValue('case_id'));
            if ($case->case_type_id == 2) {
                $folder = 'develop/';
                $imageNameWithoutExtension = imageNameWithoutExtension($this->name);
                $key = $folder.$this->transaction_id.'/'.$imageNameWithoutExtension.'/'.$this->name;
            
            }elseif($case->case_type_id == 4){
                $folder = config('aws.AWS_CHICKEN_UPLOAD_DATA');
                $key = $folder.$this->transaction_id.'/'.$this->name;
            
            }else{
                $folder = 'in/';
                $imageNameWithoutExtension = imageNameWithoutExtension($this->name);
                $key = $folder.$this->transaction_id.'/'.$this->transaction_id.'/'.str_replace(" ", "", $case->bodyPart->name).'/'.$imageNameWithoutExtension.'/'.$this->name;
            }
            
            $imagePath = $storagePath.$key;
            $img = $this->resize($imagePath);
            
            $path = 'thumbnail/'.$this->name;
            if (!is_dir(public_path('thumbnail/'))) {
                mkdir(public_path('thumbnail/'), 0777, true);
            }
            if ($img) {
                try {
                    $img->save(public_path($path), 100);
                } catch (\Throwable $th) {
                    //throw $th;
                    return false;
                }
            }
            
            return $url;
        }
    }

    public function getOriginalAoiImagesAttribute($value)
    {
        $url = [];
        if(self::$preventAttrSet){
            
            $case = $this->getCase($this->getAttributeValue('case_id'));
            $folder = 'storage/images/aoi/';
            $path = $this->transaction_id.'/'.imageNameWithoutExtension($this->name);

            $aoiImages = AoiImage::where('image_id', $this->id)
                                 ->select('id', 'image_name', 'aoi_type', 'image_id', 'aoi_type_object_json', 'aoi_coords', 'stdev', 'mean', 'nbrightestpixpc', 'ml')
                                 ->get();
           
            foreach ($aoiImages as $key => $aoiImage) {
               $keyAws = $folder . $path .'/'. $aoiImage->image_name;
               $url[$key]['id'] = $aoiImage->id;
               $url[$key]['aoiType'] = $aoiImage->aoi_type;
               $url[$key]['name'] = $aoiImage->image_name;
               $url[$key]['aoi_type_object_json'] = $aoiImage->aoi_type_object_json;
               $url[$key]['aoi_coords'] = $aoiImage->aoi_coords;
               $url[$key]['stdev'] = $aoiImage->stdev;
               $url[$key]['mean'] = $aoiImage->mean;
               $url[$key]['nbrightestpixpc'] = $aoiImage->nbrightestpixpc;
               $url[$key]['ml'] = $aoiImage->ml;
               $url[$key]['image'] =  $this->getImage($keyAws);

               $img = $this->resize(storage_path('app/public/images/aoi/'.$this->transaction_id.'/'.imageNameWithoutExtension($this->name).'/'.$aoiImage->image_name));
               $pathThumbnail = 'thumbnail/'.imageNameWithoutExtension($this->name).'/'.$aoiImage->image_name;
               
               if (!is_dir(public_path('thumbnail/'.imageNameWithoutExtension($this->name)))) {
                    mkdir(public_path('thumbnail/'.imageNameWithoutExtension($this->name)), 0777, true);
               }
               if ($img) {
                   $img->save(public_path($pathThumbnail), 100);
                   $url[$key]['thumbnail'] = url($pathThumbnail);
               }
            }
        }
        return $url;
    }

    public function getImage($key)
    {
        $url = url('/'.$key);
        return $url;
    }

    public function resize($path)
    {
        // create instance
        try {
            $img = Image::make($path);
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            // $img->resize(550, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            return $img;
        } catch (\Intervention\Image\Exception\NotReadableException $e) {
            return $e;
        }
        

        
        
    }

    public function getMlEvalAttribute()
    {
        // dd($this->id);
        $result = [];
        $ml = Images::leftjoin('aoi_images', 'aoi_images.image_id', 'images.id')
                     ->select('images.ml as imagesMl', 'aoi_images.ml as aoiimagesMl')->where('images.id', $this->id)->get();
        // dd($ml);
        foreach ($ml as $key => $value) {
        
            $ml = $value->imagesMl ? json_decode($value->imagesMl) : json_decode($value->aoiimagesMl);

            if (@$ml->eval->eval ) {
                $result[$ml->eval->eval][]['confidence'] = $ml->eval->confidence;
            }
        }
        
        if (isset($result['ABNORMAL'])) {
            $mleval['eval'] = 'ABNORMAL';
            $mleval['confidence'] = collect($result['ABNORMAL'])->sortByDesc('confidence')->pluck('confidence')->first(); 

        }elseif (isset($result['NORMAL'])) {
            $mleval['eval'] = 'NORMAL';
            $mleval['confidence'] = collect($result['NORMAL'])->sortByDesc('confidence')->pluck('confidence')->first();
        }else{
            $mleval['eval'] = null;
            $mleval['confidence'] = null;
        }
        // dd($mleval);
        return $mleval;
    }

    public function getStdevAttribute()
    {
        $result = [];
        $stdev = Images::leftjoin('aoi_images', 'aoi_images.image_id', 'images.id')
                     ->select('images.stdev as imagesStdev', 'aoi_images.stdev as aoiimagesStdev')->where('images.id', $this->id)->get();
        foreach ($stdev as $key => $value) {
            $result[]['stdev'] = $value->imagesStdev ? $value->imagesStdev : $value->aoiimagesStdev;
        }
        
        $result = collect($result)->sortByDesc('stdev')->pluck('stdev')->first();
        
        return $result;

    }

}
