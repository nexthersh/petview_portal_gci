<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    
	protected $guarded = [];
	
    public function patients(){

    	return $this->hasMany('App\Patient');
    }

    public function numberPacient()
    {
    	return $this->hasMany('App\Patient');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function audits()
    {
        return $this->morphMany('App\Audit', 'auditable');
    }
}
