<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageAlgorithm extends Model
{
    protected $table = 'image_algorithm';

    public function algorithm()
    {
    	return $this->belongsTo('App\Algorithm', 'algorithm_id')->select('id', 'name', 'suffix');
    }
}
