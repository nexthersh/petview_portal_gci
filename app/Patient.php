<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	protected $guarded = [];
	
    public function client(){

    	return $this->belongsTo('App\Client', 'client_id');
    }

    public function patientType(){

    	return $this->belongsTo('App\PatientType');
    }

    public function breed(){

    	return $this->belongsTo('App\Breed');
    }

    public function alteredStatus(){

    	return $this->belongsTo('App\AlteredStatus');
    }

    public function caseHistory(){

        return $this->hasMany('App\CaseModel');
    }

    public function ddCase(){

        return $this->hasMany('App\CaseModel')->whereNotNull('differential_diagnosis')->where('differential_diagnosis', '!=', "")->select('patient_id','differential_diagnosis');
    }

    public function audits()
    {
        return $this->morphMany('App\Audit', 'auditable');
    }
}
