<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\NotificationsController;
use App\Http\Controllers\ProcessedImagesController;
use App\Http\Controllers\Chicken\ChickenBreastController;
use App\CaseModel;
use App\Mail\Notifications;
use Mail;

class ProcessedImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $case;
    public $handle;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($case, $handle)
    {
        $this->case = $case;
        $this->handle = $handle;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->case) {
            $caseOutStoragePath = 'app/public/images/out/'.$this->case->images[0]->transaction_id.'_RESULT/';
            foreach(scandir(storage_path($caseOutStoragePath)) as $file){
                
                if(is_file(storage_path($caseOutStoragePath).$file)){
                    try {
                        ProcessedImagesController::create(
                            $caseOutStoragePath.$file,
                            filesize(storage_path($caseOutStoragePath).$file),
                            $this->case->id
                        );
                    } catch (\Throwable $th) {
                        //throw $th;
                    }
                }
            }
            
            NotificationsController::create($this->case);
        }else{
            $chickenBreast = new ChickenBreastController;
            $chickenBreast->setResult($this->handle);
        }
        
    }
}
