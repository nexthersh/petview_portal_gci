<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
	return "PetView";
});

Route::get('/liver-archive', 'ImagesNoProcessingController@liverArchive');

Route::get('/clear', function () {
	Artisan::call('config:cache');
	Artisan::call('config:clear');
	Artisan::call('cache:clear');
	Artisan::call('view:clear');
});

Route::get('/migrate', function () {
	Artisan::call('migrate');
});

// test imago ice rest api
Route::get('/imagoice/login', 'ImagoRestApiController@login');
Route::get('/imagoice/list-sessions', 'ImagoRestApiController@listSessions');
Route::get('/imagoice/start-session', 'ImagoRestApiController@startSession');
Route::get('/imagoice/upload', 'ImagoRestApiController@upload');
Route::get('/imagoice/process', 'ImagoRestApiController@process');
Route::get('/imagoice/status', 'ImagoRestApiController@status');
Route::get('/imagoice/listoutputs', 'ImagoRestApiController@listoutputs');
Route::get('/imagoice/download', 'ImagoRestApiController@download');
Route::get('/imagoice/websocket', 'ImagoRestApiController@websocket');


/////////////////////


Route::post('/webhook', 'Payments\WebhookController@notifications');
Route::post('/notifications', 'ProcessedImagesController@getResponse');
// Route::get('/sendmessage', 'NotificationsController@sendNotificationTest');
Route::post('login/check-email', 'AuthController@checkIfEmailExists');
Route::post('login', 'AuthController@login')->name('login');
Route::get('auth/me', 'AuthController@me')->name('me');
Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::post('register/check-email', 'Auth\RegisterController@checkIfEmailExists')->name('check.email');
Route::post('register/check-email-trial-user', 'Auth\RegisterController@checkIfEmailExistsAndUserIsTrial')->name('check.email.trial.user');
Route::post('register/invite-user/{token}', 'Auth\RegisterController@registerInvitedUser');
Route::post('accounts/verifyAccount', 'Auth\VerificationController@verifyEmail')->name('verify.email');

// forgot password
Route::post('accounts/password/change', 'Auth\ResetPasswordController@sendsPasswordResetEmail');
Route::post('accounts/password/reset', 'Auth\ResetPasswordController@passwordReset');

//download file
Route::post('download', 'ImagesController@downloadImage');
Route::get('images', 'ImagesController@getImage');

// new clinic request
Route::get('clinics/dropdown', 'ClinicController@getClinics'); // bez paginacije
Route::post('clinics/newClinicRequest', 'ClinicController@newClinicRequest');

//chicken breast
Route::post('/chicken-breast', '\App\Http\Controllers\Chicken\ChickenBreastController@create');
Route::get('/chicken-breast/result', '\App\Http\Controllers\Chicken\ChickenBreastController@notificationForProcesedCases');

Route::group(['middleware' => ['auth:api', 'role:in_process']], function () {
	Route::post('/accounts/choose-plan', 'ChoosePlanController@choosePlan');
});

Route::group(['middleware' => ['auth:api', 'role:admin,woodybreast_user']], function () {
	Route::post('/chicken-breast/upload-data', '\App\Http\Controllers\Chicken\UploadDataController@uploadData');
});

Route::group(['middleware' => ['auth:api', 'role:clinic_admin']], function () {
	Route::post('/payments/payment-customer-profile', '\App\Http\Controllers\Payments\CustomerProfileController@update');
	Route::post('/payments/payment-pricing-plan/update', '\App\Http\Controllers\Payments\PricingPlanController@update');
});

Route::group(['middleware' => ['auth:api', 'role:admin,user,clinic_admin,woodybreast_user,trial_user']], function () {
	// case
	Route::resource('case', 'CaseController', ['except' => ['create', 'edit']]);
	Route::get('case/patient/{id}', 'CaseController@getAllCaseByPatientId');
	Route::post('case/{id}', 'CaseController@update');
	Route::delete('case/{id}', 'CaseController@destroy');
	Route::delete('cases', 'CaseController@deleteAll');
	Route::get('case/activate/{id}', 'CaseController@activate');
	Route::get('case/deactivate/{id}', 'CaseController@deactivate');

	Route::get('case/filter/dropdown', 'CaseController@getFilter');

	Route::get('clinics/available-cases', 'ClinicController@getAvailableCases');

	//client
	Route::post('clients', 'ClientController@store');
	Route::get('clients/dropdown', 'ClientController@getClients');
	Route::get('clients', 'ClientController@allClients');
	Route::get('clients/{id}', 'ClientController@show');
	Route::post('clients/{id}', 'ClientController@update');
	Route::delete('clients/{id}', 'ClientController@delete');
	Route::delete('clients', 'ClientController@deleteAll');

	//users
	Route::put('users/{id}', 'UserController@update');
	
	//patient 
	Route::get('patient/client/{id}/dropdown', 'PatientController@AllByClientId');
	Route::get('patient/client/{id}', 'PatientController@getAllByClientId');
	Route::get('patients', 'PatientController@index');
	Route::get('patients/{id}', 'PatientController@show');
	Route::post('patients', 'PatientController@store');
	Route::post('patient/{id}', 'PatientController@update');
	Route::delete('patient/{id}', 'PatientController@delete');
	Route::delete('patient', 'PatientController@deleteAll');

	// patient type
	Route::get('patient/type', 'PatientTypeController@getType');
	// animal type
	Route::get('animal_types/dropdown', 'PatientTypeController@getType');

	//breed dropdown
	Route::get('type/breed/{id_type}/dropdown', 'BreedController@getBreed');
	//algorithm dropdown
	Route::get('algorithm/dropdown', 'AlgorithmController@getAlgorithm');
	Route::get('dropdown/bodypart/algorithms/{id}', 'AlgorithmController@getByBodyPartId');
	Route::get('dropdown/case_noprocessing/bodypart/algorithms/{id}', 'AlgorithmController@getByBodyPartIdForNoProcessingCase');
	// body_part dropdown
	Route::get('dropdown/algorithms/bodypart/{id}', 'BodyPartController@getByAlgorithmId');
	//body_view dropdown
	Route::get('body_view/dropdown', 'BodyViewController@getBodyView');
	Route::get('dropdown/algorithms/body_view', 'BodyViewController@getByAlgorithmsId');
	//image_type dropdown
	Route::get('image_type/dropdown', 'ImageTypeController@getImageType');
	Route::get('dropdown/algorithms/image_type/{id}', 'ImageTypeController@getByAlgorithmId');
	//body_part dropdown
	Route::get('body_part/dropdown', 'BodyPartController@getAllBodyPart');
	//image_modality dropdown
	Route::get('image_modality/dropdown', 'ImageModalityController@getImageModality');
	Route::get('dropdown/algorithms/image_modality', 'ImageModalityController@getByAlgorithmsId');
	// altered_status dropdown
	Route::get('altered_status/dropdown', 'AlteredStatusController@getAlteredStatus');
	//upload file
	Route::post('file', 'CaseController@uploadFile');

	//update firebase token
	Route::post('updatetoken', 'UserController@updatetoken');

	//upload image only - no processing
	Route::post('/image_upload_noprocessing', 'CaseNoProcessingController@store');
	Route::post('/image_upload_noprocessing/{id}', 'CaseNoProcessingController@update');

	//create quick view case
	Route::post('/quick_case', 'CaseQuickViewController@create');
	//notifications
	Route::get('allnotifications', 'NotificationsController@getNotifications');
	Route::get('notifications/changestatus', 'NotificationsController@changeStatus');
	Route::get('notifications/changestatus/{id}', 'NotificationsController@notificationStatus');
	Route::delete('notifications/delete', 'NotificationsController@delete');

	Route::get('statistics', 'StatisticController@getStatisticsByRole');
});

Route::group(['middleware' => ['auth', 'role:admin,clinic_admin']], function () {
	// newsfeed
	Route::get('users/performance', 'UserController@userPerformance');

	Route::get('case/user/{userId}', 'CaseController@getAllCaseByUserId');
	Route::get('users', 'UserController@getAllUsers');
	Route::get('users-request-list', 'UserController@getUsersRequestList');
	Route::get('users/dropdown', 'UserController@getUsers'); // bez paginacije
	
	Route::post('activate-user/{id}', 'UserController@activateUser');
	Route::post('reject-user/{id}', 'UserController@rejectedUserRequest');
	Route::post('users', 'UserController@store');
	Route::get('users/{id}', 'UserController@show');
	Route::delete('users/{id}', 'UserController@delete');
	Route::delete('users', 'UserController@deleteAll');
	Route::get('clients/user/{userId}', 'ClientController@getAllClientsByUserId');

	// Invite users
	Route::post('/invite', 'InviteController@sendInvitationLink');

	Route::get('search-capability', 'CaseController@searchCapability');
	Route::post('download_search', 'ImagesController@searchCapabilityDownload');

	// clinics
	// newsfeed
	Route::get('clinics/performance', 'ClinicController@clinicPerformance');

	Route::get('clinics/clinicsRequestList', 'ClinicController@clinicsRequestList');
	Route::post('clinics/approveClinicRequest/{id}', 'ClinicController@activateClinic');
	Route::post('clinics/rejectClinicRequest/{id}', 'ClinicController@rejectedClinicRequest');
	Route::resource('clinics', 'ClinicController');

	// referent-images
	Route::get('referent-images/aoi-type', 'ReferentImageController@getAoiTypeReferentImages');
	Route::delete('referent-images', 'ReferentImageController@deleteAll');
	Route::resource('referent-images', 'ReferentImageController');

	//audits
	Route::get('audits', 'AuditController@index');

	Route::get('request-stats', 'StatisticController@index');
});
