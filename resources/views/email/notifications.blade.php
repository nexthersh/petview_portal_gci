<!DOCTYPE html>
<html lang="en"
    style="padding: 0;margin: 0;box-sizing: border-box;background-color: #5776a1;font-family: 'Roboto', sans-serif;font-size: 1rem;width: 100%;height: 100%;">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Download</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    <style>
        html,
        body {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            background-color: #5776a1;
            font-family: 'Roboto', sans-serif;
            font-size: 1rem;
            width: 100%;
            height: 100%;
        }
        table {
            color: #ffffff;
            text-align: center;
            margin: auto;
            margin-top: 50px;
            width: 100%;
        }
        a.btn {
            border-radius: 50px;
            border: none;
            padding: 8px 20px;
            color: #5776a1;
            cursor: pointer;
            background-color: #ffffff;
            display: inline-block;
            text-decoration: none;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 150px;
            margin: auto;
            margin-top: 20px;
            background: white;
        }
        a.btn:hover {
            background-color: #e9edf3;
            background: #e9edf3;
            color: #5776a1;
        }
        a {
            text-decoration: none;
            color: #e9edf3;
        }
        a:hover {
            color: #21395a;
        }
        img {
            margin-right: 5px;
        }
        .footer {
            margin-top: 20px;
            font-size: 0.5rem;
        }
    </style>
</head>
<body
    style="padding: 0;margin: 0;box-sizing: border-box;background-color: #5776a1;font-family: 'Roboto', sans-serif;font-size: 1rem;width: 100%;height: 100%;">
    <table style="color: #ffffff;text-align: center;margin: auto;margin-top: 50px;width: 100%;">
        <tr>
            <td>
                <strong>Your download is Ready</strong>
            </td>
        </tr>
        <tr>
            <td>
                <small>
                    Use the link below to download your images and XML.
                </small>
            </td>
        </tr>
        <tr>
            <td>
                <a class="btn" target="_blank" href="{{$data['link']}}"
                    style="text-decoration: none;color: #5776a1;border-radius: 50px;border: none;padding: 8px 20px;cursor: pointer;background-color: #ffffff;display: flex;align-items: center;justify-content: center;width: 150px;margin: auto;margin-top: 20px;background: white;"><img
                        src="https://i.ibb.co/XX1Ygcj/folder.png" width="20px" height="20px" alt="Screenshot-7"
                        border="0" style="margin-right: 5px;">Download</a>
            </td>
        </tr>
        <tr>
            <td>
                <div class="footer" style="margin-top: 20px;font-size: 0.5rem;">
                    <img width="100px" height="" src="https://petview-v2.itcentar.rs/assets/logo.png" alt=""
                        style="margin-right: 5px;">
                    <p>Powered by <a href="petview-v2.itcentar.rs"
                            style="text-decoration: none;color: #e9edf3;">PetView</a></p>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>