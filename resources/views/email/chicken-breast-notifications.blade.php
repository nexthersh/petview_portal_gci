@php
	$result = json_decode($data);
@endphp
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Petview Portal</h2>
        <h3>Hi, {{$result->Operator}}</h3>
        <p>
	    	Your case <b>{{$result->TransactionNumber}}</b> has been completed.<br>
	    	Here are the requested case details:
		</p>
		<p>
			Farm: {{$result->Farm}}<br>
			Line: {{$result->Line}}<br>
			Equipment: {{$result->Equipment}}<br>
			Operator: {{$result->Operator}}<br>
			Contact Email: {{$result->Email}}<br>
		</p>	
		@foreach($result->Images as $key => $image)
			<ul style="list-style-type: none;">
				<li><b>Image file name: {{$image->ImageFileName}}</b></li>
				<li>Wingband: {{$image->Wingband}}</li>
				<li>Body Weight: {{$image->BodyWeight}}</li>
				<li>Gender: {{$image->Gender}}</li>
				<li>Source: {{$image->Source}}</li>
				<li><b>Processed case info:</b></li>
				<li>WB Score: {{$image->WB_SCORE}}</li>
				<li>Breast Weight: {{$image->BREASTWEIGHT}}</li>
				<li><b>Result links:</b></li>
				@foreach($result->result as $key => $link)
					@if(strpos($link, '.xml'))
						@php unset($result->result[$key]); @endphp
						@continue
					@elseif(strpos($link, $image->ImageFileName) || strpos($link, imageNameWithoutExtension($image->ImageFileName). '_AOI_'. explode(':::', $image->ALGVERSION)[0]))
						<li><a href="{{$link}}">{{$link}}</a></li>
					@endif
					
				@endforeach
				<li><b>Development data:</b></li>
				<li>ML Version:{{$image->MDLVERSION}}</li>
				<li>Algorythm version:{{$image->ALGVERSION}}</li>
			</ul>
		@endforeach   
    </body>
</html>
